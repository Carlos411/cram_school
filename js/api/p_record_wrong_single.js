var ueId;
var token;

$(function(){
  // 取得頁面間傳遞參數
  ueId = $.url('?ue_id');
  token = Cookies.get('u_t');

  getUserErrata(token, ueId);
});

/**
 * 取得學生勘誤
 */
function getUserErrata(token, ueId) {
  $.ajax({
    url: g_obj.domain + '/api/userErrata?token=' + token + '&ue_id=' + ueId,
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      var item = response.data;
      if (item != null) {
        $("#us_name").html('<span class="date_time_text">' + item.ue_datetime + '</span>' + '發問人：' + item.us_name);
        $("#ue_content").html('問題: ' + item.ue_content);
        if (item.ue_img != '') {
          $("#question_images_list").html('<li><div class="item_block"><p class="img_title">問題圖片一</p><div class="img_parent"><img src="' + item.ue_img + '" class="data_img_preview"></div></div></li>');
        } else {
          $("#q_images_block").hide();
        }
        if (item.admin_id != null) {
          $("#admin_name").html('<span class="date_time_text">' + item.ue_answer_datetime + '</span>' + '解題老師：' + item.admin_name);
          $("#ue_answer_content").html('回答: ' + item.ue_answer_content);
          $("#answer_block").show();
        }
      }
    }
  });
}
