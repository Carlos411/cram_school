var select_1 = ''; // 第一個下拉選單：領域
var select_2 = ''; // 第二個下拉選單：科目
//var select_2_ol_ids = []; // 第二個下拉選單：科目

var callForSelect2 = 0;

$(function(){
  // 取得 url 參數
  if($.url('?select1') != undefined){
    select_1 = $.url('?select1');
  }
  if($.url('?select2') != undefined){
    select_2 = $.url('?select2');
  }

  getOutlineRoot(g_obj.ec_id, g_obj.db_practice_ot_id);
});

/**
 * 取得大綱根節點資料
 */
function getOutlineRoot(ecId, otId) {
  $.get( g_obj.domain + '/api/outline/root?ec_id=' + ecId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    $.each(response.data, function( index, item ) {
      if(index == 0){
        getOutlineNextNodeForSelect1(item.ol_id, otId);
      }
       //return false;
    });
  });
}

/**
 * 取得領域資料
 */
function getOutlineNextNodeForSelect1(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "";
      $.each(response.data, function( index, item ) {
         if(index == 0){ // 33 為「最新版　全科」
           html += '<option value="' + item.ol_id + '" selected>' + item.ol_display_name + '</option>';
           if(select_1 == ""){
             select_1 = item.ol_id;
           }
         }else{
           html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
         }
      });
      $("#select1").append(html);
      if(select_1 != ""){
        $("#select1").val(select_1);
      }

      if(select_1 != "" && callForSelect2 == 0){
        callForSelect2++;
        //alert(select_1);
        getOutlineNextNodeForSelect2(select_1, otId);

        // 取得答題河流
        getTestRecordTotalAndRightCount();
      }

    }
  });
}

/**
 * 取得科目資料
 */
function getOutlineNextNodeForSelect2(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "<option value='all' selected>全部</option>";
      if(select_2 == "" || select_2 == "all"){ // 若網址無參數，或有 select_2為"all"時，將所有的 ol_ids 存至 select_2_ol_ids 陣列
        select_2 = "all";
        //$.each(response.data, function( index, item ) {
          //select_2_ol_ids.push(item.ol_id)
        //});
      }else{
        //select_2_ol_ids.push(select_2)
      }
      $.each(response.data, function( index, item ) {
         html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
      });
      $("#select2").append(html);
      $("#select2").val(select_2);

      //$("#select2_text").html($("#select2 option:selected").text());
      //getVideoRiver();
    }
    //else{
      //$("#page_container").addClass("no_display");
    //}
  });
}

// 取得答題河流資料
function getTestRecordTotalAndRightCount(){
  //var call_times = 0;
  $.ajax({
    url: g_obj.domain + '/api/test/record/totalAndRightCount?token=' + Cookies.get('u_t') + '&ol_id=' + select_1,
    type: 'get',
    // data: data,
    dataType: 'json',
    //headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      //console.log(res);
      $("#select1_text").text(res.data.ol_display_name);
      $("#count_test_correct").text(res.data.test_correct_count);
      $("#count_test_already").text(res.data.test_already_count);
      $("#count_test_total").text(res.data.test_total + '題');

      var test_total = res.data.test_total; // 總題數
      var test_already_count = res.data.test_already_count; // 測驗已做答題數
      var test_correct_count = res.data.test_correct_count; // 測驗做對答題數


      // 大的瓶子
      var big_bottle = '';
      big_bottle += '<div class="subject_block">';
      big_bottle +=   '<div class="subject -empty" style="height: ' + (((test_total - test_already_count) / test_total) * 100) + '%;"></div>'; // 空的佔位
      big_bottle +=   '<div class="subject -water" style="height: ' + (((test_already_count - test_correct_count) / test_total) * 100) + '%;"></div>'; // 水
      $.each(res.data.nodes, function(i, item){
        if(item.ol_display_name == "國文"){
          big_bottle +=   '<div class="subject -purple" style="height: ' + ( (item.test_correct_count / test_total) * 100 ) + '%;"></div>'; // 國文
        }
        if(item.ol_display_name == "英文"){
          big_bottle +=   '<div class="subject -red1" style="height: ' + ( (item.test_correct_count / test_total) * 100 ) + '%;"></div>'; // 英文
        }
        if(item.ol_display_name == "數學"){
          big_bottle +=   '<div class="subject -blue" style="height: ' + ( (item.test_correct_count / test_total) * 100 ) + '%;"></div>'; // 數學
        }
        if(item.ol_display_name == "生物"){
          big_bottle +=   '<div class="subject -red2" style="height: ' + ( (item.test_correct_count / test_total) * 100 ) + '%;"></div>'; // 生物
        }
        if(item.ol_display_name == "地球科學"){
          big_bottle +=   '<div class="subject -yellow" style="height: ' + ( (item.test_correct_count / test_total) * 100 ) + '%;"></div>'; // 地球科學
        }
        if(item.ol_display_name == "物理"){
          big_bottle +=   '<div class="subject -green2" style="height: ' + ( (item.test_correct_count / test_total) * 100 ) + '%;"></div>'; // 物理
        }
        if(item.ol_display_name == "化學"){
          big_bottle +=   '<div class="subject -green1" style="height: ' + ( (item.test_correct_count / test_total) * 100 ) + '%;"></div>'; // 化學
        }
        if(item.ol_display_name == "歷史"){
          big_bottle +=   '<div class="subject -brown" style="height: ' + ( (item.test_correct_count / test_total) * 100 ) + '%;"></div>'; // 歷史
        }
        if(item.ol_display_name == "地理"){
          big_bottle +=   '<div class="subject -pink" style="height: ' + ( (item.test_correct_count / test_total) * 100 ) + '%;"></div>'; // 地理
        }
        if(item.ol_display_name == "公民"){
          big_bottle +=   '<div class="subject -green3" style="height: ' + ( (item.test_correct_count / test_total) * 100 ) + '%;"></div>'; // 公民
        }
      });
      big_bottle += '</div>';
      $("#main_bottle").html(big_bottle);

      // 小瓶子
      if(res.data.nodes.length > 0){
        var small_bottle = '';
        $.each(res.data.nodes, function( index, item_obj ) {

          small_bottle += '<li>';
          small_bottle +=   '<div class="bottle_parent">';
          small_bottle +=     '<p class="para1 -small">' + item_obj.test_correct_count + ' 題</p>';
          small_bottle +=     '<p class="para2 -small">' + item_obj.test_already_count + ' 題</p>';
          small_bottle +=     '<div class="bottle -small">';
          small_bottle +=       '<div class="subject_block">';
          small_bottle +=         '<div class="subject -empty" style="height: ' + ( ((item_obj.test_total - item_obj.test_already_count) / item_obj.test_total) * 100 ) + '%;"></div>'; // 空的佔位
          small_bottle +=         '<div class="subject -water" style="height: ' + (( (item_obj.test_already_count - item_obj.test_correct_count) / item_obj.test_total) * 100) + '%;"></div>'; // 水
          if(item_obj.ol_display_name == "國文"){
            small_bottle +=       '<div class="subject -purple" style="height: ' + ((item_obj.test_correct_count / item_obj.test_total) * 100) + '%;"></div>'; // 國文
          }
          if(item_obj.ol_display_name == "英文"){
            small_bottle +=       '<div class="subject -red1" style="height: ' + ((item_obj.test_correct_count / item_obj.test_total) * 100) + '%;"></div>'; // 英文
          }
          if(item_obj.ol_display_name == "數學"){
            small_bottle +=       '<div class="subject -blue" style="height: ' + ((item_obj.test_correct_count / item_obj.test_total) * 100) + '%;"></div>'; // 數學
          }
          if(item_obj.ol_display_name == "生物"){
            small_bottle +=       '<div class="subject -red2" style="height: ' + ((item_obj.test_correct_count / item_obj.test_total) * 100) + '%;"></div>'; // 生物
          }
          if(item_obj.ol_display_name == "地球科學"){
            small_bottle +=       '<div class="subject -yellow" style="height: ' + ((item_obj.test_correct_count / item_obj.test_total) * 100) + '%;"></div>'; // 地球科學
          }
          if(item_obj.ol_display_name == "物理"){
            small_bottle +=       '<div class="subject -green2" style="height: ' + ((item_obj.test_correct_count / item_obj.test_total) * 100) + '%;"></div>'; // 物理
          }
          if(item_obj.ol_display_name == "化學"){
            small_bottle +=       '<div class="subject -green1" style="height: ' + ((item_obj.test_correct_count / item_obj.test_total) * 100) + '%;"></div>'; // 化學
          }
          if(item_obj.ol_display_name == "歷史"){
            small_bottle +=       '<div class="subject -brown" style="height: ' + ((item_obj.test_correct_count / item_obj.test_total) * 100) + '%;"></div>'; // 歷史
          }
          if(item_obj.ol_display_name == "地理"){
            small_bottle +=       '<div class="subject -pink" style="height: ' + ((item_obj.test_correct_count / item_obj.test_total) * 100) + '%;"></div>'; // 地理
          }
          if(item_obj.ol_display_name == "公民"){
            small_bottle +=       '<div class="subject -green3" style="height: ' + ((item_obj.test_correct_count / item_obj.test_total) * 100) + '%;"></div>'; // 公民
          }

          small_bottle +=       '</div>';
          small_bottle +=       '<span class="subject_text">' + item_obj.ol_display_name + '</span>';
          small_bottle +=     '</div>';
          small_bottle +=     '<p class="para6">' + item_obj.test_total + '題</p>';
          small_bottle +=   '</div>';
          small_bottle += '</li>';

        });
        $("#bottle_list").html(small_bottle);
      }


    }
  });

}
