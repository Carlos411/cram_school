var olId;
var fieldName;
var subjectName;

$(function(){
  // 取得頁面間傳遞參數
  olId = $.url('?ol_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');

  // 設定導覽列
  checkHeaderTextGradeGroup(olId);
  //$("#header_field").text(fieldName);
  //$("#header_subject").text(subjectName);
  $("#header_grade_group").attr("href", "#");

  getOutlineVersion(olId, g_obj.ask_ot_id);
  getOutlineLastNode(olId);

  left_aside_link(2); // 更換大類別連結
});

/**
 * 取得版本資料
 */
function getOutlineVersion(olId, otId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        var html = "";     // 版本的tabs
        var html2 = "";    // tabs對應的區塊

        $.each(response.data, function( index, item ) {
           if (html == "") {
              html += '<li><button type="button" class="btn_tab -on" data-target="tab' + (index+1) + '">' + item.ol_display_name + '</button></li>';
              html2 += '<div class="tab_content" data-tab="tab' + (index+1) + '"><ul id="tab_content_list' + item.ol_id + '" class="tab_content_list"></ul></div>';
            } else {
              html += '<li><button type="button" class="btn_tab" data-target="tab' + (index+1) + '">' + item.ol_display_name + '</button></li>';
              html2 += '<div class="tab_content no_display" data-tab="tab' + (index+1) + '"><ul id="tab_content_list' + item.ol_id + '" class="tab_content_list"></ul></div>';
            }
        });

        $("#tab_list").html(html);
        $("#tab_content_block").html(html2);
        // tabs的html組合完後再設定tabs的功能
        setupTabs();

        // 最後再組合tabs對應區塊的年級/群組資料內容
        $.each(response.data, function( index, item ) {
           getOutlineNextNode(item.ol_id, otId, item.ol_display_name);
        });
      }else{
        $("#tab_content_block").html("");
      }
    }
  });
}

/**
 * 取得年級/群組資料
 */
function getOutlineNextNode(olId, otId, versionName) {
  $.ajax({
    url: g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        var html = "";

        $.each(response.data, function( index, item ) {
           html += '<li><a href="./p_ask_unit.html?ol_id=' + item.ol_id + '&field_name=' + fieldName + '&subject_name=' + subjectName + '&version_name=' + getVersionName(versionName) +'&grade_group_name=' + item.ol_display_name + '"><img src="'+ item.ol_img +'" class="bg_img"><span class="-text">' + item.ol_display_name + '</span></a></li>';
        });

        $("#tab_content_list" + olId).html(html);
      }
    }
  });
}

/**
 * 取得版本名稱，"不分版"不顯示
 */
function getVersionName(versionName) {
  // console.log(versionName);
  if (versionName != "不分版") {
    return versionName;
  }

  return "";
}

/**
 * 設定導覽列科目連結
 */
function getOutlineLastNode(olId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/lastNode?ol_id=' + olId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $.each(response.data, function( index, item ) {
         $("#header_subject").attr("href", "./p_ask_subject.html?ol_id=" + item.ol_id + "&field_name=" + fieldName);
         return false;
      });
    }
  });
}
