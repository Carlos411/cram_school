var select_1 = ''; // 第一個下拉選單：領域
var select_2 = ''; // 第二個下拉選單：科目
var select_3 = ''; // 第三個下拉選單：年級群組
//var select_2_ol_ids = []; // 第二個下拉選單：科目

var version_data = [];
var select3_data = []; // 第三個下拉選單準備放進去的資料

var callForSelect2 = 0;
var callForSelect3 = 0;

var small_bottle_arr = []; // 小瓶子相關資料

$(function(){
  // 取得 url 參數
  if($.url('?select1') != undefined){
    select_1 = $.url('?select1');
  }
  if($.url('?select2') != undefined){
    select_2 = $.url('?select2');
  }
  if($.url('?select3') != undefined){
    select_3 = $.url('?select3');
  }

  getOutlineRoot(g_obj.ec_id, g_obj.teacher_ot_id); // 學習記錄的 ot_id 與名師教學相同
});

/**
 * 取得大綱根節點資料
 */
function getOutlineRoot(ecId, otId) {
  $.get( g_obj.domain + '/api/outline/root?ec_id=' + ecId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    $.each(response.data, function( index, item ) {
      if(index == 0){
        getOutlineNextNodeForSelect1(item.ol_id, otId);
      }
       //return false;
    });
  });
}


/**
 * 取得領域資料
 */
function getOutlineNextNodeForSelect1(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "";
      $.each(response.data, function( index, item ) {
         if(index == 0){ // 33 為「最新版　全科」
           html += '<option value="' + item.ol_id + '" selected>' + item.ol_display_name + '</option>';
           if(select_1 == ""){
             select_1 = item.ol_id;
           }
         }else{
           html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
         }
      });
      $("#select1").append(html);
      if(select_1 != ""){
        $("#select1").val(select_1);
      }

      if(select_1 != "" && callForSelect2 == 0){
        callForSelect2++;
        getOutlineNextNodeForSelect2(select_1, otId);

        // 取得影片河流
        //getVideoRiverBig();
      }

    }
  });
}

/**
 * 取得科目資料
 */
function getOutlineNextNodeForSelect2(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "<option value='all' selected>全部</option>";
      if(select_2 == "" || select_2 == "all"){ // 若網址無參數，或有 select_2為"all"時，將所有的 ol_ids 存至 select_2_ol_ids 陣列
        select_2 = "all";
        //$.each(response.data, function( index, item ) {
          //select_2_ol_ids.push(item.ol_id)
        //});
      }else{
        //select_2_ol_ids.push(select_2)
      }
      $.each(response.data, function( index, item ) {
         html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
      });
      $("#select2").append(html);
      $("#select2").val(select_2);

      getOutlineNextNodeForSelect3(select_2, otId);
      //$("#select2_text").html($("#select2 option:selected").text());
    }
    //else{
      //$("#page_container").addClass("no_display");
    //}
  });
}

/**
 * 取得年級群組資料：第三個下拉選單
 */
function getOutlineNextNodeForSelect3(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      //var version_data = [];
      $.each(response.data, function( index, item ) {
        version_data.push({
          ol_id: item.ol_id,
          ol_display_name: item.ol_display_name
        });
      });
      getOutlineNextNodeForSelect3Again(otId);
    }
  });
}

function getOutlineNextNodeForSelect3Again(otId) {

  //console.log(version_data);
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + version_data[callForSelect3].ol_id + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( res ) {
    //console.log(res);
    if(res.data.length > 0){
      $.each(res.data, function( i, item ) {
        //console.log(item);
        //alert(callForSelect3);
        select3_data.push({
          ol_id: item.ol_id,
          ol_display_name: item.ol_display_name,
          version_name: (version_data[callForSelect3].ol_display_name == "不分版" ? "" : version_data[callForSelect3].ol_display_name )
        });
      });
      callForSelect3++;
      if(callForSelect3 < version_data.length){
        getOutlineNextNodeForSelect3Again(otId);
      }
      if(callForSelect3 == version_data.length){
        //console.log(select3_data);
        // 將 select3_data 裡的資料，放入第三個下拉選單
        var select3_html = "";
        $.each(select3_data, function( i, item ) {
          if( i == 0 ){
            if(select_3 == ""){
              select_3 = item.ol_id;
            }
          }
          select3_html += '<option value="' + item.ol_id + '">' + item.version_name + item.ol_display_name + '</option>';
        });
        $("#select3").html(select3_html);
        $("#select3").val(select_3);

        // 取得影片河流資料
        getVideoRiverBig(select_3);
        getVideoRiverSmallStep1(otId);
      }
    }
  });

}

// 取得大瓶子影片河流資料
function getVideoRiverBig(the_ol_id){
  //var call_times = 0;
  $.ajax({
    url: g_obj.domain + '/api/video/river?ol_id=' + the_ol_id + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    //headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      //console.log(res);
      //$("#select1_text").text(res.data.ol_display_name);
      $("#select1_text").text($("#select2 option:selected").text() + ' ' + $("#select3 option:selected").text());
      $("#select1_vd_second").text(Math.floor(res.data.vd_second / 60 / 60) + '小時' + Math.floor(Math.floor(res.data.vd_second / 60) % 60) + '分鐘');
      $("#select1_vr_second_total").text(Math.floor(res.data.vr_second_total / 60 / 60) + '小時' + Math.floor(Math.floor(res.data.vr_second_total / 60) % 60) + '分鐘');

      // 大的瓶子
      var big_bottle = '';
      big_bottle += '<div class="subject_block">';
      big_bottle +=   '<div class="subject -empty" style="height: ' + (100 - res.data.percent) + '%;"></div>'; // 空的佔位
      big_bottle +=   '<div class="subject -water" style="height: ' + res.data.percent + '%;"></div>'; // 水
      big_bottle += '</div>';
      $("#main_bottle").html(big_bottle);
    }
  });

}

// 取得小瓶子影片河流資料
var callGetVideoRiverSmallStep2 = 0;
function getVideoRiverSmallStep1(otId){
  //console.log(select_3);
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + select_3 + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( res ) {
    //console.log(res);
    if(res.data.length > 0){
      $.each(res.data, function( i, item ) {
        small_bottle_arr.push({
          ol_id: item.ol_id,
          ol_display_name: item.ol_display_name,
          nodes: []
        });
      });
      getVideoRiverSmallStep2(otId);
    }
  })
}

function getVideoRiverSmallStep2(otId){
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + small_bottle_arr[callGetVideoRiverSmallStep2].ol_id + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( res ) {
    //console.log(res);
    if(res.data.length >= 0){
      $.each(res.data, function( i, item ) {
        small_bottle_arr[callGetVideoRiverSmallStep2].nodes.push({
          ol_id: item.ol_id,
          ol_display_name: item.ol_display_name
        });
      });

      callGetVideoRiverSmallStep2++;
      if(callGetVideoRiverSmallStep2 < small_bottle_arr.length){
        getVideoRiverSmallStep2(otId);
      }
      if(callGetVideoRiverSmallStep2 == small_bottle_arr.length){
        //console.log(small_bottle_arr);
        // 取各節河流資料
        getVideoRiverSmallStep3(otId)
      }

    }
  });
}


var callGetVideoRiverSmallStep3 = 0;
var callGetVideoRiverSmallStep3Nodes = 0;
function getVideoRiverSmallStep3(otId){
  if(small_bottle_arr[callGetVideoRiverSmallStep3].nodes.length > 0){
    $.ajax({
      url: g_obj.domain + '/api/video/river?ol_id=' + small_bottle_arr[callGetVideoRiverSmallStep3].nodes[callGetVideoRiverSmallStep3Nodes].ol_id + '&token=' + Cookies.get('u_t'),
      type: 'get',
      // data: data,
      dataType: 'json',
      //headers: { 'token': Cookies.get('u_t') },
      success: function(response){
        small_bottle_arr[callGetVideoRiverSmallStep3].nodes[callGetVideoRiverSmallStep3Nodes].river = response.data;
        callGetVideoRiverSmallStep3Nodes++;
        if(callGetVideoRiverSmallStep3Nodes < small_bottle_arr[callGetVideoRiverSmallStep3].nodes.length){
          getVideoRiverSmallStep3(otId);
        }else{
          callGetVideoRiverSmallStep3++;
          callGetVideoRiverSmallStep3Nodes = 0;
          if( callGetVideoRiverSmallStep3 < small_bottle_arr.length ){
            getVideoRiverSmallStep3(otId);
          }else{
            //console.log(small_bottle_arr);
            renderSmallBottleArr();
          }
          //console.log(small_bottle_arr);
        }

      }
    });
  }else{
    callGetVideoRiverSmallStep3++;
    callGetVideoRiverSmallStep3Nodes = 0;
    if( callGetVideoRiverSmallStep3 < small_bottle_arr.length ){
      getVideoRiverSmallStep3(otId);
    }else{
      //console.log(small_bottle_arr);
      renderSmallBottleArr();
    }
  }
}

function renderSmallBottleArr(){
  //console.log(small_bottle_arr);
  var bottle_list_li = '';
  $.each(small_bottle_arr, function(i, item){
    if(item.nodes.length > 0){
      $.each(item.nodes, function(j, node){
        bottle_list_li += '<li>';
        bottle_list_li +=   '<div class="bottle_parent">';
        bottle_list_li +=     '<p class="para1 -small">' + Math.floor((small_bottle_arr[i].nodes[j].river.vr_second_total)/60) + '分</p>';
        bottle_list_li +=     '<div class="bottle -small">';
        bottle_list_li +=       '<div class="subject_block">';
        bottle_list_li +=         '<div class="subject -empty" style="height: ' + (100 - small_bottle_arr[i].nodes[j].river.percent) + '%;"></div>' // 空的佔位
        bottle_list_li +=         '<div class="subject -water" style="height: ' + small_bottle_arr[i].nodes[j].river.percent + '%;"></div>' // 水
        bottle_list_li +=       '</div>';
        if(small_bottle_arr[i].nodes[j].river.percent >= 50){
          bottle_list_li +=       '<span class="pipe_line -water"></span>';
        }else{
          bottle_list_li +=       '<span class="pipe_line -lock"><img src="./images/icon/icon-lock.svg" class="icon_lock"></span>';
        }
        bottle_list_li +=     '</div>';
        bottle_list_li +=     '<p class="para6">' + Math.floor((small_bottle_arr[i].nodes[j].river.vd_second)/60) + '分</p>';
        bottle_list_li +=     '<p class="para7">' + small_bottle_arr[i].ol_display_name + '</p>';
        bottle_list_li +=     '<p class="para8">' + small_bottle_arr[i].nodes[j].ol_display_name + '</p>';
        bottle_list_li +=   '</div>';
        bottle_list_li += '</li>';
      });
    }
  });
  $("#bottle_list").html(bottle_list_li);
  $("#bottle_list").children("li").last().find("span.pipe_line").addClass("no_display");

  setPager();
}

/**
 * 設定分頁
 */
function setPager() {
  //$("#bottle_list").html(totalContent);
  $("#bottle_list").children("li").each(function(index){
    if(index >= 7){
      $(this).addClass("no_display");
    }
  });
  var totalPage = Math.ceil($("#bottle_list").children().length / 7);
  $("#total_page").html(str_pad(totalPage, 2, 0));
  if (totalPage == 1) {
    $("#pager_right_button").addClass("-disabled");
  }
  if (totalPage > 1) {
    $("#pager_right_button").removeClass("-disabled");
  }
}
