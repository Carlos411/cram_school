$(function(){
  // 開發者模式 token
  //Cookies.set('u_t', 'eyJpdiI6IldIU1gyXC9CWVRYSlwvSkdrYzNUV2lsUT09IiwidmFsdWUiOiI3S3prTUZRa3lzQjFRZGpuTjZUUk9peFBKaWc3MmtBc3V6b2ZzSUp6dHNNODhNOW4ydVBkeVVVbXlZWFBtTHhLazMxZjhvalh6N2ZnaUpiQk52WDhFUjg2bVVoaDZuWUVqelwvbExmbHphM05saDlFYUtoNzVaOTVhZHZkdytaWHk1MFBxSWxOWkt4MytpdFBPbW1kNkh2TmM0dFdMTmY5N3Z6M21Pb0VPcktZSkFRRUpaYjhMU0psYTdMbnF2WlN1MG00U3ZyNUE0d0JcL2p1VGlVdjVHcXc9PSIsIm1hYyI6IjFmODUzMDU2MDFmZmVmMTNhYzZjN2Q4YTc4NjM1ZmIzNGM1ZWI3OTZmMDBlZGExNTQ0MzkzZmVlNGNjMGFiMGQifQ');

  $("#login_action").on("click", function(e){
    e.preventDefault();
    var up_name = $("#up_name").val();
    var up_cell_phone = $("#up_cell_phone").val();
    var up_password = $("#up_password").val();
    //console.log(up_name + " " + up_cell_phone + " " + up_password);

    if(up_name == "" || up_cell_phone == "" || up_password == ""){
      $("#login_fail_modal").find("span.-head").html("登入失敗");
      var show_content = "";
      if(up_password == ""){
        show_content = "家長密碼未填";
      }
      if(up_cell_phone == ""){
        show_content = "家長手機未填";
      }
      if(up_name == ""){
        show_content = "家長姓名未填";
      }
      $("#login_fail_modal").find("span.-content").html(show_content);
      $("#login_fail_modal").modal();
    }else{
      $.ajax({
        url: g_obj.domain + '/api/user/parent/signIn?ec_id=' + g_obj.ec_id + '&dv_id=' + g_obj.dv_id + '&up_name=' + up_name + '&up_cell_phone=' + up_cell_phone + '&up_password=' + up_password,
        //url: g_obj.domain + '/api/user/parent/signIn?ec_id=' + g_obj.ec_id + '&dv_id=' + g_obj.dv_id + '&up_name=' + '家長1' + '&up_cell_phone=' + '0900111222' + '&up_password=' + '1111',
        type: 'get',
        //data: data,
        dataType: 'json',
        statusCode: {
          200: function (response) {
            //console.log(response);
          },

          422: function (response) {
            //console.log(response.responseJSON.error);
          }
        },
        success: function(res){
          if(parseInt(res.status) == 1000){
            //alert(res.msg[0]);
            //alert("登入成功");
            Cookies.set('u_t', res.data.token);
            location.href = "./user_parents.html";
          }else{
            if(parseInt(res.status) == 3023){
              $("#login_fail_modal").find("span.-head").html("密碼錯誤");
            }else{
              $("#login_fail_modal").find("span.-head").html("登入失敗");
            }
            $("#login_fail_modal").find("span.-content").html(res.msg);

            $("#login_fail_modal").modal();
          }
        }
      });
    }

  });

});
