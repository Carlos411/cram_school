﻿var olId;
var unitOlId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterName;
var partName;
var pptData;
var firstPara;
var firstKnowledgePoint;
var outlineKnowledgePointHtml = "";
var knowledgeOdId;

var select_1 = '';
var select_2 = '';

$(function(){

  // 取得頁面間傳遞參數
  olId = $.url('?ol_id'); //這個值會被下面的20覆蓋
  ex_olId = $.url('?ol_id');
  unitOlId = $.url('?unit_ol_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');
  versionName = $.url('?version_name');
  gradeGroupName = $.url('?grade_group_name');
  chapterName = $.url('?chapter_name');
  partName = $.url('?part_name');

  select_1 = $.url('?select_1');
  select_2 = $.url('?select_2');

  //console.log("ex_olId=["+ ex_olId +"]");

  //console.log("olId=["+ olId +"]");

  // 設定導覽列
  $("#header_field").text(fieldName);
  $("#header_subject").text(subjectName);
  //$("#header_grade_group").text(versionName + gradeGroupName);
  //$("#header_unit").attr("href", "./f_teacher_unit.html?ol_id=" + unitOlId + "&field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + versionName + "&grade_group_name=" + gradeGroupName);

  //$("#main_title").text(subjectName + " " + ((versionName)?versionName + " " : "") + gradeGroupName + " " + chapterName + " " + partName);
  $("#main_title").text( fieldName + " " + chapterName + " " + partName);
  //$("#course_name").text(subjectName + versionName + gradeGroupName);

  //$("#ppt_img_block").hide();

  //設定下拉選項
  //setSelect_ol_id_item(olId, g_obj.exam_live_stream_ot_id );


  // 最近觀看
  get_video_recent("video_recent_item_list", g_obj.exam_live_stream_ot_id);



   //  移除選擇的項目
  $("#select_item_A").empty();
  $("#select_item_B").empty();

  //var olId = 20; //直接抓取 ol_id=20 (107 段考直播-年度) 之值套入 /outline/nextNode 使用
  //getOutlineRoot(olId , g_obj.exam_live_stream_ot_id);
  getOutlineRootInit(g_obj.ec_id , g_obj.exam_live_stream_ot_id);

  getKnowledgePoint2(ex_olId); //add by Myron

  // 設定導覽列連結
  //setHeaderGradeGroupHref(unitOlId);

  //setSelectOption();
});



function setSelectOption(){
	$('#select_item_A').val(select_1);
	$('#select_item_B').val(select_2);
}


function getOutlineRootInit(ecId, otId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/root?ec_id=' + ecId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        console.log(response);
      }
    },
    success: function(response){
      $.each(response.data, function( index, item ) {
         //console.log(item.ol_id);
         if(index == 0){
           //getOutlineNextNode(item.ol_id, otId);
           getOutlineRoot(item.ol_id, otId);
         }
         //return false;
      });
    }
  });
}

/**
 * 取得大綱根節點資料
 */
 //產生科目的下拉選項
function getOutlineRoot(olId, otId) {

  var runOnce=true;

  $.ajax({
    url: g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $.each(response.data, function( index, item ) {
         // console.log(item.ol_id);
         //create option item by Myron
         //alert("item.ol_id=["+ item.ol_id +"] , item.ol_display_name=["+ item.ol_display_name +"] ");
         $('#select_item_A').append( $('<option></option>').val(item.ol_id).text(item.ol_display_name) );
         /*
		 if ( runOnce==true ){
         	getOptionNode( item.ol_id , g_obj.exam_live_stream_ot_id);
         	runOnce=false; //只跑一次，就可以，啟始值
         }
		 */
         //return false;
      });
	  //getOptionNode( window.localStorage["Select_A_Value"] , g_obj.exam_live_stream_ot_id );
    getOptionNode( select_1 , g_obj.exam_live_stream_ot_id );
    }
  });

}

function SubmitForm(){
  location.href = "./exam_live_stream_list.html?select_1=" + $('#select_item_A').val();
}

/**
 * 取得資料
 */
 //產生年級的下拉選項

//var runGetOptionNodeOnce=true;
function getOptionNode(olId, otId) {

  $.ajax({
    url: g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        var html = "";
        $.each(response.data, function( index, item ) {
        	//alert("item_B item.ol_id=["+ item.ol_id +"] , item.ol_display_name=["+ item.ol_display_name +"] ");
           $('#select_item_B').append( $('<option></option>').val(item.ol_id).text(item.ol_display_name) );
          //  	if (runGetOptionNodeOnce==true){
          //  		getOutlineNextNode(
          //  			item.ol_id,
          //  			g_obj.exam_live_stream_ot_id ,
          //  			$('#select_item_B').find("option:selected").text(),
          //  			$('#select_item_B').find("option:selected").val()
          //  		);
          //  		runGetOptionNodeOnce=false;
        	// }
        });

      }

	     setSelectOption();

    }
  });
}

function SubmitFormB(){
  location.href = "./exam_live_stream_list.html?select_1=" + $('#select_item_A').val() + '&select_2=' + $('#select_item_B').val();
}

/**
 * 取得資料
 */
function getOutlineNextNode(olId, otId ,select_item_A_Text, select_item_A_Val ) {

  var unitOlId = select_item_A_Val;
  var chapterName = select_item_A_Text;
  var chapterName2 = ""; //20180530 改成不顯示

  $.ajax({
    url: g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        var html = "";

        $.each(response.data, function( index, item ) {
           //html += '<li><a href="#" class="item_link"><img src="'+ item.ol_img +'" class="link_img"><div class="desc_block"><p class="para1">'+ select_item_A_Text +'</p><p class="para2">' + item.ol_display_name + '</p></div></a></li>';
           html += '<li><a href="./exam_live_stream_video.html?ol_id=' + item.ol_id + '&unit_ol_id=' + unitOlId + '&chapter_name=' + chapterName + '&part_name=' + item.ol_display_name + '" class="item_link"><img src="' + item.ol_img + '" class="link_img"><div class="desc_block"><p class="para1">' + chapterName2 + '</p><p class="para2">' + item.ol_display_name + '</p></div></a></li>';
        });

        $("#item_list").append(html);
      }
    }
  });
}

/**
 * 取得影片右側知識點資料
 */
function getKnowledgePoint2(olId) {
  $.ajax({
    url: g_obj.domain + '/api/knowledgePoint?ol_id=' + olId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
         //console.log(response);
      }
    },
    success: function(response){
      //console.log(response);
      if (response.data.length > 0) {
        var html = '';
        $.each(response.data, function(index, item) {
          if(index == 0){
            html += '<li><span class="item_block"><span class="str_text">'+ item.od_name +'</span><span id="time_text' + item.od_id + '" class="time_text"></span></span></li>';
            knowledgeOdId = item.od_id;
            getVideo2(item.od_id);
            getUsercollection(g_obj.exam_live_stream_ot_id, ex_olId, knowledgeOdId, "", "", "");
          }
        });
        $("#video_desc_list").html(html);
      }
    }
  });
}



/**
 * 取得影片右側影片資料
 */
function getVideo2(odId) {
  $.ajax({
    url: g_obj.domain + '/api/video?od_id=' + odId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $.each(response.data, function(index, item) {
        // console.log(item.vd_id);
        // console.log(item.vd_second);
        $("#time_text" + odId).text(secondsTimeSpanToHMS(item.vd_second));
        loadVideo(getVideoStreamUrl(item.vd_id), true);
      });
    }
  });
}

/**
 * 取得影片連結
 */
function getVideoStreamUrl(vdId) {
  // console.log(g_obj.domain + '/api/video/stream?vd_id=' + vdId);
  return g_obj.domain + '/api/video/stream?vd_id=' + vdId + '&token=' + Cookies.get('u_t');
}

/**
 * 載入影片
 */
function loadVideo(videoUrl, autoplay){

    videojs('my-video').ready(function () {
                var player = this;
                var executed = false;
                player.src({type: 'video/mp4', src: videoUrl});

                if (autoplay) {
                  // console.log("autoplay");
                  player.play();
                }

                // 偵測影片播放完畢
                player.on('ended', function() {
                    if (!executed) { // 避免載入多個影片後，ended會被呼叫多次
                      // console.log('video is done!');
                      executed = true
                      var odId = $(".item_block.-on").parent().next().data("odid");
                      if (odId != undefined) {
                          // console.log(odId);
                          setTimeout(function(){switchVideo(odId);}, 2000);
                      } else {
                          odId = $(".item_block.-on").parent().next().next().data("odid");
                          if (odId != undefined) {
                            // console.log(odId);
                            setTimeout(function(){switchVideo(odId);}, 2000);
                          }
                      }
                    }
                });

                // 偵測影片播放進度
                player.on('timeupdate', function () {
                  var currentTime = player.currentTime();
                  // console.log("currentTime:" + currentTime);

                  if (pptData != null) {
                    $.each(pptData, function(index, item) {
                      if (currentTime > item.cs_second_from && currentTime < item.cs_second_to) {
                        $("#ppt_img").attr("src", item.cs_path);
                        $("#ppt_img_block").show();
                        return false;
                      } else {
                        if (pptData.length == index + 1) { // 檢查是不是最後一張PTT
                          $("#ppt_img_block").hide();
                        } else {
                          return;
                        }
                      }
                    });
                  }
                })

                // 暫停時，就將 div.comment_block 顯示
                player.on('pause', function () {
                  if($("div.comment_block").hasClass("-none")){
                    $("div.comment_block").removeClass("-none");
                  }
                });
            });

}

/**
 * 影片時間格式轉換
 */
function secondsTimeSpanToHMS(s) {
    var h = Math.floor(s/3600); //Get whole hours
    s -= h*3600;
    var m = Math.floor(s/60); //Get remaining minutes
    s -= m*60;
    return (h == 0 ? '' : (h < 10 ? '0'+h+':' : h+':'))+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding
}
