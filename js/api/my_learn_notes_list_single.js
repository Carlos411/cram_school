var olId;
var unitOlId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterName;
var partName;

var unId;

$(function(){
  // 取得頁面間傳遞參數
  olId = $.url('?ol_id');
  unitOlId = $.url('?unit_ol_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');
  versionName = $.url('?version_name');
  gradeGroupName = $.url('?grade_group_name');
  chapterName = $.url('?chapter_name');
  partName = $.url('?part_name');
  unId = $.url("?un_id");

  // 設定導覽列
  checkHeaderTextAfterUnit(unitOlId);
  //$("#header_field").text(fieldName);
  //$("#header_subject").text(subjectName);
  //$("#header_grade_group").text(versionName + gradeGroupName);
  $("#header_unit").attr("href", "./my_learn_notes_unit.html?ol_id=" + unitOlId + "&field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + versionName + "&grade_group_name=" + gradeGroupName);

  $("#main_title").text(subjectName + " " + ((versionName)?versionName + " " : "") + gradeGroupName + " " + chapterName + " " + partName);

  // 設定導覽列連結
  setHeaderGradeGroupHref(unitOlId);

  // 取得使用者筆記
  getUsernote(unId);

  // 取得使用者點數
  getUserPoint();
});

function getUsernote(unId){
  if(unId != undefined){
    $.ajax({
      url: g_obj.domain + '/api/usernote?un_id=' + unId,
      type: 'get',
      // data: data,
      dataType: 'json',
      headers: { 'token': Cookies.get('u_t') },
      success: function(res){
        if(res.data.length == undefined){
          //alert(res.data.length);
          img_src = res.data.un_path;
          InitThis();

          $("#comment_text").val(res.data.un_content);

          var small_pics = [];
          // if(res.data.un_path2 != ""){
            small_pics.push(res.data.un_path2);
          // }
          // if(res.data.un_path3 != ""){
            small_pics.push(res.data.un_path3);
          // }
          // if(small_pics.length == 2){
          //   $("#add_pic_li").addClass("no_display");
          // }
          if(small_pics.length > 0){
            var html = '';
            $.each(small_pics, function(i, item){
              if (item != "") {
  				html += '<li class="note_item_pic note_item_pic_' + i + '">';
  				html +=   '<a href="javascript:;" class="item_link" onclick="javascript:return false;">';
  				html +=     '<button type="button" onclick="javascript:removeNotePic(' + i + ', 0);" class="btn_delete_picture"><img src="./images/icon/btn-delete.svg"></button>';
  				html +=     '<img src="' + item + '" class="link_img">';
  				//html +=     '<div class="desc_block">';
  				//html +=       '<p class="para1">' + res.data.un_content + '</p>';
  				//html +=     '</div>';
  				html +=   '</a>';
  				html += '</li>';
  			}
            });
            $("#add_pic_li").before(html);
  		  if ($('.note_item_pic').length >= 2) $("#add_pic_li").addClass("no_display");
          }
        }else{
          // 回上一頁
          alert("該收藏已被取消");
          window.history.go(-1);
        }
      }
    });
  } else {
    img_src = "./images/bg/bg_temp_canvas.png";
    InitThis();
  }
}
