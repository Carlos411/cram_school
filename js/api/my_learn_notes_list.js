var olId;
var unitOlId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterName;
var partName;

var totalCount = 0;

$(function(){
  // 取得頁面間傳遞參數
  olId = $.url('?ol_id');
  unitOlId = $.url('?unit_ol_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');
  versionName = $.url('?version_name');
  gradeGroupName = $.url('?grade_group_name');
  chapterName = $.url('?chapter_name');
  partName = $.url('?part_name');

  // 設定導覽列
  checkHeaderTextAfterUnit(unitOlId);
  //$("#header_field").text(fieldName);
  //$("#header_subject").text(subjectName);
  //$("#header_grade_group").text(versionName + gradeGroupName);
  $("#header_unit").attr("href", "./my_learn_notes_unit.html?ol_id=" + unitOlId + "&field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + versionName + "&grade_group_name=" + gradeGroupName);

  $("#main_title").text(subjectName + " " + ((versionName)?versionName + " " : "") + gradeGroupName + " " + chapterName + " " + partName);

  // 設定導覽列連結
  setHeaderGradeGroupHref(unitOlId);

  // 取得使用者筆記列表
  getUsernoteList(olId);
});

function getUsernoteList(olId){
  $.ajax({
    url: g_obj.domain + '/api/usernote/list?ol_id=' + olId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      //alert(res.data.length);
      if(res.data.length > 0){
        var html = '';
        totalCount = res.data.length + 1; // +1 是因為最後還有一個新增的項目
        $.each(res.data, function(index, item){
          html += '<li class="' + (index <= 14 ? "": "no_display") + '">';
          html +=   '<a href="./my_learn_notes_list_single.html?' + url('query') + '&un_id=' + item.un_id + '" class="item_link">';
          html +=     '<button type="button" class="btn_delete" onclick="$(\'#del_un_id\').val(' + item.un_id + ');"><img src="./images/icon/btn-delete.svg"></button>';
          html +=     '<img src="' + item.un_path + '" class="link_img">';
          html +=     '<div class="desc_block">';
          html +=       '<p class="para1">' + item.un_content + '</p>';
          html +=     '</div>';
          html +=   '</a>';
          html += '</li>';
        });
        html += '<li id="add_item_li" class="' + (totalCount > 15?"no_display":"") + '">';
        html +=   '<a href="' + "./my_learn_notes_list_single.html?" + url('query') + '" id="add_item_li_inner_link" class="btn_add_item">';
        html +=     '<span class="add_item">+</span>';
        html +=   '</a>';
        html += '</li>';
        $("#item_list").html(html);

        setPager();
      }else{
        var html = '';
        html += '<li id="add_item_li" class="">';
        html +=   '<a href="' + "./my_learn_notes_list_single.html?" + url('query') + '" id="add_item_li_inner_link" class="btn_add_item">';
        html +=     '<span class="add_item">+</span>';
        html +=   '</a>';
        html += '</li>';
        $("#item_list").html(html);
        $("#pager_right_button").addClass("-disabled");
      }

      // 最後的新增項目的連結
      //$("#add_item_li_inner_link").attr("href", "./my_learn_notes_list_single.html?" + url('query'));
    }
  });
}

/**
 * 設定分頁
 */
function setPager() {
  //$("#item_list").html(totalContent);
  var totalPage = Math.ceil(totalCount / 15);

  $("#total_page").html(str_pad(totalPage, 2, 0));
  if (totalPage == 1) {
    $("#pager_right_button").addClass("-disabled");
  }
  if (totalPage > 1) {
    $("#pager_right_button").removeClass("-disabled");
  }
}
