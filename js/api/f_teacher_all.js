// 最近觀看
var get_video_recent = function(id_name, ot_id){
  if(ot_id == 1){ // 名師教學
    $.get( g_obj.domain + '/api/video/recent?token=' + Cookies.get('u_t') + '&ot_id=' + ot_id, function( response ) {
      /*
      最後網址參數應產生如下：
      ol_id=16&
      unit_ol_id=12&
      field_name=最新版全科&
      subject_name=國文&
      version_name=&
      grade_group_name=古文&
      chapter_name=第一單元虯髯客傳&
      part_name=1-1課文解析(上)
      */
      if(response.data.length > 0){
        var recent_html = '';
        $.each(response.data, function(i, item){
          recent_html += '<li><a href="./f_teacher_video.html?ol_id=' + item.previous_nodes[0].ol_id + '&unit_ol_id=' + item.previous_nodes[2].ol_id + '&field_name=' + item.previous_nodes[5].ol_display_name + '&subject_name=' + item.previous_nodes[4].ol_display_name + '&version_name=' + ((item.previous_nodes[3].ol_display_name == "不分版" ? "" : item.previous_nodes[3].ol_display_name )) + '&grade_group_name=' + item.previous_nodes[2].ol_display_name + '&chapter_name=' + item.previous_nodes[1].ol_display_name + '&part_name=' + item.previous_nodes[0].ol_display_name + '" class="item_link"><img src="' + item.vd_img + '" class="link_img"><div class="desc_block"><p class="para1">' + item.previous_nodes[1].ol_display_name + '</p><p class="para2">' + item.previous_nodes[0].ol_display_name + '</p></div></a></li>';
        });
        $("#" + id_name).html(recent_html);
      }else{
        $("#" + id_name).html("");
      }
    });
  }

  if(ot_id == 2){ // 段考直播
    $.get( g_obj.domain + '/api/video/recent?token=' + Cookies.get('u_t') + '&ot_id=' + ot_id, function( response ) {
      /*
      最後網址參數應產生如下：
      ol_id=29&
      unit_ol_id=23&
      field_name=國文&
      chapter_name=七上&
      part_name=康版第一次段考複習&
      select_1=21&
      select_2=23
      */
      if(response.data.length > 0){
        var recent_html = '';
        $.each(response.data, function(i, item){
          recent_html += '<li><a href="./exam_live_stream_video.html?ol_id=' + item.ol_id_2 + '&unit_ol_id=' + item.ol_id_1 + '&field_name=' + item.previous_nodes[2].ol_display_name + '&chapter_name=' + item.ol_display_name_1 + '&part_name=' + item.ol_display_name_2 + '&select_1=' + item.previous_nodes[2].ol_id + '&select_2=' + item.ol_id_1 + '" class="item_link"><img src="' + item.vd_img + '" class="link_img"><div class="desc_block"><p class="para1">' + item.previous_nodes[2].ol_display_name + ' ' + item.previous_nodes[1].ol_display_name + '</p><p class="para2">' + item.ol_display_name_2 + '</p></div></a></li>';
        });
        $("#" + id_name).html(recent_html);
      }else{
        $("#" + id_name).html("");
      }
    });
  }
};
