﻿
var chapterCount;
var totalCount = 0;
var totalContent = "";

$(function(){

  getOutlineRoot(g_obj.ec_id, g_obj.db_test_history_ot_id);

  // 最近觀看
  getExamRecent("recently_view_item_list");
});

// 最近觀看
function getExamRecent(id_name){
  $.get( g_obj.domain + '/api/exam/recent?token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var recent_html = '';
      $.each(response.data, function(i, item){
        recent_html += '<li>';
        recent_html +=   '<a href="#" class="item_link" data-ol-id="' + item.ol_id + '" data-exam-question-path="" data-exam-explain-path="">';
        recent_html +=     '<img src="' + item.ol_img + '" class="link_img">';
        recent_html +=     '<div class="desc_block">';
        recent_html +=       '<p class="para1">' + item.ol_display_name3 + ' </p>';
        recent_html +=       '<p class="para2">' + item.ol_display_name2 + ' ' + item.ol_display_name + '</p>';
        recent_html +=     '</div>';
        recent_html +=   '</a>';
        recent_html += '</li>';
      });
      $("#" + id_name).html(recent_html);
      getExamRecentQuestionAndExplain();
    }else{
      $("#" + id_name).html("");
    }
  });
}
function getExamRecentQuestionAndExplain(){
  $("#recently_view_item_list").find("a.item_link").each(function(){
    //console.log($(this).attr("data-ol-id"));
    var that = this;
    $.get( g_obj.domain + "/api/exam?token=" + Cookies.get('u_t') + "&ol_id=" + $(this).attr("data-ol-id"), function( res ) {
      $(that).attr("data-exam-id", res.data.exam_id);
      $(that).attr("data-exam-question-name", res.data.exam_question_name);
      $(that).attr("data-exam-question-path", res.data.exam_question_path);
      $(that).attr("data-exam-explain-name", res.data.exam_explain_name);
      $(that).attr("data-exam-explain-path", res.data.exam_explain_path);
    });
  });
}

// 送出觀看記錄
function sendExamRecord(examid) {
	var fData = new FormData($("#form_user_edit")[0]);	// 表單名稱請依實際修改
	fData.append("token", Cookies.get('u_t'));
	fData.append("exam_id", examid);
	$.ajax({
		url: g_obj.domain + '/api/exam/record',
		type: 'post',
		data: fData,
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
		}
	});
}

/**
 * 取得大綱根節點資料
 */
function getOutlineRoot(ecId, otId) {

  $.ajax({
    url: g_obj.domain + '/api/outline/root?ec_id=' + ecId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $.each(response.data, function( index, item ) {
         // console.log(item.ol_id);
         //create option item by Myron
         $('#select_item_A').append( $('<option></option>').val(item.ol_id).text(item.ol_display_name) );
         //return false;
      });
      SubmitForm();
    }
  });
}


//add by Myron 2018.05.19
function SubmitForm(){
	var ol_id_value = 0;

	ol_id_value = $('#select_item_A').val();


	//  移除選擇的項目
	$("#select_item_B").empty();

	getOptionNode(ol_id_value, g_obj.db_test_history_ot_id);
}

/**
 * 取得年度資料
 */
function getOptionNode(olId, otId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        var html = "";
        //$('#select_item_B').append( $('<option></option>').val("").text('年度') );

        $.each(response.data, function( index, item ) {
           $('#select_item_B').append( $('<option></option>').val(item.ol_id).text(item.ol_display_name) );
        });
        SubmitFormB();
      }
    }
  });
}



//add by Myron 2018.05.19
function SubmitFormB(){
	var ol_id_value = 0;
	ol_id_value = $('#select_item_B').val();

	//alert("ol_id_value=["+ ol_id_value +"]");
	getOutlineNextNode(ol_id_value, g_obj.db_test_history_ot_id, $('#select_item_A').find("option:selected").text() );

}


/**
 * 取得領域資料
 */
function getOutlineNextNode(olId, otId, select_item_A_Text ) {
  $.ajax({
    url: g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        //console.log(response);
      }
    },
    success: function(response){
      totalCount = response.data.length;
      if(response.data.length > 0){
        var html = "";

        $.each(response.data, function( index, item ) {
           html += '<li><a href="#" class="item_link" data-ol-id="' + item.ol_id + '"><img src="'+ item.ol_img +'" class="link_img"><div class="desc_block"><p class="para2">' + item.ol_display_name + '</p></div></a></li>';
        });

        $("#item_list").html(html);
      }else{
        $("#item_list").html("");
      }
      setPager();
      getApiExam();

    }
  });
}

// 歷屆試題
function getApiExam(){
  $("#item_list").find("a.item_link").each(function(){
    //console.log($(this).attr("data-ol-id"));
    var that = this;
    $.get( g_obj.domain + "/api/exam?token=" + Cookies.get('u_t') + "&ol_id=" + $(this).attr("data-ol-id"), function( res ) {
      $(that).attr("data-exam-id", res.data.exam_id);
      $(that).attr("data-exam-question-name", res.data.exam_question_name);
      $(that).attr("data-exam-question-path", res.data.exam_question_path);
      $(that).attr("data-exam-explain-name", res.data.exam_explain_name);
      $(that).attr("data-exam-explain-path", res.data.exam_explain_path);
    });

  });
}

/**
 * 設定分頁
 */
function setPager() {
  var totalPage = Math.ceil(totalCount / 15);
  $("#total_page").html(str_pad(totalPage, 2, 0));
  if (totalPage == 1) {
    $("#pager_right_button").addClass("-disabled");
  }
  if (totalPage == 0) {
    $("#page_container").addClass("no_display");
  }else{
    $("#page_container").removeClass("no_display");
  }
  if (totalPage > 1) {
    $("#pager_right_button").removeClass("-disabled");
  }
}
