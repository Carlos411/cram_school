$(function(){
  getOutlineRoot(g_obj.ec_id, g_obj.m_i_ot_id);
});


/**
 * 取得大綱根節點資料
 */
function getOutlineRoot(ecId, otId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/root?ec_id=' + ecId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $.each(response.data, function( index, item ) {
         // console.log(item.ol_id);
         // console.log(item);
         //getOutlineNextNode(item.ol_id, otId);
         if(index == 0){
           getOutlineNextNode(item.ol_id, otId);
         }
         //return false;
      });
    }
  });
}



/**
 * 取得領域資料
 */
function getOutlineNextNode(olId, otId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        var html = "";

        $.each(response.data, function( index, item ) {
           // console.log(item.ol_id);
           // console.log(item.ol_display_name);
           html += '<li><a href="./m_i_subject.html?ol_id=' + item.ol_id + '&field_name=' + item.ol_display_name + '"><img src="'+ item.ol_img +'" class="bg_img"><span class="-text">' + item.ol_display_name + '</span></a></li>';
        });

        $(".tab_content_list").html(html);
      }else{
        $(".tab_content_list").html("");
      }
    }
  });
}
