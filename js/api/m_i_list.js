var olId;
var unitOlId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterName;
var partName;

$(function(){

  // 取得頁面間傳遞參數
  olId = $.url('?ol_id');
  unitOlId = $.url('?unit_ol_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');
  versionName = $.url('?version_name');
  gradeGroupName = $.url('?grade_group_name');
  chapterName = $.url('?chapter_name');
  partName = $.url('?part_name');

  // 設定導覽列
  checkHeaderTextAfterUnit(unitOlId);

  $("#header_field").text(fieldName);
  $("#header_subject").text(subjectName);
  $("#header_grade_group").text(versionName + gradeGroupName);

  $("#header_unit").attr("href", "./m_i_unit.html?ol_id=" + unitOlId + "&field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + versionName + "&grade_group_name=" + gradeGroupName);

  $("#main_title").text(subjectName + " " + ((versionName)?versionName + " " : "") + gradeGroupName + " " + chapterName + " " + partName);

  // 設定導覽列連結
  setHeaderGradeGroupHref(unitOlId);

  // 取得互動實驗列表
  getInteractionList();

  // 最近觀看
  //get_video_recent("video_recent_item_list");
});

// 取得互動實驗列表
function getInteractionList(){
  $.ajax({
    url: g_obj.domain + '/api/experiment/user?ol_id=' + olId,
    type: 'get',
    // data: data,
    dataType: 'json',
    headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      if(res.data.length > 0){
        var item_list_interaction_html = '';
        $.each(res.data, function(i, item){
          item_list_interaction_html += '<li class="' + ( (i > 14 ? "no_display" : "") ) + '">';
          item_list_interaction_html +=   '<a href="' + item.eps_path + '" target="_blank" class="item_link" data-eps-id="' + item.eps_id + '">';
          //item_list_interaction_html +=     '<img src="http://via.placeholder.com/190x120" class="link_img">';
          item_list_interaction_html +=     '<div class="desc_block">';
          //item_list_interaction_html +=       '<p class="para1">第一章</p>';
          item_list_interaction_html +=       '<p class="para2">' + item.eps_name + '</p>';
          item_list_interaction_html +=     '</div>';
          item_list_interaction_html +=   '</a>';
          item_list_interaction_html +=   '<a href="javascript:;" onclick="javascript:addRemoveCollection(this); return false;" class="tag_ok" id="tag_ok_' + item.eps_id + '" data-eps-id="' + item.eps_id + '"><img src="./images/icon/icon-tag-ok.svg" class="-plus"><img src="./images/icon/icon-tag-cancel.svg" class="-cancel"></a>';
          item_list_interaction_html += '</li>';
        });
        $("#item_list_interaction").html(item_list_interaction_html);

        // 取得各互動實驗的收藏狀態
        $.each(res.data, function(i, item){
			getIListUserCollection(item.eps_id);
        });

        // 設定分頁
        setPager();
      }else{
        $("#item_list_interaction").html("");
        $("#page_container").addClass("no_display");
      }
    }
  });
}

// 取得各互動實驗的收藏狀態
function getIListUserCollection(eps_id) {
	$.get( g_obj.domain + "/api/usercollection?token=" + Cookies.get('u_t') + "&ot_id=" + g_obj.m_i_ot_id + "&ol_id=" + olId + "&eps_id=" + eps_id, function( res ) {
		if(typeof res.data.uc_id != 'undefined') { // 表示目前有收藏
			$("#tag_ok_" + eps_id).attr("data-status", "1").attr("data-eps_id", eps_id).attr("data-uc-id", res.data.uc_id).addClass("-on");
		}else { // 表示目前無收藏
			$("#tag_ok_" + eps_id).attr("data-status", "0").attr("data-eps_id", eps_id).removeAttr("data-uc-id").removeClass("-on");
		}
	});
}

/**
 * 設定分頁
 */
function setPager() {
  var totalCount = $("#item_list_interaction").children("li").length;
  var totalPage = Math.ceil(totalCount / 15);

  $("#total_page").html(str_pad(totalPage, 2, 0));
  if (totalPage == 1) {
    $("#pager_right_button").addClass("-disabled");
  }
  if (totalPage > 1) {
    $("#pager_right_button").removeClass("-disabled");
  }
}
