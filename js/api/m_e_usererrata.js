var ol_id;

$(function(){
	// 取得頁面間傳遞參數
	ol_id = $.url('?ol_id');
	if (typeof ol_id == "undefined") ol_id = "";
});

/**
 * 新增勘誤
 */
function newUserErrata(imgBase64, content, ot_id, t_ol_id, tq_id) {
	var rtnStatus = false;
	var fData = new FormData();
	fData.append("token", Cookies.get('u_t'));
	fData.append("ol_id", (t_ol_id != "") ? t_ol_id : ((ol_id == "" && $('#tmp_ol_id').length > 0) ? $('#tmp_ol_id').val() : ol_id));
	fData.append("ot_id", ot_id);
	fData.append("dv_id", "4");
	fData.append("ue_content", content);
	fData.append("ue_img_base64", imgBase64);
	if (tq_id != "") fData.append("tq_id", tq_id);
	$.ajax({
		url: g_obj.domain + '/api/userErrata/student',
		type: 'post',
		data: fData,
		dataType: 'json',
		async: false,
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					rtnStatus = true;
					break;
				default:
					alert(response.msg);
					/*
						2009: // 大綱id無效
						2002: // 大綱類型id無效
						3800: // 裝置id無效
						3901: // 學生勘誤內容未填
						3902: // 學生勘誤內容長度過長
						3903: // 學生勘誤圖片不為圖片檔
					*/
					break;
			}
		}
	});
	return rtnStatus;
}