$(function(){
	// 取得頁面間傳遞參數
});

/**
 * 刪除圖片
 */
function delUserclassImg() {
	if (confirm("確定要刪除嗎?")) {
		var fData = new FormData();
		fData.append("token", Cookies.get('u_t'));
		$.ajax({
			url: g_obj.domain + '/api/userclass/img',
			type: 'DELETE',
			data: fData,
			headers: {
				"token": Cookies.get('u_t'),
				"Content-Type": "application/x-www-form-urlencoded"
			},
			dataType: 'json',
			processData: false,
			contentType: false,
			statusCode: {
				200: function (response) {
					// console.log(response);
					// 成功
				}
			},
			success: function(response){
				console.log(response);
				switch (response.status) {
					case 1000:
						// alert(response.msg);
						window.location.reload();
						break;
					default:
						alert(response.msg);
						/*
							3001: // 使用者id無效
						*/
						break;
				}
			}
		});	
	}
}

/**
 * 取課表圖片
 */
function getUserclassImg() {
	$.ajax({
		url: g_obj.domain + '/api/userclass/img',
		type: 'GET',
		data: "token=" + Cookies.get('u_t'),
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					var curriculumImage = response.data["ucli_path"];
					if (typeof curriculumImage == "undefined" || curriculumImage == "") {
						getUserclass();
						$("#divCurriculumImage").hide();
						$("#divCurriculumTable").show();
						$(".btn_update_txt").show();
					}
					else {
						$("#curriculumImage").attr("src", curriculumImage);
						$("#divCurriculumImage").show();
						$("#divCurriculumTable").hide();
						$(".btn_update_txt").hide();
					}
					break;
				default:
					alert(response.msg);
					break;
			}
		}
	});
}
/**
 * 取課表文字
 */
function getUserclass() {
	$.ajax({
		url: g_obj.domain + '/api/userclass',
		type: 'GET',
		data: "token=" + Cookies.get('u_t'),
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					var curriculumData = response.data;
					for(var i = 0; i < curriculumData.length; i++) {
						$("#txt_cur_" + (curriculumData[i]["ucls_weekday"] + 1) + "_" + (curriculumData[i]["ucls_class"] + 1)).val(curriculumData[i]["ucls_name"]);
					}
					break;
				default:
					alert(response.msg);
					break;
			}
		}
	});
}


/**
 * 新增課表圖片
 */
function newUserclassImg() {
	var fData = new FormData($("#submit_userclass")[0]);	// 表單名稱請依實際修改
	fData.append("token", Cookies.get('u_t'));
	$.ajax({
		url: g_obj.domain + '/api/userclass/img',
		type: 'post',
		data: fData,
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// alert("新增完成");
					window.location.reload();
					break;
				default:
					alert(response.msg);
					/*
						5004: // 使用者課表圖片未上傳
						5005: // 使用者課表圖片不為圖片檔
					*/
					break;
			}
		}
	});
}

/**
 * 新增課表
 */
function newUserclass() {
	var token = Cookies.get('u_t');
	var userclases = [];
	// 單筆資料
	// var userclase = { ucls_weekday, ucls_class, ucls_name };
	// userclases.push(userclase);
	// 取出全部課表
	for (var j = 1; j <= 7; j++) {
		for (var i = 1; i <= 11; i++) {
			var ucls_weekday = j - 1;
			var ucls_class = i - 1;
			var ucls_name = $("#txt_cur_" + j + "_" + i).val();
			var userclase = { ucls_weekday, ucls_class, ucls_name };
			userclases.push(userclase);
		}
	}
	var olIDArray = { token, userclases };
	var myJSON = JSON.stringify(olIDArray);
	/* 參考 JSON: {"token":"testToken","userclases":[{"ucls_weekday":1,"ucls_class":10,"ucls_name":"test"}]} */
	$.ajax({
		url: g_obj.domain + '/api/userclass',
		type: 'post',
		data: myJSON,
		headers: {
            "token": Cookies.get('u_t'),
            "Content-Type": "application/json"
        },
		dataType: 'json',
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// alert(response.msg);
					window.location.reload();
					break;
				default:
					alert(response.msg);
					/*
						5000: // 使用者課表id無效
						5001: // 使用者課表星期無效
						5002: // 使用者課表時間無效
						5003: // 使用者課程名稱過長
					*/
					break;
			}
		}
	});
}