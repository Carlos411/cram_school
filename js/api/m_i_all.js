$(function(){
  getExperimentRecent();
});

// 取得最近互動實驗
function getExperimentRecent(){
  $.ajax({
    url: g_obj.domain + '/api/experiment/recent',
    type: 'get',
    // data: data,
    dataType: 'json',
    headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      if(res.data.length > 0){
        var item_list_recently_view_html = '';
        $.each(res.data, function(i, item){
          item_list_recently_view_html += '<li>';
          item_list_recently_view_html +=   '<a href="' + item.eps_path + '" target="_blank" data-eps-id="' + item.eps_id + '" class="item_link">';
          item_list_recently_view_html +=     '<img src="http://via.placeholder.com/190x120" class="link_img">';
          item_list_recently_view_html +=     '<div class="desc_block">';
          item_list_recently_view_html +=       '<p class="para1">' + (item.ol_display_name).split("/")[5] + '</p>';
          item_list_recently_view_html +=       '<p class="para2">' + (item.ol_display_name).split("/")[6] + '</p>';
          item_list_recently_view_html +=     '</div>';
          item_list_recently_view_html +=   '</a>';
          item_list_recently_view_html += '</li>';
        });
        $("#item_list_recently_view").html(item_list_recently_view_html);
      }else{
        $("#item_list_recently_view").html("");
      }
    }
  });
}
