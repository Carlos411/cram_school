$(function(){
	// 取得頁面間傳遞參數
	
});

/**
 * 新增收藏
 */
 var tmp_uc_id = "";
function newUserCollection(ot_id, ol_id, od_id, tq_id, ho_id, eps_id, uc_video_from) {
	var boolRtn = false;
	var fData = new FormData();
	fData.append("token", Cookies.get('u_t'));
	if (ot_id != "") fData.append("ot_id", ot_id);
	if (ol_id != "") fData.append("ol_id", ol_id);
	if (od_id != "") fData.append("od_id", od_id);
	if (tq_id != "") fData.append("tq_id", tq_id);
	if (ho_id != "") fData.append("ho_id", ho_id);
	if (eps_id != "") fData.append("eps_id", eps_id);
	if (uc_video_from != "") fData.append("uc_video_from", uc_video_from);
	$.ajax({
		url: g_obj.domain + '/api/usercollection',
		type: 'post',
		data: fData,
		dataType: 'json',
		async: false,
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					tmp_uc_id = response.data.uc_id;
					boolRtn = true;
					break;
				default:
					// alert(response.msg);
					/*
						2002: // 大綱類型id無效
						2009: // 大綱id無效
						2305: // 知識點id無效
						2800: // 題庫id無效
						2900: // 講義id無效
						7001: // 使用者收藏秒數無效
					*/
					break;
			}
		}
	});
	return boolRtn;
}

/**
 * 移除收藏
 */
function delUserCollection(uc_id) {
	var boolRtn = false;
	$.ajax({
		url: g_obj.domain + '/api/usercollection',
		type: 'DELETE',
		data: "uc_id=" + uc_id,
		headers: {
            "token": Cookies.get('u_t'),
            "Content-Type": "application/x-www-form-urlencoded"
        },
		dataType: 'json',
		async: false,
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					boolRtn = true;
					break;
				default:
					// alert(response.msg);
					/*
						2002: // 大綱類型id無效
						2009: // 大綱id無效
						2305: // 知識點id無效
						2800: // 題庫id無效
						2900: // 講義id無效
						7001: // 使用者收藏秒數無效
					*/
					break;
			}
		}
	});
	return boolRtn;
}