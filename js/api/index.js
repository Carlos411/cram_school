$(function(){
  $.ajax({
    url: g_obj.domain + '/api/post?ec_id=' + g_obj.ec_id + '&token=' + Cookies.get('u_t'),
    type: 'get',
    //data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        //console.log(response);
      },

      422: function (response) {
        //console.log(response.responseJSON.error);
      }
    },
    success: function(data){
      if(data.data.length > 0){
        var list_html = '';
        var modal_html = '';
        var pages = Math.ceil(data.data.length / 5);
        $(".page_number_block .total_page").html(str_pad(pages, 2, 0));
        if(pages == 1){
          $("button.btn_news.btn_right").addClass("-disabled");
        }
        $.each(data.data, function(i, data){
          if (i<=4){
            list_html += '<li><a href="#news_' + (i+1) + '" rel="modal:open" class="item_block">' + data.ps_title + '</a></li>';
          }else{
            list_html += '<li class="no_display"><a href="#news_' + (i+1) + '" rel="modal:open" class="item_block">' + data.ps_title + '</a></li>';
          }
          modal_html += '<div id="news_' + (i+1) +'" class="modal"><div class="modal_header">訊息公告<a href="#" rel="modal:close" class="modal_close">&times;</a></div><div class="modal_content"><h1 class="modal_title">' + data.ps_title + '</h1><div class="para_content_parent"><p>' + data.ps_content.replace(/\r\n|\n|\r/gm, '<br />') + '</p></div></div><div class="modal_footer"><a href="' + data.ps_link + '" class="other_link" target="_blank">' + data.ps_link_title + '</a></div></div>';
        });
        $("ul.news_list").html(list_html);
        $('#news_modal').html(modal_html);

        $.each(data.data, function(i, item){
          //console.log(item.ps_id)
          $.ajax({
            url:  g_obj.domain + '/api/post/file?ps_id=' + item.ps_id + '&token=' + Cookies.get('u_t'),
            type: 'get',
            dataType: 'json',
            success: function(data){
              if(data.data.length > 0){
                file_html = '<a href="#" class="btn_icon_download" data-ps-id="' + item.ps_id + '"><img src="./images/icon/btn-icon-download.svg"></a>';
                $('#news_' + (i+1) + ' .modal_footer').append(file_html);
              }
            }
          });

        });


      }
    }
  });
  $.ajax({
    url:  g_obj.domain + '/api/banner?ec_id=' + g_obj.ec_id + '&token=' + Cookies.get('u_t'),
    type: 'get',
    dataType: 'json',
    success: function(data){
      if(data.data.length > 0){
        var html = '';
        $.each(data.data, function(i, data){
          html += '<div><a href="' + data.bn_link + '" class="item_link" target="_blank"><img src="' + data.bn_img + '" class="item_img"></a></div>';
        });
        $("div.slick_slider").html(html);

        // 輪播外掛的執行
        $('.slick_slider').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          autoplay: true,
          autoplaySpeed: 3000,
          pauseOnHover: false,
          pauseOnFocus: false
        });
        $('.slick_slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
          //$('.slick_slider .slick-dots li').removeClass('slick-active').attr('aria-hidden','true');
          $('.slick_slider .slick-dots li button').focus(function() {
            this.blur();
          });
          $('.slick_slider .slick-arrow').focus(function() {
            this.blur();
          });
        });
      }
    }
  });
});
