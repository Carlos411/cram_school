﻿var olId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterCount;
var totalCount = 0;
var totalContent = "";
//var chapterData;

$(function(){
  // 取得頁面間傳遞參數
  olId = $.url('?ol_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');
  versionName = $.url('?version_name');
  gradeGroupName = $.url('?grade_group_name');

  // 設定導覽列
  checkHeaderTextUnit(olId);
  //$("#header_field").text(fieldName);
  //$("#header_subject").text(subjectName);
  //$("#header_grade_group").text(versionName + gradeGroupName);
  $("#header_unit").attr("href", "#");

  getChapterAndPart(olId, g_obj.db_class_practice_ot_id);

  // 設定導覽列連結
  setHeaderGradeGroupHref(olId);

  // 最近測驗結果
  getTestRecordRecent("recently_view_item_list", 2);

  left_aside_link(3); // 更換大類別連結
});

// 取得章和節資料
function getChapterAndPart(olId, otId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/nextTwoNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        $.each(response.data, function( index, item ) {
           totalContent += '<li class="' + (totalCount <= 14 ? "" : "no_display") + '"><a href="./q_db_exam.html?ol_id=' + item.ol_id + '&unit_ol_id=' + olId + '&field_name=' + fieldName + '&subject_name=' + subjectName + '&version_name=' + versionName + '&grade_group_name=' + gradeGroupName + '&chapter_name=' + item.ol_display_name_1 + '&part_name=' + item.ol_display_name_2 + '" class="item_link"><img src="' + item.ol_img + '" class="link_img"><div class="desc_block"><p class="para1">' + item.ol_display_name_1 + '</p><p class="para2">' + item.ol_display_name_2 + '</p></div></a></li>';
           totalCount++;
        });
        setPager();
      } else {
        $("#item_list").find("span.-loading").text("此課程無隨堂練習");
        $("#total_page").html(str_pad(1, 2, 0));
        $("#pager_right_button").addClass("-disabled");
      }
    }
  });
}

/**
 * 設定分頁
 */
function setPager() {
  $("#item_list").html(totalContent);
  var totalPage = Math.ceil(totalCount / 15);
  $("#total_page").html(str_pad(totalPage, 2, 0));
  if (totalPage == 1) {
    $("#pager_right_button").addClass("-disabled");
  }
  if (totalPage > 1) {
    $("#pager_right_button").removeClass("-disabled");
  }
}
