/**
 * Header縮寫：科目頁面 call 這支
 */
 function checkHeaderTextField(ol_id){
   $.get( g_obj.domain + "/api/outline?ol_id=" + ol_id + '&token=' + Cookies.get('u_t'), function( res ) {
     $("#header_field").text(res.data[0].ol_abbreviation);
   });
 }

 /**
  * Header縮寫：年級群組頁面 call 這支
  */
function checkHeaderTextGradeGroup(ol_id){
  $.get( g_obj.domain + "/api/outline?ol_id=" + ol_id + '&token=' + Cookies.get('u_t'), function( res ) {
    $("#header_subject").text(res.data[0].ol_abbreviation);
  });
  $.get( g_obj.domain + "/api/outline/lastNode?ol_id=" + ol_id + '&token=' + Cookies.get('u_t'), function( res ) {
    $("#header_field").text(res.data[0].ol_abbreviation);
  });
}

/**
 * Header縮寫：單元頁面 call 這支
 */
 function checkHeaderTextUnit(ol_id){
   $.get( g_obj.domain + "/api/outline?ol_id=" + ol_id + '&token=' + Cookies.get('u_t'), function( res ) {
     $("#header_grade_group").text(versionName + res.data[0].ol_abbreviation);
   });
   $.get( g_obj.domain + "/api/outline/lastNode?ol_id=" + ol_id + '&token=' + Cookies.get('u_t'), function( res1 ) {
     $.get( g_obj.domain + "/api/outline/lastNode?ol_id=" + res1.data[0].ol_id + '&token=' + Cookies.get('u_t'), function( res2 ) {
       $("#header_subject").text(res2.data[0].ol_abbreviation);
       $.get( g_obj.domain + "/api/outline/lastNode?ol_id=" + res2.data[0].ol_id + '&token=' + Cookies.get('u_t'), function( res3 ) {
         $("#header_field").text(res3.data[0].ol_abbreviation);
       });
     });
   });
 }

 /**
  * Header縮寫：單元之後的頁面 call 這支
  */
  function checkHeaderTextAfterUnit(ol_id){
    $.get( g_obj.domain + "/api/outline?ol_id=" + ol_id + '&token=' + Cookies.get('u_t'), function( res ) {
      //console.log(res);
      $("#header_grade_group").text(versionName + res.data[0].ol_abbreviation);
    });

    $.get( g_obj.domain + "/api/outline/lastNode?ol_id=" + ol_id + '&token=' + Cookies.get('u_t'), function( res1 ) {
      $.get( g_obj.domain + "/api/outline/lastNode?ol_id=" + res1.data[0].ol_id + '&token=' + Cookies.get('u_t'), function( res2 ) {
        $("#header_subject").text(res2.data[0].ol_abbreviation);
        $.get( g_obj.domain + "/api/outline/lastNode?ol_id=" + res2.data[0].ol_id + '&token=' + Cookies.get('u_t'), function( res3 ) {
          $("#header_field").text(res3.data[0].ol_abbreviation);
        });
      });
    });
  }
