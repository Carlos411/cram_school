﻿// global variable
var g_obj = {
  domain: "https://test.azul.com.tw:15200",
  //domain: "http://sb1.samebest.com.tw", // for production
  ec_id: 3, // 高中版
  teacher_ot_id: 1, // 名師教學
  question_ot_id: 6, // 瀏覽問題
  ask_ot_id: 7, // 我要發問
  m_e_ot_id: 4, // 電子講義
  exam_live_stream_ot_id: 2, //段考直播
  db_practice_ot_id: 3, //百萬題庫
  db_class_practice_ot_id: 11, // 隨堂練習
  db_test_history_ot_id : 5, //歷屆試題
  my_learn_notes_ot_id: 8, // 我的筆記
  m_i_ot_id: 9, // 互動實驗
  dv_id: 4 // web
};
//getTokenRefresh();

// 更新 token 有效時間
function getTokenRefresh(){
  if(location.pathname != "/login.html" && location.pathname != "/login_parents.html"){
    //if(Cookies.get("u_t") != undefined){
      $.get( g_obj.domain + "/api/token/refresh?token=" + Cookies.get('u_t'), function( res ) {
        /*
        1100 token 無效
        1101 權限不足
        1102 token 過期
        1103 已有其他裝置登入
        */
        if(res.status == 1100 || res.status == 1101 || res.status == 1102 || res.status == 1103){
          //Cookies.set('u_t', res.data.token);
          //location.reload();
          //location.href = "./login.html";
        }
      });
    //}
  }
}

// 判斷使用者收藏
function getUsercollection(ot_id, ol_id, od_id, tq_id, ho_id, eps_id){
  // console.log("ot_id: " + ot_id);
  // console.log("ol_id: " + ol_id);
  // console.log("od_id: " + od_id);
  // console.log("tq_id: " + tq_id);
  // console.log("ho_id: " + ho_id);
  // console.log("eps_id: " + eps_id);
  if(od_id != ""){ // 知識點 id：單一名師教學、單一段考直播
    $.get( g_obj.domain + "/api/usercollection?token=" + Cookies.get('u_t') + "&ot_id=" + ot_id + "&ol_id=" + ol_id + "&od_id=" + od_id, function( res ) {
      // if(res.data.length > 0){ // 表示目前有收藏
	  if(typeof res.data.uc_id != 'undefined') { // 表示目前有收藏
        $("#tag_plus").attr("data-status", "1").attr("data-od_id", od_id).attr("data-uc-id", res.data.uc_id).addClass("-on");
      }
	  else { // 表示目前無收藏
        $("#tag_plus").attr("data-status", "0").attr("data-od_id", od_id).removeAttr("data-uc-id").removeClass("-on");
      }
    });
  }
  if(tq_id != ""){ // 題庫 id
    $.get( g_obj.domain + "/api/usercollection?token=" + Cookies.get('u_t') + "&ot_id=" + ot_id + "&ol_id=" + ol_id + "&tq_id=" + tq_id, function( res ) {
      // if(res.data.length > 0){ // 表示目前有收藏
	  if(typeof res.data.uc_id != 'undefined') { // 表示目前有收藏
        $("#tag_plus").attr("data-status", "1").attr("data-tq_id", tq_id).attr("data-uc-id", res.data.uc_id).addClass("-on");
      }
	  else {
        $("#tag_plus").attr("data-status", "0").attr("data-tq_id", tq_id).removeAttr("data-uc-id").removeClass("-on");
      }
    });
  }
  if(ho_id != ""){ // 講義
    $.get( g_obj.domain + "/api/usercollection?token=" + Cookies.get('u_t') + "&ot_id=" + ot_id + "&ol_id=" + ol_id + "&ho_id=" + ho_id, function( res ) {
      // if(res.data.length > 0){ // 表示目前有收藏
	  if(typeof res.data.uc_id != 'undefined') { // 表示目前有收藏
        $("#tag_plus").attr("data-status", "1").attr("data-ho_id", ho_id).attr("data-uc-id", res.data.uc_id).addClass("-on");
      }
	  else {
        $("#tag_plus").attr("data-status", "0").attr("data-ho_id", ho_id).removeAttr("data-uc-id").removeClass("-on");
      }
    });

  }
  if(eps_id != ""){ // 互動實驗 id

  }
}

// 判斷使用者收藏(針對考卷測驗q_db_test.html)
function getUsercollectionForTest(ot_id, ol_id, tq_id, tag_plus_id){
	$.get( g_obj.domain + "/api/usercollection?token=" + Cookies.get('u_t') + "&ot_id=" + ot_id + "&ol_id=" + ol_id + "&tq_id=" + tq_id, function( res ) {
		console.log(res);
		// if(res.data.length > 0){ // 表示目前有收藏
		if (typeof res.data.uc_id != 'undefined') { // 表示目前有收藏
			if ($("#" + tag_plus_id).length > 0) {
				$("#" + tag_plus_id).attr("data-status", "1").attr("data-tq_id", tq_id).attr("data-uc-id", res.data.uc_id).addClass("-on");
			}
			else {
				$("#tag_plus").attr("data-status", "1").attr("data-tq_id", tq_id).attr("data-uc-id", res.data.uc_id).addClass("-on");
			}
		}
		else {
			if ($("#" + tag_plus_id).length > 0) {
				$("#" + tag_plus_id).attr("data-status", "0").attr("data-tq_id", tq_id).removeAttr("data-uc-id").removeClass("-on");
			}
			else {
				$("#tag_plus").attr("data-status", "0").attr("data-tq_id", tq_id).removeAttr("data-uc-id").removeClass("-on");
			}
		}
	});
}

// jQuery + XMLHttpRequest 取得 Binary 檔案
$.ajaxTransport("+binary", function(options, originalOptions, jqXHR){
    // check for conditions and support for blob / arraybuffer response type
    if (window.FormData && ((options.dataType && (options.dataType == 'binary')) || (options.data && ((window.ArrayBuffer && options.data instanceof ArrayBuffer) || (window.Blob && options.data instanceof Blob))))) {
        return {
			// create new XMLHttpRequest
			send: function(headers, callback){
				// setup all variables
				var xhr = new XMLHttpRequest(),
					url = options.url,
					type = options.type,
					async = options.async || true,
					// blob or arraybuffer. Default is blob
					dataType = options.responseType || "blob",
					data = options.data || null,
					username = options.username || null,
					password = options.password || null;

				xhr.addEventListener('load', function(){
					var data = {};
					data[options.dataType] = xhr.response;
					// make callback and send data
					callback(xhr.status, xhr.statusText, data, xhr.getAllResponseHeaders());
				});

				xhr.open(type, url, async, username, password);

				// setup custom headers
				for (var i in headers ) {
					xhr.setRequestHeader(i, headers[i] );
				}

                xhr.responseType = dataType;
                xhr.send(data);
            },
            abort: function(){
                jqXHR.abort();
            }
        };
    }
});

// 側邊欄連結
function left_aside_link(level){
  switch(level){
    case 1:
      $("#f_teacher").attr("href", "./f_teacher_subject.html?ol_id=" + olId + "&" + "field_name=" + fieldName);         // 名師教學
      $("#q_db_practice").attr("href", "./q_db_practice_subject.html?ol_id=" + olId + "&" + "field_name=" + fieldName); // 隨堂練習
      $("#q_db_exam").attr("href", "./q_db_exam_subject.html?ol_id=" + olId + "&" + "field_name=" + fieldName);         // 考卷測驗
      $("#m_e").attr("href", "./m_e_subject.html?ol_id=" + olId + "&" + "field_name=" + fieldName);                     // 電子講義
      $("#m_i").attr("href", "./m_i_subject.html?ol_id=" + olId + "&" + "field_name=" + fieldName);                     // 互動實驗
      $("#p_question").attr("href", "./p_question_subject.html?ol_id=" + olId + "&" + "field_name=" + fieldName);       // 瀏覽問題
      $("#p_ask").attr("href", "./p_ask_subject.html?ol_id=" + olId + "&" + "field_name=" + fieldName);                 // 我要發問
      $("#my_learn").attr("href", "./my_learn_notes_subject.html?ol_id=" + olId + "&" + "field_name=" + fieldName);     // 我的筆記
      break;
    case 2:
      $("#f_teacher").attr("href","./f_teacher_grade_group.html?ol_id=" + olId + "&field_name=" + fieldName + "&subject_name=" + subjectName );              // 名師教學
      $("#q_db_practice").attr("href", "./q_db_practice_grade_group.html?ol_id=" + olId + "&" + "field_name=" + fieldName + "&subject_name=" + subjectName); // 隨堂練習
      $("#q_db_exam").attr("href", "./q_db_exam_grade_group.html?ol_id=" + olId + "&" + "field_name=" + fieldName + "&subject_name=" + subjectName);         // 考卷測驗
      $("#m_e").attr("href", "./m_e_grade_group.html?ol_id=" + olId + "&" + "field_name=" + fieldName + "&subject_name=" + subjectName);                     // 電子講義
      $("#m_i").attr("href", "./m_i_grade_group.html?ol_id=" + olId + "&" + "field_name=" + fieldName + "&subject_name=" + subjectName);                     // 互動實驗
      $("#p_question").attr("href", "./p_question_grade_group.html?ol_id=" + olId + "&" + "field_name=" + fieldName + "&subject_name=" + subjectName);       // 瀏覽問題
      $("#p_ask").attr("href", "./p_ask_grade_group.html?ol_id=" + olId + "&" + "field_name=" + fieldName + "&subject_name=" + subjectName);                 // 我要發問
      $("#my_learn").attr("href", "./my_learn_notes_grade_group.html?ol_id=" + olId + "&" + "field_name=" + fieldName + "&subject_name=" + subjectName);     // 我的筆記
      break;
    case 3:
      $("#f_teacher").attr("href","./f_teacher_unit.html?ol_id=" + olId + "&field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + (versionName == "不分版" ? "":versionName) + "&grade_group_name=" + gradeGroupName);               // 名師教學
      $("#q_db_practice").attr("href", "./q_db_practice_unit.html?ol_id=" + olId + "&" + "field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + (versionName == "不分版" ? "":versionName) + "&grade_group_name=" + gradeGroupName); // 隨堂練習
      $("#q_db_exam").attr("href", "./q_db_exam_unit.html?ol_id=" + olId + "&" + "field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + (versionName == "不分版" ? "":versionName) + "&grade_group_name=" + gradeGroupName);         // 考卷測驗
      $("#m_e").attr("href", "./m_e_unit.html?ol_id=" + olId + "&" + "field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + (versionName == "不分版" ? "":versionName) + "&grade_group_name=" + gradeGroupName);                     // 電子講義
      $("#m_i").attr("href", "./m_i_unit.html?ol_id=" + olId + "&" + "field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + (versionName == "不分版" ? "":versionName) + "&grade_group_name=" + gradeGroupName);                     // 互動實驗
      $("#p_question").attr("href", "./p_question_unit.html?ol_id=" + olId + "&" + "field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + (versionName == "不分版" ? "":versionName) + "&grade_group_name=" + gradeGroupName);       // 瀏覽問題
      $("#p_ask").attr("href", "./p_ask_unit.html?ol_id=" + olId + "&" + "field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + (versionName == "不分版" ? "":versionName) + "&grade_group_name=" + gradeGroupName);                 // 我要發問
      $("#my_learn").attr("href", "./my_learn_notes_unit.html?ol_id=" + olId + "&" + "field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + (versionName == "不分版" ? "":versionName) + "&grade_group_name=" + gradeGroupName);     // 我的筆記
      break;
    case 4:
      $("#p_question").attr("href", "./p_question_list.html?ol_id=" + olId + "&unit_ol_id=" + unitOlId + "&field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + (versionName == "不分版" ? "":versionName) + "&grade_group_name=" + gradeGroupName + "&chapter_name=" + chapterName + "&part_name=" + partName); // 我要發問
      $("#p_ask").attr("href", "./p_ask_question.html?ol_id=" + olId + "&unit_ol_id=" + unitOlId + "&field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + (versionName == "不分版" ? "":versionName) + "&grade_group_name=" + gradeGroupName + "&chapter_name=" + chapterName + "&part_name=" + partName); // 我要發問
      break;
    default:
  }
}

// 左邊補零，例：str_pad(1, 2, 0) 會輸出 01
function str_pad(input, length, padding){
  while((input = input.toString()).length + (padding = padding.toString()).length < length) {
    padding += padding;
  }
  return padding.substr(0, length - input.length) + input;
}
