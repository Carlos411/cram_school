var ol_id, tr_id;

$(function(){
	// 取得頁面間傳遞參數
	ol_id = $.url('?ol_id');
	if (typeof ol_id == "undefined") ol_id = "";
	tr_id = $.url('?tr_id');
	if (typeof tr_id == "undefined") tr_id = "";
});

// 取得大綱節點下測驗數量，依難度區分
var maxLevelQty_easy = 0;
var maxLevelQty_normal = 0;
var maxLevelQty_difficult = 0;
function testDifficultCount() {
	var token = Cookies.get('u_t');
	var ol_ids = [];
	var storage_ol_ids = (typeof window.localStorage["String_Check_Value"] == 'undefined') ? [] : window.localStorage["String_Check_Value"].split(",");
	for(var i = 0; i < storage_ol_ids.length; i++) {
		if (storage_ol_ids[i] != "") ol_ids.push(storage_ol_ids[i]);
	}
	if (ol_ids.length < 1) ol_ids.push(ol_id);
	var olIDArray = { token, ol_ids };
	var myJSON = JSON.stringify(olIDArray);

	console.log(myJSON);

	/* 參考 JSON: {"token":"user token", "ol_ids":[15,16]} */
	$.ajax({
		url: g_obj.domain + '/api/test/difficultCount',
		type: 'post',
		data: myJSON,
		dataType: 'json',
		async: false,
		contentType: "application/json; charset=utf-8",
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					$("#q_level_qty_easy").html(response.data["easy"] + "題");
					$("#q_level_qty_normal").html(response.data["normal"] + "題");
					$("#q_level_qty_difficult").html(response.data["difficult"] + "題");
					maxLevelQty_easy = parseInt(response.data["easy"], 10) || 0;
					maxLevelQty_normal = parseInt(response.data["normal"], 10) || 0;
					maxLevelQty_difficult = parseInt(response.data["difficult"], 10) || 0;
					setDifficult('normal');
					break;
				default:
					alert(response.msg);
					break;
			}
		}
	});
}

// 取得題目數量
function setDifficult(t) {
	var tmpLevelQty_easy = 0;
	var tmpLevelQty_normal = 0;
	var tmpLevelQty_difficult = 0;
	switch(t) {
		case "easy":
			tmpLevelQty_easy = 4;
			tmpLevelQty_normal = 5;
			tmpLevelQty_difficult = 1;
			break;
		case "normal":
			tmpLevelQty_easy = 2;
			tmpLevelQty_normal = 5;
			tmpLevelQty_difficult = 3;
			break;
		case "difficult":
			tmpLevelQty_easy = 1;
			tmpLevelQty_normal = 5;
			tmpLevelQty_difficult = 4;
			break;
	}
	$("#difficult_easy_qty").val((tmpLevelQty_easy <= maxLevelQty_easy) ? tmpLevelQty_easy : maxLevelQty_easy);
	$("#difficult_medium_qty").val((tmpLevelQty_normal <= maxLevelQty_normal) ? tmpLevelQty_normal : maxLevelQty_normal);
	$("#difficult_hard_qty").val((tmpLevelQty_difficult <= maxLevelQty_difficult) ? tmpLevelQty_difficult : maxLevelQty_difficult);
	resetButton();
}
function resetButton() {
	if (parseInt($("#difficult_easy_qty").val(), 10) + parseInt($("#difficult_medium_qty").val(), 10) + parseInt($("#difficult_hard_qty").val(), 10) >= 5) {
		$(".to_start_1").removeClass("-disabled");
	}
	else {
		$(".to_start_1").addClass("-disabled");
	}
	if (parseInt($("#difficult_easy_qty").val(), 10) + parseInt($("#difficult_medium_qty").val(), 10) + parseInt($("#difficult_hard_qty").val(), 10) >= 1) {
		$(".to_print_1").removeClass("-disabled");
	}
	else {
		$(".to_print_1").addClass("-disabled");
	}
}

// 取得大綱節點下題庫及答案
function testGet(easy, normal, difficult, tq_video_priority, not_yet_priority, random_5_single_choice, getHtml) {
	var token = Cookies.get('u_t');
	var ol_ids = [];
	var storage_ol_ids = (typeof window.localStorage["String_Check_Value"] == 'undefined') ? [] : window.localStorage["String_Check_Value"].split(",");
	for(var i = 0; i < storage_ol_ids.length; i++) {
		if (storage_ol_ids[i] != "") ol_ids.push(storage_ol_ids[i]);
	}
	if (ol_ids.length < 1 || random_5_single_choice == 1) ol_ids.push(ol_id);
	var has_html_content = 1;
	var olIDArray = { token, ol_ids, easy, normal, difficult, tq_video_priority, not_yet_priority, has_html_content};
	if (random_5_single_choice == 1) {
		olIDArray = { token, ol_ids, random_5_single_choice, has_html_content};
	}

	var rtnQuestion;
	var myJSON = JSON.stringify(olIDArray);
	/* 參考 JSON: {"ol_ids":[15,16],"easy":0,"normal":0,"difficult":0,"tq_video_priority":0,"not_yet_priority":0,"random_5_single_choice":0} */
	$.ajax({
		url: g_obj.domain + '/api/test/get',
		type: 'post',
		data: myJSON,
		dataType: 'json',
		async: false,
		contentType: "application/json; charset=utf-8",
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					var questionHtml = '', questionAnswerHtml = '', questionExplainHtml = '', questionCountHtml = '';
					for (var i = 0; i < response.data.length; i++) {
						// 取得題目
						var tmpQuestionHtml = response.data[i].tq_html_link;
						if (getHtml) {
							$.ajax({
								url: response.data[i].tq_html_link,
								async: false,
								success: function(rtnHtml){
									tmpQuestionHtml = rtnHtml;
								}
							});
							// 取得說明
							var tmpExplain = response.data[i].tq_explain;
							$.ajax({
								url: response.data[i].tq_explain,
								async: false,
								success: function(rtnHtml){
									tmpExplain = rtnHtml;
								}
							});
						}
						response.data[i].tq_html = tmpQuestionHtml;
						response.data[i].tq_explain_html = tmpExplain;
					}
					rtnQuestion = response.data;
					break;
				default:
					alert(response.msg);
					break;
			}
		}
	});
	return rtnQuestion;
}

// 新增測驗結果
function testRecord(tr_type, questions) {
	var boolStatus = false;
	// tr_type:0為錯題訂正, 1為考試測驗, 2為隨堂練習
	var token = Cookies.get('u_t');
	var olIDArray = { token, tr_type, questions };
	var myJSON = JSON.stringify(olIDArray);
	/* 參考 JSON: {"token": "testToken","tr_type": "1","questions": [{"tq_id": 1,"tqr_used_second": 10,"odt_id": 1,"answers": [{"ta_id": 2}]}]} */
	$.ajax({
		url: g_obj.domain + '/api/test/record',
		type: 'POST',
		data: myJSON,
		dataType: 'json',
		async: false,
		contentType: "application/json; charset=utf-8",
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					switch (tr_type) {
						case 1:
							window.location.href = "./my_learn_record_score.html?ol_id=" + ol_id + "&tr_id=" + response.data.tr_id + "&subject_name=" + $.url('?subject_name');
							break;
						case 2:
							window.location.href = "./q_db_practice_unit.html?ol_id=" + $.url('?unit_ol_id') + "&field_name=" + $.url('?field_name') + "&subject_name=" + $.url('?subject_name') + "&version_name=" + $.url('?version_name') + "&grade_group_name=" + $.url('?grade_group_name');
							break;
					}
					boolStatus = true;
					break;
				default:
					alert(response.msg);
					break;
			}
		}
	});
	return boolStatus;
}

// 取得測驗記錄 PDF
function testRecordPdf() {
	$.ajax({
		url: g_obj.domain + '/api/test/record/pdf?token=' + Cookies.get('u_t') + '&tr_id=' + tr_id,
		type: "GET",
		dataType: "binary",
		processData: false,
		success: function(response){
			// console.log(response);
			var binaryData = [];
			binaryData.push(response);
			var downloadUrl = window.URL.createObjectURL(new Blob(binaryData, {type: "application/pdf"}))
			var a = document.createElement("a");
			a.href = downloadUrl;
			a.download = "成績單_" + print_filename + "_" + new Date().getFullYear().toString().substr(-2) + ("0" + (new Date().getMonth() + 1).toString()).substr(-2) + ("0" + new Date().getDate().toString()).substr(-2) + ("0" + new Date().getHours().toString()).substr(-2) + ("0" + new Date().getMinutes().toString()).substr(-2) + ("0" + new Date().getSeconds().toString()).substr(-2) + ".pdf";
			document.body.appendChild(a);
			a.click();
		}
	});
	// window.open(g_obj.domain + '/api/test/record/pdf?token=' + Cookies.get('u_t') + '&tr_id=' + tr_id);
}

// 取得測驗題目 PDF
function testGetPdf(ol_ids, tq_ids, filetype, filename) {
	/* 參考 JSON: {"token": "testToken", "tq_ids": [87,88]} */
	$.ajax({
		// url: "https://test.azul.com.tw:15200/handout_ol_id_58.pdf",
		url: g_obj.domain + '/api/test/getPdf?token=' + Cookies.get('u_t') + '',
		type: "POST",
		data: '{"token": "' + Cookies.get('u_t') + '", "ol_ids": [' + ol_ids + '], "tq_ids": [' + tq_ids + ']}',
		dataType: "binary",
		contentType: "application/json; charset=utf-8",
		processData: false,
		success: function(response){
			var binaryData = [];
			binaryData.push(response);
			var downloadUrl = window.URL.createObjectURL(new Blob(binaryData, {type: "application/pdf"}))
			var a = document.createElement("a");
			a.href = downloadUrl;
			if (filename != "") {
				a.download = filetype + "_" + filename + "_" + new Date().getFullYear().toString().substr(-2) + ("0" + (new Date().getMonth() + 1).toString()).substr(-2) + ("0" + new Date().getDate().toString()).substr(-2) + ("0" + new Date().getHours().toString()).substr(-2) + ("0" + new Date().getMinutes().toString()).substr(-2) + ("0" + new Date().getSeconds().toString()).substr(-2) + ".pdf";
			}
			else {
				a.download = filetype + "_" + (($.url('?field_name') != undefined) ? $.url('?field_name') + "_" : "") + (($.url('?subject_name') != undefined) ? $.url('?subject_name') + "_" : "") + (($.url('?version_name') != undefined) ? $.url('?version_name') + "_" : "") + (($.url('?grade_group_name') != undefined) ? $.url('?grade_group_name') : "") + "_" + new Date().getFullYear().toString().substr(-2) + ("0" + (new Date().getMonth() + 1).toString()).substr(-2) + ("0" + new Date().getDate().toString()).substr(-2) + ("0" + new Date().getHours().toString()).substr(-2) + ("0" + new Date().getMinutes().toString()).substr(-2) + ("0" + new Date().getSeconds().toString()).substr(-2) + ".pdf";
			}
			document.body.appendChild(a);
			a.click();
		}
	});
}
