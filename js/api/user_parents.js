var parent_up_id = 0;
var parent_devices_upd_id = "";

$(function(){
	getUserParentInfo();
});

function getUserParentInfo(){
	$.ajax({
		url: g_obj.domain + '/api/user/parent?token=' + Cookies.get('u_t'),
		type: 'get',
		// data: data,
		dataType: 'json',
		//headers: { 'token': Cookies.get('u_t') },
		success: function(res){
			//console.log(res.data.up_name);
			//console.log(res);
			parent_up_id = res.data.up_id;	// 家長 ID
			parent_devices_upd_id = res.data.devices[0].upd_id;

			console.log(parent_devices_upd_id);

			getApiUserStudent(parent_up_id);

			$("#up_name_view").html(res.data.up_name); // 學生姓名
			//$("#us_img_tag").attr("src", res.data[0].us_img); // 使用者圖片路徑
			$("#up_is_male_view").html((res.data.up_is_male == 1 ? "男" : "女")); // 性別

			// 使用者生日
			//$("input.temp_birtyday").val(parseInt(((((res.data[0].us_birthday).split(" "))[0]).split("-"))[0]) + "/" + ((((res.data[0].us_birthday).split(" "))[0]).split("-"))[1] + "/" + ((((res.data[0].us_birthday).split(" "))[0]).split("-"))[2]);
			setting_temp_birtyday();
			//$("#birthY").val( parseInt(((((res.data.us_birthday).split(" "))[0]).split("-"))[0]) - 1911 );
			//$("#birthM").val( ((((res.data.us_birthday).split(" "))[0]).split("-"))[1] );
			//$("#birthD").val( ((((res.data.us_birthday).split(" "))[0]).split("-"))[2] );
			$("#up_cell_phone_view").html(res.data.up_cell_phone); // 使用者行動電話
			$("#up_phone_view").html(res.data.up_phone); // 市話
			$("#up_mail_view").html(res.data.up_mail); // 使用者電子郵件
			//$("#us_school").val(res.data[0].us_school); // 使用者學校
			// 使用者年級
			//$("#us_grade").html("<option value='" + res.data[0].us_grade + "'>" + res.data[0].us_grade + "</option>")
			//$("#us_grade").val(res.data[0].us_grade); // 使用者年級
			//$("#us_class").val(res.data[0].us_class); // 使用者班級
			$("#up_name").val(res.data.up_name); // 使用者家長姓名
			$("#up_cell_phone").val(res.data.up_cell_phone); // 使用者家長行動電話

			if(res.data.up_birthday != null){
				$("#up_birthday_view").html(res.data.up_birthday.substr(0, 10)); // 生日
			}
		}
	});

}
function getApiUserStudent(up_id) {
	// udp_unique 值為 box_num_1
	$.get( g_obj.domain + "/api/user/parent/student?up_id=" + up_id, function( res ) {
		var how_many_can_add = 3 - res.data.users.length;
		var user_html = '';
		if(res.data.users.length > 0) {
			$.each(res.data.users, function(i, item){
				user_html += '<li>';
				user_html +=   '<a href="javascript:;" onclick="javascript:changeStudent(' + item.us_id + '); return false;" class="student_item">';
				user_html +=     '<div class="item_block">';
				user_html +=       '<div class="profile_img_block">';
				if(item.us_img == ""){
					user_html +=         '<span class="avatar_img"><img src="../images/icon/icon-profile.svg" class="profile_img"></span>';
				}else{
					user_html +=         '<span class="avatar_img -add-border" style="background-image: url(' + item.us_img + ');"></span>';
				}
				user_html +=       '</div>';
				user_html +=       '<p class="para">' + item.us_name + '</p>';
				user_html +=     '</div>';
				user_html +=   '</a>';
				user_html += '</li>';
			});
		}

		if(how_many_can_add > 0){
			for(var i = 1 ; i <= how_many_can_add ; i++){
				user_html += '<li>';
	      user_html +=   '<a href="#" class="add_item">';
	      user_html +=     '<div class="item_block">';
				user_html +=       '<div class="profile_img_block">';
				user_html +=         '<span class="avatar_img"><img src="./images/icon/btn_add_item.svg"></span>';
	      user_html +=       '</div>';
	      user_html +=       '<p class="para">新增</p>';
	      user_html +=     '</div>';
	      user_html +=   '</a>';
	      user_html += '</li>';
			}
		}
		$("#students_list").html(user_html);
	});
}

function setting_temp_birtyday(){
  $('input.temp_birtyday').each(function(){
    $(this).dateRangePicker({
      //separator : ' 到 ',
      language: 'tc',
      format: 'YYYY/MM/DD',
      autoClose: true,
      singleDate : true,
      showShortcuts: false,
      singleMonth: true,
      yearSelect: function(current) {
        return [1900, (new Date()).getFullYear()];
      },
      inline:true,
      container: "div.invisible_birthday_block",
      alwaysOpen: true
    }).bind('datepicker-change',function(event,obj){
      var user_new_birthday = new Date(obj.date1);
      $('.-birthday.-year').val(user_new_birthday.getFullYear());
      $('.-birthday.-month').val(str_pad(user_new_birthday.getMonth() + 1, 2, 0));
      $('.-birthday.-date').val(str_pad(user_new_birthday.getDate(), 2, 0));
      $("div.invisible_birthday_block").hide();
    });
  });
}
