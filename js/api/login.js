$(function(){
  // 開發者模式 token
  // Cookies.set('u_t', 'eyJpdiI6InBsYnJ6RlZ6TEIrT09qblY0aDlRbmc9PSIsInZhbHVlIjoiU1kyNFwvMDNsbFZYc293N0ptSXM3d2VcL080ZlRpMmR5TXB5T1F1WXBueVFFbzQrUldxRGljSW5HWTNpNEJoOXdDUmQ5NUc1MXVLdTUzTzhLOUFucnEwcHRzVTVzRGRKQlp2UFZXK2VwcnNsRWV2a0NKenVlMFY0TFNJSmd0NVhGcFQxMHJkNm5yNGcwQ0o4aURcL2JQMFI0MnphcUpCTFVpaFI5RlBEUjBVbHJrMEFyVmJjNTRqVTg2elhyV29WYnBiIiwibWFjIjoiNmFlZjA0YzBlOTA5ZjgyZmMzYjViNjAzZDJiMTY3YzEzNTlmMjc3ODBiMTY3NTgyNTc3MTg0N2M3ODg3YzBkMCJ9');

  $("#login_action").on("click", function(e){
    e.preventDefault();
    var up_name = $("#up_name").val();
    var us_name = $("#us_name").val();
    var us_password = $("#us_password").val();
    //console.log(up_name + " " + us_name + " " + us_password);

    if(up_name == "" || us_name == "" || us_password == ""){
      $("#login_fail_modal").find("span.-head").html("登入失敗");
      var show_content = "";
      if(us_password == ""){
        show_content = "學生密碼未填";
      }
      if(us_name == ""){
        show_content = "學生姓名未填";
      }
      if(up_name == ""){
        show_content = "家長姓名未填";
      }
      $("#login_fail_modal").find("span.-content").html(show_content);
      $("#login_fail_modal").modal();
    }else{
      $.ajax({
        url: g_obj.domain + '/api/user/student/signIn?ec_id=' + g_obj.ec_id + '&dv_id=' + g_obj.dv_id + '&up_name=' + up_name + '&us_name=' + us_name + '&us_password=' + us_password,
        type: 'get',
        //data: data,
        dataType: 'json',
        statusCode: {
          200: function (response) {
            //console.log(response);
          },

          422: function (response) {
            //console.log(response.responseJSON.error);
          }
        },
        success: function(res){
          if(parseInt(res.status) == 1000){
            //alert(res.msg[0]);
            //alert("登入成功");
            Cookies.set('u_t', res.data.token);
            location.href = "./index.html";
          }else{
            if(parseInt(res.status) == 3023){
              $("#login_fail_modal").find("span.-head").html("密碼錯誤");
            }else{
              $("#login_fail_modal").find("span.-head").html("登入失敗");
            }
            $("#login_fail_modal").find("span.-content").html(res.msg);

            $("#login_fail_modal").modal();
          }
        }
      });
    }
  });

});
