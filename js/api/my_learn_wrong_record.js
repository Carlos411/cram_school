var select_1 = ''; // 第一個下拉選單：領域
var select_2 = ''; // 第二個下拉選單：科目
var select_3 = ''; // 第三個下拉選單：年級群組
var select_2_ol_ids = []; // 第二個下拉選單：科目


var version_data = [];
var select3_data = []; // 第三個下拉選單準備放進去的資料

var callForSelect2 = 0;
var callForSelect3 = 0;

var render_data = [];

var total_test_wrong_count = 0;

$(function(){
  // 取得 url 參數
  if($.url('?select1') != undefined){
    select_1 = $.url('?select1');
  }
  if($.url('?select2') != undefined){
    select_2 = $.url('?select2');
  }
  if($.url('?select3') != undefined){
    select_3 = $.url('?select3');
  }

  getOutlineRoot(g_obj.ec_id, g_obj.db_practice_ot_id);
});

/**
 * 取得大綱根節點資料
 */
function getOutlineRoot(ecId, otId) {
  $.get( g_obj.domain + '/api/outline/root?ec_id=' + ecId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    $.each(response.data, function( index, item ) {
      if(index == 0){
        getOutlineNextNodeForSelect1(item.ol_id, otId);
      }
       //return false;
    });
  });
}

/**
 * 取得領域資料
 */
function getOutlineNextNodeForSelect1(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "";
      $.each(response.data, function( index, item ) {
         if(index == 0){ // 33 為「最新版　全科」
           html += '<option value="' + item.ol_id + '" selected>' + item.ol_display_name + '</option>';
           if(select_1 == ""){
             select_1 = item.ol_id;
           }
         }else{
           html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
         }
      });
      $("#select1").append(html);
      if(select_1 != ""){
        $("#select1").val(select_1);
      }

      if(select_1 != "" && callForSelect2 == 0){
        callForSelect2++;
        //alert(select_1);
        getOutlineNextNodeForSelect2(select_1, otId);
      }

    }
  });
}

/**
 * 取得科目資料
 */
function getOutlineNextNodeForSelect2(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "<option value='all' selected>全部</option>";
      if(select_2 == "" || select_2 == "all"){ // 若網址無參數，或有 select_2為"all"時，將所有的 ol_ids 存至 select_2_ol_ids 陣列
        select_2 = "all";
        //$.each(response.data, function( index, item ) {
          //select_2_ol_ids.push(item.ol_id)
        //});
      }else{
        //select_2_ol_ids.push(select_2)
      }

      $.each(response.data, function( index, item ) {
        html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
      });
      $("#select2").append(html);
      $("#select2").val(select_2);

      if(select_2 == "all"){
        /*$.each(response.data, function( index, item ) {
          select_2_ol_ids.push({
            ol_id: item.ol_id,
            ol_display_name: item.ol_display_name
          });
        });*/

        select_2_ol_ids.push({ // 因為是全部，故帶入領域的 ol_id
          ol_id: parseInt(select_1),
          ol_display_name: $("#select1 option:selected").text()
        });

        getTestRecordWrongCountStep1(select_2_ol_ids);
      }else{
        select_2_ol_ids.push({
          ol_id: select_2,
          ol_display_name: $("#select2 option:selected").text()
        });
        $("#select3").closest("div.option_inner").removeClass("no_display");
        getOutlineNextNodeForSelect3(select_2, otId);

      }
    }
  });
}


/**
 * 取得年級群組資料：第三個下拉選單
 */
function getOutlineNextNodeForSelect3(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      //var version_data = [];
      $.each(response.data, function( index, item ) {
        version_data.push({
          ol_id: item.ol_id,
          ol_display_name: item.ol_display_name
        });
      });
      getOutlineNextNodeForSelect3Again(otId);
    }
  });
}

function getOutlineNextNodeForSelect3Again(otId) {

  //console.log(version_data);
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + version_data[callForSelect3].ol_id + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( res ) {
    //console.log(res);
    if(res.data.length > 0){
      $.each(res.data, function( i, item ) {
        //console.log(item);
        //alert(callForSelect3);
        select3_data.push({
          ol_id: item.ol_id,
          ol_display_name: item.ol_display_name,
          version_name: (version_data[callForSelect3].ol_display_name == "不分版" ? "" : version_data[callForSelect3].ol_display_name )
        });
      });
      callForSelect3++;
      if(callForSelect3 < version_data.length){
        getOutlineNextNodeForSelect3Again(otId);
      }
      if(callForSelect3 == version_data.length){
        //console.log(select3_data);
        // 將 select3_data 裡的資料，放入第三個下拉選單
        var select3_html = "";
        $.each(select3_data, function( i, item ) {
          if( i == 0 ){
            if(select_3 == ""){
              select_3 = item.ol_id;
            }
          }
          select3_html += '<option value="' + item.ol_id + '">' + item.version_name + item.ol_display_name + '</option>';
        });
        $("#select3").html(select3_html);
        $("#select3").val(select_3);

        getTestRecordWrongCountStep1([{ol_id:select_3}]);
      }
    }
  });

}

// 取得各節錯題數
var callGetTestRecordWrongCountStep1 = 0;
function getTestRecordWrongCountStep1(select_ol_ids){
  //console.log(select_ol_ids);
  render_data = select_ol_ids;

  //alert(render_data[callGetTestRecordWrongCountStep1].ol_id);

  $.get( g_obj.domain + "/api/test/record/wrongCount?token=" + Cookies.get('u_t') + "&ol_id=" + render_data[callGetTestRecordWrongCountStep1].ol_id, function( res ) {
    //render_data[callGetTestRecordWrongCountStep1].version = res.data.nodes;
    //console.log(res);

    if(res.data.nodes.length > 0){
      var item_list_html = '';
      $.each(res.data.nodes, function(n, item){
        var item_ol_display_name_all_arr = (item.ol_display_name_all).split("/");
        var item_ol_display_name_all_str = item_ol_display_name_all_arr[2] + ' ' + (item_ol_display_name_all_arr[3] == "不分版" ? "": item_ol_display_name_all_arr[3] + " ") + item_ol_display_name_all_arr[4];
        var item_ol_display_name_all_for_print = item_ol_display_name_all_arr[1] + "_" + item_ol_display_name_all_arr[2] + (item_ol_display_name_all_arr[3] == "不分版" ? "" : "_" + item_ol_display_name_all_arr[3]) + "_" + item_ol_display_name_all_arr[4] + "_" + item_ol_display_name_all_arr[5];
        total_test_wrong_count += item.test_wrong_count;

        item_list_html += '<li>';
        item_list_html +=   '<a href="./my_learn_wrong_record_correct.html?ol_id=' + item.ol_id + '&test_wrong_count=' + item.test_wrong_count + '" class="item_link" data-ol-id="' + item.ol_id + '" data-filename="' + item_ol_display_name_all_for_print + '">';
        //item_list_html +=     '<img src="' + item.ol_img + '" class="link_img">';
        item_list_html +=     '<img src="../images/bg/bg_my_learn_wrong_record.svg" class="link_img">';
        item_list_html +=     '<div class="wrong_block">錯題 <span class="wrong_num">' + item.test_wrong_count + '</span> 題</div>';
        item_list_html +=       '<div class="desc_block">';
        item_list_html +=         '<p class="para1">' + item_ol_display_name_all_str + '</p>';
        item_list_html +=         '<p class="para2">' + item.ol_display_name2 + '</p>';
        item_list_html +=       '</div>';

        item_list_html +=     '<div class="custom_checkbox">';
        item_list_html +=       '<input type="checkbox" id="custom_checkbox_' + callGetTestRecordWrongCountStep1 + '_' + n + '" data_tq_ids="' + item.tq_ids + '" data_ol_ids="' + item.ol_id + '" class="custom_checkbox input_custom_checkbox">';
        item_list_html +=       '<label for="custom_checkbox_' + callGetTestRecordWrongCountStep1 + '_' + n + '" class="-checkitem">Custom Checkbox!</label>';
        item_list_html +=     '</div>';
        item_list_html +=   '</a>';
        item_list_html += '</li>';


      });

      $("#loading").remove();
      $("#item_list").append(item_list_html);
    }

    callGetTestRecordWrongCountStep1++
    if(callGetTestRecordWrongCountStep1 < render_data.length){
      getTestRecordWrongCountStep1(select_ol_ids);
    }
    if(callGetTestRecordWrongCountStep1 == render_data.length){
      $("#loading").remove();
      $("#test_wrong_count").html(total_test_wrong_count);
      if($("#item_list").children("li").length > 0){
        $("#item_list").children("li").each(function(i){
          if(i > 14){
            $(this).addClass("no_display");
          }
        });
        setPager();
      }else{
        $("#page_container").addClass("no_display");
      }
    }
  });
}

/**
 * 設定分頁
 */
function setPager() {
  var totalCount = $("#item_list").children("li").length;
  // $("#item_list").html(totalContent);
  var totalPage = Math.ceil(totalCount / 15);
  $("#total_page").html(str_pad(totalPage, 2, 0));
  if (totalPage > 1) {
    $("#pager_right_button").removeClass("-disabled");
  }
}
