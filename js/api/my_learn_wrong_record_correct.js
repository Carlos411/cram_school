var olId = '';

var subject_text = '';
var version = '';
var grade_group = '';
var chapter = '';
var part = '';
var token;
var listData = null;
var wrongCount;
var remainWrongCount;
var currentTqId;

$(function(){
  // 取得 url 參數
  if($.url('?ol_id') != undefined){
    olId = $.url('?ol_id');
  }
  token = Cookies.get('u_t');

  //getLastNode(g_obj.ec_id, g_obj.teacher_ot_id); // 學習記錄的 ot_id 與名師教學相同
  getLastNode();
  getTestRecordWrongQuestions(token, olId);
});

/**
 * 取得主標題
 */
function getLastNode(){
  $.get( g_obj.domain + "/api/outline/lastNode?ol_id=" + olId + '&token=' + token, function( res1 ) {
    // console.log(res1);
    chapter = res1.data[0].ol_display_name;
    $.get( g_obj.domain + "/api/outline/lastNode?ol_id=" + res1.data[0].ol_id + '&token=' + token, function( res2 ) {
      //console.log(res2);
      grade_group = res2.data[0].ol_display_name;
      $.get( g_obj.domain + "/api/outline/lastNode?ol_id=" + res2.data[0].ol_id + '&token=' + token, function( res3 ) {
        //console.log(res3);
        version = res3.data[0].ol_display_name;
        $.get( g_obj.domain + "/api/outline/lastNode?ol_id=" + res3.data[0].ol_id + '&token=' + token, function( res4 ) {
          //console.log(res4);
          subject_text = res4.data[0].ol_display_name;

          // 透過 nextNode 取得節的名稱
          $.get( g_obj.domain + "/api/outline/nextNode?ol_id=" + res1.data[0].ol_id + "&ot_id=" + g_obj.teacher_ot_id + '&token=' + token, function( res5 ) {
            //console.log(res5);
            $.each(res5.data, function(i, item){
              if(item.ol_id == olId){
                part = item.ol_display_name;
                main_title_string();
              }
            })
          });

        });
      });
    });
  });
}

/**
 * 設定主標題
 */
function main_title_string(){
  $("#main_title").html(subject_text + " " + (version == "不分版" ? "": version) + " " + grade_group + " " + chapter + " " + part);
}

/**
 * 取得取得錯題記錄(題目+答案)
 */
function getTestRecordWrongQuestions(token, olId) {
  $.ajax({
    url: g_obj.domain + '/api/test/record/wrongQuestions?token=' + token + '&ol_id=' + olId,
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data.length == 1){ // 僅一題的話，不會出現「下一題」按鈕
        $("button.q_next.-n").addClass("-invisible");
      }
      listData = response.data;
      wrongCount = listData.length;
      remainWrongCount = listData.length;;
      if(wrongCount > 0){
        var tabs = '';
        var tabs_question = '';
        var tabs_option = '';
        var tabs_answer = '';

        $.each(response.data, function( index, item ) {
           if (index == 0) {
              currentTqId = item.tq_id;
              tabs += '<li><button type="button" class="btn_q_and_tab -on" data-odt-id="' + item.odt_id + '" data-tq-id="' + item.tq_id + '" data-index=' + index + ' data-target="q_tab_' + (index+1) + '" data-ol-id="' + item.ol_id + '" data-multiple="' + ((item.tqt_name == "多重選擇題")?"1":"0") + '">' + (index+1) + '</button></li>';
              tabs_question += '<div class="each_q_block for_each_q q_tab_' + (index+1) + ' -on">';
              tabs_option += '<div class="answer_block for_each_q q_tab_' + (index+1) + ' -on">';
              tabs_answer += '<div class="tab_container for_each_q q_tab_' + (index+1) + ' -on">';
           } else {
              tabs += '<li><button type="button" class="btn_q_and_tab" data-odt-id="' + item.odt_id + '" data-tq-id="' + item.tq_id + '" data-index=' + index + ' data-target="q_tab_' + (index+1) + '" data-ol-id="' + item.ol_id + '" data-multiple="' + ((item.tqt_name == "多重選擇題")?"1":"0") + '">' + (index+1) + '</button></li>';
              tabs_question += '<div class="each_q_block for_each_q q_tab_' + (index+1) + '">';
              tabs_option += '<div class="answer_block for_each_q q_tab_' + (index+1) + '">';
              tabs_answer += '<div class="tab_container for_each_q q_tab_' + (index+1) + '">';
           }
           tabs_question += '<div class="each_q_block_content">';
           if (item.tq_audio_path != '') {
                tabs_question += '<div class="audio_block"><button type="button" class="btn_audio"><img src="./images/icon/btn_audio.svg"></button>';
                tabs_question += '<audio><source src="' + item.tq_audio_path + '">瀏覽器不支援</audio>';
                tabs_question += '</div>';
           }
           tabs_question += '<p class="para" id="question' + item.tq_id + '" data-tq-html="' + item.tq_html_link + '"></p>';
           tabs_question += '</div></div>';

           if (item.answers.length != 0) {
             tabs_option += '<button type="button" class="btn_answer_move btn_left"></button>';
             tabs_option += '<ul class="q_answer_list" data-original-index="0">';
             $.each(item.answers, function( index2, item2 ) {
                tabs_option += '<li><button type="button" data-ta-id="' + item2.ta_id + '" data-correct=' + item2.ta_correct + ' class="btn_answer">' + item2.option + '<span class="right"></span><span class="wrong">&times;</span></button></li>';
             });
             tabs_option += '</ul>';
             tabs_option += '<button type="button" class="btn_answer_move btn_right"></button>';
           }
           tabs_option += '</div>';

           tabs_answer += '<div id="navi' + item.tq_id + '" class="tab tab_1"><p class="para">' + item.tq_navi + '</p></div>';
           tabs_answer += '<div id="explain' + item.tq_id + '" data-tq-explain="' + item.tq_explain_link + '" class="tab tab_2"></div>';
           tabs_answer += '<div id="video_tab' + item.tq_id + '" class="tab tab_3">';
           tabs_answer += '<div class="video_block"><video id="video' + item.tq_id + '" class="answer_video" width="268" controls controlsList="nodownload" preload="auto" disablePictureInPicture="true"><source src="' + item.tq_video_path + '" type="video/mp4"></video></div>';
           tabs_answer += '</div></div>';
        });

        $("#tab_list").html(tabs);
        $("#tabs_question").html(tabs_question);
        $("#tabs_option").html(tabs_option);
        $("#tabs_answer").html(tabs_answer);

        preloadQuestionAndExplain(listData);
        setAnswerTabBtn(listData[0], true);
        setTabsOnclick();

        // 判斷收藏
        getUsercollection(g_obj.db_practice_ot_id, olId, "", $("#tab_list").find("button.btn_q_and_tab.-on").attr("data-tq-id"), "", "");
      }

      $("#remain_wrong").html('剩餘錯題數量：' + remainWrongCount + '題');
    }
  });
}

/**
 * 取得題目
 */
function getQuestion(tqId) {
  $.ajax({
    url: $("#question" + tqId).data("tq-html"),
    type: 'get',
    // data: data,
    // dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $("#question" + tqId).html(response);
    }
  });
}

/**
 * 取得詳解
 */
function getExplain(tqId) {
  $.ajax({
    url: $("#explain" + tqId).data("tq-explain"),
    type: 'get',
    // data: data,
    // dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $("#explain" + tqId).html(response);
    }
  });
}

/**
 * 預載題目和詳解
 */
function preloadQuestionAndExplain(listData) {
  $.each(listData, function( index, item ) {
       //$("#explain" + item.tq_id).css('visibility', 'hidden');
       //$("#video" + item.tq_id).css('visibility', 'hidden');
       getQuestion(item.tq_id);
       getExplain(item.tq_id);
  });
}

/**
 * 設定解答分頁按鍵狀態
 */
function setAnswerTabBtn(item, isInit) {

   if (item.tq_navi != '') {
     $("#navi_tab_btn").addClass('-have-data');
   } else {
     $("#navi_tab_btn").removeClass('-have-data -on');
   }

   if (item.tq_explain_link != '') {
     $("#explain_tab_btn").addClass('-have-data');
   } else {
     $("#explain_tab_btn").removeClass('-have-data -on');
   }

   if (item.tq_video_path != '') {
     $("#video_tab_btn").addClass('-have-data');
   } else {
     $("#video_tab_btn").removeClass('-have-data -on');
   }

   if (isInit) {
      $("#explain_tab_btn").addClass('-disabled');
      $("#video_tab_btn").addClass('-disabled');
   }

}

/**
 * 目前這題是否有答過
 */
function checkAnswered(el_id) {
  var btn_q_and_tab_on = $("#tab_list").find("button.btn_q_and_tab.-on");
  if($(btn_q_and_tab_on).attr("data-tq-id") == currentTqId){ // 點選到這題時，若已作答過，則不用再加上 -disabled
    if($(btn_q_and_tab_on).hasClass("-right") || $(btn_q_and_tab_on).hasClass("-wrong")){ // 已作答
      return true;
    }
    return false;
  }
}

/**
 * 設定題號分頁OnClick
 */
function setTabsOnclick() {
  // 題號分頁
  $("button.btn_q_and_tab").on("click", function(){
    var item = listData[$(this).data("index")];

    if(!($(this).hasClass("-right") || $(this).hasClass("-wrong"))){
      $("#btn_confirm").removeClass("-disabled"); // 若尚未作答，「確定」按鈕恢復可按
      $("#explain_tab_btn").addClass('-disabled');
      $("#video_tab_btn").addClass('-disabled');
    } else {
      $("#explain_tab_btn").removeClass('-disabled');
      $("#video_tab_btn").removeClass('-disabled');
    }

    $("#navi_tab_btn").removeClass('-on');
    $("#explain_tab_btn").removeClass('-on');
    $("#video_tab_btn").removeClass('-on');
    $("#navi" + item.tq_id).removeClass('-on');
    $("#explain" + item.tq_id).removeClass('-on');
    $("#video_tab" + item.tq_id).removeClass('-on');

    $(this).closest("ul.q_list").find("button.btn_q_and_tab").removeClass("-on");
    $(this).addClass("-on");

    var el_q_block_container = $(this).closest("div.q_block_container")[0];
    $(el_q_block_container).find("div.for_each_q").removeClass("-on");
    $(el_q_block_container).find("div.for_each_q." + $(this).attr("data-target")).addClass("-on");

    setAnswerTabBtn(item, false);
    currentTqId = $(this).attr("data-tq-id");

    // 判斷收藏
    getUsercollection(g_obj.db_practice_ot_id, olId, "", $(this).attr("data-tq-id"), "", "");
  });
}
