﻿var olId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterCount;
var totalCount = 0;
var totalContent = "";

$(function(){
  // 取得頁面間傳遞參數
  olId = $.url('?ol_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');
  versionName = $.url('?version_name');
  gradeGroupName = $.url('?grade_group_name');

  // 設定導覽列
  checkHeaderTextUnit(olId);
  //$("#header_field").text(fieldName);
  //$("#header_subject").text(subjectName);
  //$("#header_grade_group").text(versionName + gradeGroupName);
  $("#header_unit").attr("href", "#");

  //getChapter(olId, g_obj.teacher_ot_id);
  getOutlineNextTwoNode(olId, g_obj.db_practice_ot_id);

  // 設定導覽列連結
  setHeaderGradeGroupHref(olId);

  // 最近測驗結果
  getTestRecordRecent("recently_view_item_list", 1);

  left_aside_link(3); // 更換大類別連結
});



function getOutlineNextTwoNode(olId, otId ) {

  //alert('olId=['+ olId +'] , otId=['+ otId +'] ');
  //alert("因為測試的圖檔解析度有問題， 所以先佔時使用，預設的圖片");

  $.ajax({
    url: g_obj.domain + '/api/outline/nextTwoNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        totalCount = response.data.length;
        var html = "";

        $.each(response.data, function( index, item ) {
          //console.log(item);
           //正常版的圖片
           //html += '<li><a href="#" class="item_link"><img src="' + item.ol_img + '" class="link_img"><div class="desc_block"><p class="para1">'+ item.ol_display_name_1 +'</p><p class="para2">'+ item.ol_display_name_2 +'</p></div><div class="custom_checkbox"><input type="checkbox" id="custom_checkbox_'+ item.ol_id +'" value="'+ item.ol_id +'" class="custom_checkbox"><label for="custom_checkbox_'+ item.ol_id +'" class="-checkitem">Custom Checkbox!</label></div></a></li>';
           html += '<li class="' + (index < 15 ? "" : "no_display" ) + '"><a href="#" class="item_link"><img src="' + item.ol_img + '" class="link_img"><div class="desc_block"><p class="para1">'+ item.ol_display_name_1 +'</p><p class="para2">'+ item.ol_display_name_2 +'</p></div><div class="custom_checkbox"><input type="checkbox" id="custom_checkbox_'+ item.ol_id +'" value="'+ item.ol_id +'" class="custom_checkbox"><label for="custom_checkbox_'+ item.ol_id +'" class="-checkitem">Custom Checkbox!</label></div></a></li>';
           //$('#custom_checkbox_'+ item.ol_id).prop('checked', true);
           //alert("id=[custom_checkbox_"+ item.ol_id + "]");
        });

        $("#item_list").html(html);
      }else{
        $("#item_list").find("span.-loading").text("此課程無考卷測驗");
      }
      setPager();
    }
  });
}

/**
 * 設定分頁
 */
function setPager() {
  var totalPage = Math.ceil(totalCount / 15);
  $("#total_page").html(str_pad(totalPage, 2, 0));
  if (totalPage == 1) {
    $("#pager_right_button").addClass("-disabled");
  }
  if (totalPage > 1) {
    $("#pager_right_button").removeClass("-disabled");
  }
}

function saveToStorage(){

	var String_Check = "";
	window.localStorage["String_Check_Value"] = "";


	//這樣寫，沒有辦法全選，寫法不對
	//$("input.custom_checkbox").each(function(index, item){
	//	//$(item).prop("checked");
	//});


	//這樣寫， 可以全選
	$("input[type='checkbox']").each(function(){
		//this.checked=true;
    if(!$(this).hasClass("custom_checkbox_all")){
      if ($(this).prop("checked")){
        var id = $(this).attr("id");          // equals：this.id
        var name = $(this).attr("name");      // equals: this.name
        var theClass = $(this).attr("class"); // equals: this.className
        var value = $(this).attr("value");    // equals: this.value　// equals： $(this).val();
        String_Check = String_Check + value + "," ;
      }
    }

	}); //end each(function(){

	//$("button.btn_send").removeClass("-disabled");
	//alert("String_Check=["+ String_Check +"]");

	window.localStorage["String_Check_Value"] = String_Check ;
	//alert("W localStorage=["+ window.localStorage["String_Check_Value"] +"]");

}
