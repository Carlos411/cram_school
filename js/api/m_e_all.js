$(function(){
  getHandoutRecent("recently_view_item_list");
});
// 取得最近講義
function getHandoutRecent(id_name){
  $.get( g_obj.domain + '/api/handout/recent?token=' + Cookies.get('u_t'), function( response ) {
    /*
    最後網址參數應產生如下：
    ol_id=58&
    unit_ol_id=45&
    field_name=全科&
    subject_name=國文&
    version_name=&
    grade_group_name=核心文言文選&
    chapter_name=第一單元%20虯髯客傳&
    part_name=燭之武退秦師(上)
    */
    if(response.data.length > 0){
      var recent_html = '';
      $.each(response.data, function(i, item){
        if(item.ho_id != null){
          recent_html += '<li>';
          recent_html +=   '<a href="./m_e_paper.html?ol_id=' + item.ol_id + '&unit_ol_id=' + item.previous_nodes[1].ol_id + '&field_name=' + item.previous_nodes[4].ol_display_name + '&subject_name=' + item.previous_nodes[3].ol_display_name + '&version_name=' + (item.previous_nodes[2].ol_display_name == "不分版" ? "" : item.previous_nodes[2].ol_display_name ) + '&grade_group_name=' + item.previous_nodes[1].ol_display_name + '&chapter_name=' + item.previous_nodes[0].ol_display_name + '&part_name=' + item.ol_display_name + '" class="item_link">';
          recent_html +=     '<img src="' + item.ol_img + '" class="link_img">';
          recent_html +=     '<div class="desc_block">';
          recent_html +=       '<p class="para1">' + item.previous_nodes[0].ol_display_name + '</p>';
          recent_html +=       '<p class="para2">' + item.ol_display_name + '</p>';
          recent_html +=     '</div>';
          recent_html +=   '</a>';
          recent_html += '</li>';
        }
      });
      $("#" + id_name).html(recent_html);
    }else{
      $("#" + id_name).html("");
    }
  });
}
