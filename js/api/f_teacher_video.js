var olId;
var unitOlId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterName;
var partName;
var pptData;
var firstPara;
var firstKnowledgePoint;
//var outlineKnowledgePointHtml = "";
var knowledge_od_id; // 知識點 id
var uc_video_from; // 使用者收藏秒數

// 判斷是否為最後一個 節 相關
var knowledge_points_data = [];
var is_last_part_video = false;
var have_rated = false;

var nextFourNodeData;
var user_collection_od_id;

$(function(){

  //Cookies.remove('rating_sent'); // 直接先清除 rating_sent 這個 cookie

  // 取得頁面間傳遞參數
  olId = $.url('?ol_id');
  unitOlId = $.url('?unit_ol_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');
  versionName = $.url('?version_name');
  gradeGroupName = $.url('?grade_group_name');
  chapterName = $.url('?chapter_name');
  partName = $.url('?part_name');
  if($.url('?od_id') !== undefined){
    user_collection_od_id = $.url('?od_id');
  }
  // 設定導覽列
  checkHeaderTextAfterUnit(unitOlId);

  //$("#header_field").text(fieldName);
  //$("#header_subject").text(subjectName);
  //$("#header_grade_group").text(versionName + gradeGroupName);

  $("#header_unit").attr("href", "./f_teacher_unit.html?ol_id=" + unitOlId + "&field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + versionName + "&grade_group_name=" + gradeGroupName);

  $("#main_title").text(subjectName + " " + ((versionName)?versionName + " " : "") + gradeGroupName + " " + chapterName + " " + partName);
  $("#course_name").text(subjectName + versionName + gradeGroupName);

  $("#ppt_img_block").hide();

  /*
  $.get( g_obj.domain + "/api/outline/nextFourNode?ol_id=" + unitOlId + '&token=' + Cookies.get('u_t'), function( res ) {
    console.log("這裡2");
    console.log(res);
  });
  */

  getOutlineChapter(unitOlId, g_obj.teacher_ot_id);
  getPara(olId, g_obj.teacher_ot_id);



  // 設定導覽列連結
  setHeaderGradeGroupHref(unitOlId);

  // 最近觀看
  get_video_recent("video_recent_item_list", g_obj.teacher_ot_id);

  // 取得使用者點數
  getUserPoint();
});

/**
 * 取得影片右側影片段落資料
 */
function getPara(olId, otId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0) {
        var html = '';
        $.each(response.data, function( index, item ) {
            knowledge_points_data.push({
              ol_id: item.ol_id,
              ol_display_name: item.ol_display_name
            });
            if (index == 0) {
              firstPara = item.ol_id;
            }
            // console.log(item.ol_display_name);
            html += '<li id="video_desc_list_para' + item.ol_id + '"><span class="item_block"><span class="str_text -title"><span class="circle"><span class="circle_inner"></span></span>' + item.ol_display_name + '</span></span></li>';
            //getKnowledgePoint(item.ol_id);
        });
        $("#video_desc_list").html(html);

        $.each(response.data, function( index, item ) {
            // console.log(item.ol_display_name);
            getKnowledgePoint(item.ol_id);
        });
      }
    }
  });
}

/**
 * 取得影片右側知識點資料
 */
function getKnowledgePoint(olId) {
  $.ajax({
    url: g_obj.domain + '/api/knowledgePoint?ol_id=' + olId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){

      $.each(knowledge_points_data, function(i, item){
        if(item.ol_id == olId){
          knowledge_points_data[i].part = response.data;
        }
      });

      if (response.data.length > 0) {
        var html = '';
        $.each(response.data, function(index, item) {
            if (olId == firstPara && index == 0) {
              firstKnowledgePoint = item.od_id;
              html += '<li data-odid="' + item.od_id + '"><a id="kpoint' + item.od_id + '" href="javascript:switchVideo(' + item.od_id + ');" class="item_block -on"><span id="time_text' + item.od_id + '" class="time_text"></span><span class="str_text">' + item.od_name + '</span></a></li>';
            } else {
              html += '<li data-odid="' + item.od_id + '"><a id="kpoint' + item.od_id + '" href="javascript:switchVideo(' + item.od_id + ');" class="item_block"><span id="time_text' + item.od_id + '" class="time_text"></span><span class="str_text">' + item.od_name + '</span></a></li>';
            }
            // console.log(item.od_id);
            // console.log(item.od_name);
            getVideo(item.od_id);

        });
        $("#video_desc_list_para" + olId).after(html);
      }

      //console.log("裡");
      //console.log(knowledge_points_data);
    }
  });
}

/**
 * 取得影片右側影片資料
 */
function getVideo(odId) {
  $.ajax({
    url: g_obj.domain + '/api/video?od_id=' + odId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $.each(response.data, function(index, item) {
        // console.log(item.vd_id);
        // console.log(item.vd_second);
        $("#time_text" + odId).text(secondsTimeSpanToHMS(item.vd_second));
        $("#kpoint" + odId).data("vdid", item.vd_id);

        if(user_collection_od_id != undefined){ // 表示從收藏過來的連結
          switchVideo(user_collection_od_id);
        }else{
          if (odId == firstKnowledgePoint && index == 0) { // 載入第一個影片
            knowledge_od_id = odId;

            // 判斷收藏：名師教學
            getUsercollection(g_obj.teacher_ot_id, olId, knowledge_od_id, "", "", "");

              getPpt(odId);
              loadVideo(getVideoStreamUrl(item.vd_id), true);
              is_last_part(odId);
              marker_location(odId);
              return false;
          }
        }

      });
    }
  });
}

/**
 * 切換影片
 */
function switchVideo(odId) {
  videojs(document.getElementById('my-video')).dispose();
  $("div.all_contents_block").find("div.video_parent").html("<video id='my-video' class='video-js' controls preload='auto' data-setup='{ \"aspectRatio\":\"1920:1080\", \"playbackRates\": [0.5, 1, 1.5, 2] }' crossorigin='anonymous'></video>");
  $("div.all_contents_block div.comment_block").removeClass("-from-seekbar").addClass("-none");
  video = document.querySelector('video');
  loadedmetadata();

  knowledge_od_id = odId;

  // 判斷收藏：名師教學
  getUsercollection(g_obj.teacher_ot_id, olId, knowledge_od_id, "", "", "");

  $(".item_block").removeClass('-on');
  $("#kpoint" + odId).addClass('-on');
  getPpt(odId);
  loadVideo(getVideoStreamUrl($("#kpoint" + odId).data("vdid")), true);

  is_last_part(odId);

  marker_location(odId);
}

function marker_location(the_od_id){
  // marker icon location
  $("ul.phrase_list").find("p.item_title").each(function(){
    if($(this).attr("data-ol-id") == olId){
      var sub_phrase_list = $(this).closest("li").find("ul.sub_phrase_list");
      $(sub_phrase_list).find("span.icon_location").addClass("no_display");
      $(sub_phrase_list).find("li[data-od-id="+the_od_id+"]").find("span.icon_location").removeClass("no_display");
    }
  });
}

/**
 * 判斷是否為最後的節
 */

function is_last_part(knowledgeId){
  // 判斷是否為最後的節
  $.each(knowledge_points_data, function(i, item){
    $.each(item.part, function(j, item_part){
      if(knowledgeId == item_part.od_id){

        if( (j+1) == knowledge_points_data[i].part.length){
          //console.log("是最後一節");
          is_last_part_video = true;

          // 判斷是否要跳出評分
          $.each(nextFourNodeData, function(k, item1){
            if(item1.layer2.length > 0){
              $.each(item1.layer2, function(m, item2){

                if(item2.layer3.length > 0){
                  $.each(item2.layer3, function(p, item3){
                    if(item3.layer4.length > 0){
                      $.each(item3.layer4, function(q, item4){
                        if(item4.od_id == knowledge_od_id){

                          $.each(nextFourNodeData[k].layer2[m].layer3[p].layer4, function(r, item5){

                            if(item5.is_rated){
                              have_rated = true;
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }

          });

        }else{
          //console.log("不是最後一節");
          is_last_part_video = false;
        }
      }
    });
  });
}

/**
 * 取得影片連結
 */
function getVideoStreamUrl(vdId) {
  // console.log(g_obj.domain + '/api/video/stream?vd_id=' + vdId);
  return g_obj.domain + '/api/video/stream?vd_id=' + vdId + '&token=' + Cookies.get('u_t');
}

/**
 * 載入影片
 */
function loadVideo(videoUrl, autoplay){
    videojs('my-video').ready(function () {
                var player = this;
                var executed = false;
                player.src({type: 'video/mp4', src: videoUrl});
                player.playbackRate(1);

                if (autoplay) {
                  // console.log("autoplay");
                  player.play();
                }

                // 偵測影片播放完畢
                player.on('ended', function() {
                    if (!executed) { // 避免載入多個影片後，ended會被呼叫多次
                      // console.log('video is done!');
                      if( !$("#video_desc_list a.item_block.-on").closest("li").is(':last-child') ){ // 最後一個影片，才會顯示；其它則隱藏。
                        $("div.all_contents_block div.comment_block").addClass("-none");
                      }
                      executed = true
                      var odId = $(".item_block.-on").parent().next().data("odid");
                      if (odId != undefined) {
                          // console.log(odId);
                          setTimeout(function(){switchVideo(odId);}, 2000);
                      } else {
                          odId = $(".item_block.-on").parent().next().next().data("odid");
                          if (odId != undefined) {
                            // console.log(odId);
                            setTimeout(function(){switchVideo(odId);}, 2000);
                          }
                      }
                    }
                });

                // 偵測影片播放進度
                player.on('timeupdate', function () {
                  var currentTime = player.currentTime();
                  uc_video_from = currentTime;
                  // console.log("currentTime:" + currentTime);

                  // 評分跳出的判斷
                  if(is_last_part_video){
                    if(!have_rated){
                      if(parseInt(player.remainingTime()) <= 5){ // 剩最後 5 秒時
                        //Cookies.set('rating_sent', 1); // 表示已評分過
                        $("#comment_rating_modal").modal();
                        if(!player.isFullscreen()){
                          player.pause();
                        }
                      }
                    }
                  }

                  // 影片播到一半，變已讀狀態(-read)
                  /*if( parseInt(currentTime) / parseInt(player.duration()) > 0.5 ){
                    change_to_read(knowledge_od_id);
                  }
                  */

                  if (pptData != null) {
                    $.each(pptData, function(index, item) {
                      if (pptData.length == index + 1) {
                        if ( currentTime >= item.cs_second_from ) {
                          $("#ppt_img").attr("src", item.cs_path);
                          $("#ppt_img_block").show();
                          return false;
                        }
                      }else{
                        if ( currentTime >= item.cs_second_from && currentTime < pptData[index+1].cs_second_from ) {
                          $("#ppt_img").attr("src", item.cs_path);
                          $("#ppt_img_block").show();
                          return false;
                        }
                      }

                    });
                  }
                })


                player.on('pause', function () {
                  if($("div.all_contents_block div.comment_block").hasClass("-from-seekbar")){
                    $("div.all_contents_block div.comment_block").removeClass("-from-seekbar");
                  }else{
                    if($("div.all_contents_block div.comment_block").hasClass("-none")){
                      $("div.all_contents_block div.comment_block").removeClass("-none");
                    }
                  }

                });
                player.on('play', function () {
                  //if($("div.all_contents_block div.comment_block").hasClass("-from-seekbar")){
                    //$("div.all_contents_block div.comment_block").removeClass("-from-seekbar");
                  //}else{
                    if(!$("div.all_contents_block div.comment_block").hasClass("-none")){
                      $("div.all_contents_block div.comment_block").addClass("-none");
                    }
                  //}
                });

            });

}

/**
 * 取得PPT資料
 */
function getPpt(odId) {

  $("#ppt_img").attr("src", '');
  $("#ppt_img_block").hide();
  pptData = [];

  $.ajax({
    url: g_obj.domain + '/api/ppt?od_id=' + odId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      pptData = response.data;
    }
  });
}

/**
 * 取得影片右側大綱章資料
 */
function getOutlineChapter(olId){
  $.get( g_obj.domain + "/api/outline/nextFourNode?ol_id=" + olId + '&token=' + Cookies.get('u_t'), function( res ) {
    console.log("這裡");
    console.log(res);
    nextFourNodeData = res.data;
    var outline_html = '';
    $.each(res.data, function(i, item){
      outline_html += '<div class="phrase_block">';
      outline_html +=   '<span class="vertical_line"></span>';
      outline_html +=   '<div class="phrase_left">';
      outline_html +=     '<span class="-dash"></span>';
      outline_html +=     '<p class="chapter_para"><span class="-chapter"></span><span class="chapter_name">' + item.ol_display_name + '</span></p>';
      outline_html +=   '</div>';
      outline_html +=   '<div class="phrase_right">';
      if(item.layer2.length > 0){
        outline_html +=   '<ul class="phrase_list">';
        $.each(item.layer2, function(j, part){
          // span.text 可額放放置 -unread、-read、-strong、-basic、-familiar
          outline_html +=   '<li>';
          outline_html +=     '<p class="item_title" data-ol-id="' + part.ol_id + '"><span class="text -unread">' + part.ol_display_name + '</span></p>';
          if(part.layer3.length > 0){
            outline_html +=   '<ul class="sub_phrase_list">';
            $.each(part.layer3, function(k, para_item){
              if(para_item.layer4.length > 0){
                $.each(para_item.layer4, function(m, part){
                  //outline_html +=     '<li><span class="icon_location"><img src="./images/icon/icon-location.svg" class="icon_location"></span><span class="text -unread">KP0這個這個這個</span></li>';
                  //outline_html +=     '<li><span class="text -read">KP0xxx</span></li>';
                  //outline_html +=     '<li><span class="text -strong">KP0xxx</span></li>';
                  //outline_html +=     '<li><span class="text -basic">KP0xxx</span></li>';
                  //outline_html +=     '<li><span class="text -familiar">KP0xxx</span></li>';
                  outline_html +=     '<li data-od-id="' + part.od_id + '" data-vd-id="' + part.vd_id + '"><span class="icon_location no_display"><img src="./images/icon/icon-location.svg" class="icon_location"></span><span class="text ' + (part.is_read?"-read":"-unread") + ' ' + ( ( (part.test_level == 1) ? "-strong":( (part.test_level == 2 ? "-basic":(part.test_level == 3 ? "-familiar":"") ) ) ) ) + '">' + part.od_name + '</span></li>';
                });
              }
            });
            outline_html +=   '</ul>';
          }
          outline_html +=   '</li>';
        });
        outline_html +=   '</ul>';
      }
      outline_html +=   '</div>';
      outline_html += '</div>';
    });
    $("#all_phrase_block").html(outline_html);

    marker_location(knowledge_od_id);
  });
}

/**
 * 影片時間格式轉換
 */
function secondsTimeSpanToHMS(s) {
    var h = Math.floor(s/3600); //Get whole hours
    s -= h*3600;
    var m = Math.floor(s/60); //Get remaining minutes
    s -= m*60;
    return (h == 0 ? '' : (h < 10 ? '0'+h+':' : h+':'))+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding
}

// 大綱地圖的文字變已讀(文字變黑色)
/*
function change_to_read(the_od_id){
  $("ul.phrase_list").find("p.item_title").each(function(){
    if($(this).attr("data-ol-id") == olId){
      var sub_phrase_list = $(this).closest("li").find("ul.sub_phrase_list");
      if(!$(sub_phrase_list).find("li[data-od-id="+the_od_id+"]").find("span.text").hasClass("-read")){
        $(sub_phrase_list).find("li[data-od-id="+the_od_id+"]").find("span.text").removeClass("-unread").addClass("-read");
      }
    }
  });
}
*/
