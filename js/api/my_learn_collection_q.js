var olId;
//var unitOlId;
var subjectOlId;
var tqId; // 題目 id
var ucId; // 使用者收藏 id
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterName;
var partName;
var token;
var itemData = null;

$(function(){
  // 取得頁面間傳遞參數
  olId = $.url('?ol_id');
  subjectOlId = $.url('?subject_ol_id');
  tqId = $.url('?tq_id');
  ucId = $.url('?uc_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');
  versionName = $.url('?version_name');
  gradeGroupName = $.url('?grade_group_name');
  chapterName = $.url('?chapter_name');
  partName = $.url('?part_name');
  token = Cookies.get('u_t');

  if(gradeGroupName == undefined){ // 表示層數為4，沒有 grade_group_name ......
    $("#main_title").text(subjectName + " " + ((versionName)?versionName + " " : ""));
  }else{
    $("#main_title").text(subjectName + " " + ((versionName)?versionName + " " : "") + gradeGroupName + " " + chapterName + " " + partName);
  }


  // 收藏判斷，此處一定是已收藏的狀態
  $("#tag_plus").attr("data-ol_id", olId).attr("data-status", "1").attr("data-tq_id", tqId).attr("data-uc-id", ucId);

  getUserCollection(token, g_obj.db_practice_ot_id, subjectOlId);
});

/**
 * 取得使用者收藏
 */
function getUserCollection(token, otId, olId) {
  $.ajax({
    url: g_obj.domain + '/api/usercollection/list?token=' + token + '&ot_id=' + otId + '&ol_id=' + olId + '&uc_order=0',
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      var count = response.data.length;
      if(count > 0){
        var tabs = '';
        var tabs_question = '';
        var tabs_option = '';
        var tabs_answer = '';

        $.each(response.data, function( index, item ) {
           if (item.uc_id == ucId && item.question != undefined) {
             var i = 0;
             itemData = item;
             tabs += '<li><button type="button" class="btn_q_and_tab -on" data-tq-id="' + item.question[0].tq_id + '" data-index=' + i + ' data-target="q_tab_' + (i+1) + '" data-ol-id="' + item.ol_id + '" data-multiple="' + ((item.question[0].tqt_id == 2)?"1":"0") + '" data-tqt-id="' + item.question[0].tqt_id + '">' + (i+1) + '</button></li>';
             tabs_question += '<div class="each_q_block for_each_q q_tab_' + (i+1) + ' -on">';
             tabs_option += '<div class="answer_block for_each_q q_tab_' + (i+1) + ' -on">';
             tabs_answer += '<div class="tab_container for_each_q q_tab_' + (i+1) + ' -on">';

             tabs_question += '<div class="each_q_block_content">';
             if (item.question[0].tq_audio_path != '') {
                tabs_question += '<div class="audio_block"><button type="button" class="btn_audio"><img src="./images/icon/btn_audio.svg"></button>';
                tabs_question += '<audio><source src="' + item.question[0].tq_audio_path + '">瀏覽器不支援</audio>';
                tabs_question += '</div>';
             }
             tabs_question += '<p class="para" id="question' + item.question[0].tq_id + '" data-tq-html="' + item.question[0].tq_html_link + '"></p>';
             tabs_question += '</div></div>';

             if (item.question[0].answers.length != 0) {
               tabs_option += '<button type="button" class="btn_answer_move btn_left"></button>';
               tabs_option += '<ul class="q_answer_list" data-original-index="0">';
               $.each(item.question[0].answers, function( index2, item2 ) {
                  tabs_option += '<li><button type="button" data-correct=' + item2.ta_correct + ' class="btn_answer">' + item2.option + '<span class="right"></span><span class="wrong">&times;</span></button></li>';
               });
               tabs_option += '</ul>';
               tabs_option += '<button type="button" class="btn_answer_move btn_right"></button>';
             }
             tabs_option += '</div>';

             tabs_answer += '<div class="tab tab_1"><p class="para">' + item.question[0].tq_navi + '</p></div>';
             tabs_answer += '<div id="explain' + item.question[0].tq_id + '" data-tq-explain="' + item.question[0].tq_explain_link + '" class="tab tab_2"></div>';
             tabs_answer += '<div class="tab tab_3">';
             tabs_answer += '<div class="video_block"><video id="video' + item.question[0].tq_id + '" class="answer_video" width="268" controls controlsList="nodownload" preload="auto" disablePictureInPicture="true"><source src="' + item.question[0].tq_video_path + '" type="video/mp4"></video></div>';
             tabs_answer += '</div></div>';

             return false;
           }
        });

        $("#tab_list").html(tabs);
        $("#tabs_question").html(tabs_question);
        $("#tabs_option").html(tabs_option);
        $("#tabs_answer").html(tabs_answer);

        if (itemData != null) {
          preloadQuestionAndExplain(itemData);
          setAnswerTabBtn(itemData);
        }
      }
    }
  });
}

/**
 * 取得題目
 */
function getQuestion(tqId) {
  $.ajax({
    url: $("#question" + tqId).data("tq-html"),
    type: 'get',
    // data: data,
    // dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $("#question" + tqId).html(response);
    }
  });
}

/**
 * 取得詳解
 */
function getExplain(tqId) {
  $.ajax({
    url: $("#explain" + tqId).data("tq-explain"),
    type: 'get',
    // data: data,
    // dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $("#explain" + tqId).html(response);
    }
  });
}

/**
 * 預載題目和詳解
 */
function preloadQuestionAndExplain(item) {
  getQuestion(item.question[0].tq_id);
  getExplain(item.question[0].tq_id);
}

/**
 * 設定解答分頁按鍵狀態
 */
function setAnswerTabBtn(item) {

   if (item.question[0].tq_navi != '') {
     $("#navi_tab_btn").addClass('-have-data');
   } else {
     $("#navi_tab_btn").removeClass('-have-data -on');
   }

   if (item.question[0].tq_explain_link != '') {
     $("#explain_tab_btn").addClass('-have-data -disabled'); // 加上 -disabled 表示目前不能點選
   } else {
     $("#explain_tab_btn").removeClass('-have-data -on');
   }

   if (item.question[0].tq_video_path != '') {
     $("#video_tab_btn").addClass('-have-data -disabled'); // 加上 -disabled 表示目前不能點選
   } else {
     $("#video_tab_btn").removeClass('-have-data -on');
   }
}
