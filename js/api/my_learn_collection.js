var select_1 = ''; // 第一個下拉選單：領域
var select_2 = ''; // 第二個下拉選單：科目
var select_3 = ''; // 第三個下拉選單：年級群組
var select_2_ol_ids = []; // 第二個下拉選單：科目

var otId = '';

var version_data = [];
var select3_data = []; // 第三個下拉選單準備放進去的資料

var callForSelect2 = 0;
var callForSelect3 = 0;

var render_data = [];

var uc_order = 1; // 預設：時間排序

$(function(){
  // 取得 url 參數
  if($.url('?select1') != undefined){
    select_1 = $.url('?select1');
  }
  if($.url('?select2') != undefined){
    select_2 = $.url('?select2');
  }
  if($.url('?select3') != undefined){
    select_3 = $.url('?select3');
  }
  if($.url('?uc_order') != undefined){
    uc_order = $.url('?uc_order');
  }
  if($.url('?ot_id') != undefined){
    otId = $.url('?ot_id');
  }else{
    otId = g_obj.teacher_ot_id;
  }

  if(uc_order == "1"){
    $("#sort_time").addClass("-on");
  }else{
    $("#sort_unit").addClass("-on");
  }

  switch(otId){
    case '1': // 名師教學
      $("#collection_type1").addClass("-on");
      break;
    case '2': // 段考直播
      $("#collection_type2").addClass("-on");
      break;
    case '3': // 題目收藏
      $("#collection_type3").addClass("-on");
      break;
    case '4': // 電子講義
      $("#collection_type4").addClass("-on");
      break;
    case '9': // 互動實驗
      $("#collection_type5").addClass("-on");
      break;
    default:
      $("#collection_type1").addClass("-on");
  }

  getOutlineRoot(g_obj.ec_id, otId); // 學習記錄的 ot_id 與名師教學相同
});

/**
 * 取得大綱根節點資料
 */
function getOutlineRoot(ecId, otId) {
  $.get( g_obj.domain + '/api/outline/root?ec_id=' + ecId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    console.log(response.data);
    if(response.data.length > 0){
      if(otId == 2){ // 段考直播

        $.each(response.data, function( index, item ) {
          // 段考直播，直接取 第一筆
          if(index == 0){
            getOutlineNextNodeForSelect1(item.ol_id, otId);
          }
        });
      }else{
        $.each(response.data, function( index, item ) {
          if(index == 0){
            getOutlineNextNodeForSelect1(item.ol_id, otId);
          }
        });
      }

    }else{
      alert("無資料");
      $("#item_list").html("");
    }

  });
}

/**
 * 取得領域資料
 */
function getOutlineNextNodeForSelect1(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "";
      $.each(response.data, function( index, item ) {
         if(index == 0){ // 33 為「最新版　全科」
           html += '<option value="' + item.ol_id + '" selected>' + item.ol_display_name + '</option>';
           if(select_1 == ""){
             select_1 = item.ol_id;
           }
         }else{
           html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
         }
      });
      $("#select1").append(html);
      if(select_1 != ""){
        $("#select1").val(select_1);
      }
      if(otId == "2"){ // 段考直播
        if(select_1 == ""){
          select_1 = $("#select1").val();
        }
      }
      if(select_1 != "" && callForSelect2 == 0){

        callForSelect2++;
        //alert(select_1);
        getOutlineNextNodeForSelect2(select_1, otId);
      }

    }else{
      alert("無資料");
      $("#item_list").html("");
    }
  });
}

/**
 * 取得科目資料
 */
function getOutlineNextNodeForSelect2(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "<option value='all' selected>全部</option>";
      if(select_2 == "" || select_2 == "all"){ // 若網址無參數，或有 select_2為"all"時，將所有的 ol_ids 存至 select_2_ol_ids 陣列
        select_2 = "all";
        //$.each(response.data, function( index, item ) {
          //select_2_ol_ids.push(item.ol_id)
        //});
      }else{
        //select_2_ol_ids.push(select_2)
      }

      $.each(response.data, function( index, item ) {
        html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
      });
      $("#select2").append(html);
      $("#select2").val(select_2);

      if(select_2 == "all"){
        /*$.each(response.data, function( index, item ) {
          select_2_ol_ids.push(item.ol_id);
        });*/
        select_2_ol_ids.push(parseInt(select_1)); // 因為是全部，故帶入領域的 ol_id
        // here
        getUsercollectionListStep1();
      }else{
        if(otId == "2"){
          select_2_ol_ids.push(parseInt(select_2));
          // here
          getUsercollectionListStep1();
        }else{
          $("#select3").closest("div.option_inner").removeClass("no_display");
          getOutlineNextNodeForSelect3(select_2, otId);
        }
      }

    }else{
      alert("無資料");
      $("#item_list").html("");
    }
  });
}

/**
 * 取得年級群組資料：第三個下拉選單
 */
function getOutlineNextNodeForSelect3(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      //var version_data = [];
      $.each(response.data, function( index, item ) {
        version_data.push({
          ol_id: item.ol_id,
          ol_display_name: item.ol_display_name
        });
      });
      getOutlineNextNodeForSelect3Again(otId);
    }
  });
}

function getOutlineNextNodeForSelect3Again(otId) {

  //console.log(version_data);
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + version_data[callForSelect3].ol_id + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( res ) {
    //console.log(res);
    if(res.data.length > 0){
      $.each(res.data, function( i, item ) {
        //console.log(item);
        //alert(callForSelect3);
        select3_data.push({
          ol_id: item.ol_id,
          ol_display_name: item.ol_display_name,
          version_name: (version_data[callForSelect3].ol_display_name == "不分版" ? "" : version_data[callForSelect3].ol_display_name )
        });
      });
      callForSelect3++;
      if(callForSelect3 < version_data.length){
        getOutlineNextNodeForSelect3Again(otId);
      }
      if(callForSelect3 == version_data.length){
        //console.log(select3_data);
        // 將 select3_data 裡的資料，放入第三個下拉選單
        var select3_html = "";
        $.each(select3_data, function( i, item ) {
          if( i == 0 ){
            if(select_3 == ""){
              select_3 = item.ol_id;
            }
          }
          select3_html += '<option value="' + item.ol_id + '">' + item.version_name + item.ol_display_name + '</option>';
        });
        $("#select3").html(select3_html);
        $("#select3").val(select_3);
        select_2_ol_ids.push(parseInt(select_3)); // 出現第三個下拉選單時，將第三個下拉選單的 ol_id ，放入 select_2_ol_ids
        // here
        getUsercollectionListStep1();


      }
    }
  });

}


var callUsercollectionListStep1 = 0;
function getUsercollectionListStep1(){
  //console.log("這裡: " + callUsercollectionListStep1);
  //console.log("這裡");
  //console.log(select_2_ol_ids);
  // /api/usercollection/list?token=gg&ot_id=1&ol_id=1&uc_order=1
  $.get( g_obj.domain + '/api/usercollection/list?token=' + Cookies.get('u_t') + '&ol_id=' + select_2_ol_ids[callUsercollectionListStep1] + '&ot_id=' + otId + '&uc_order=' + uc_order, function( res1 ) {
    //console.log(res1);
    if(res1.data.length > 0){
      $.each(res1.data, function(i, item){
        render_data.push(item);
      });
    }

    callUsercollectionListStep1++;
    if(callUsercollectionListStep1 < select_2_ol_ids.length){
      getUsercollectionListStep1();
    }else{
      renderData();
    }
  });
}

function renderData(){
  //console.log(render_data);

  var item_list_li = '';
  if(select_2 == "all"){
    $.each(render_data, function(i, item){

      item_list_li += '<li>';
      if(otId == "1"){ // 名師教學
        item_list_li +=   '<a href="./f_teacher_video.html?ol_id=' + item.ol_id + '&unit_ol_id=' + item.previous_nodes[2].ol_id + '&field_name=' + item.previous_nodes[5].ol_display_name + '&subject_name=' + item.previous_nodes[4].ol_display_name + '&version_name=' + (item.previous_nodes[3].ol_display_name == "不分版" ? "" : item.previous_nodes[3].ol_display_name) + '&grade_group_name=' + item.previous_nodes[2].ol_display_name + '&chapter_name=' + item.previous_nodes[1].ol_display_name +  '&part_name=' + item.ol_display_name + '&od_id=' + item.od_id + '" class="item_link">';
        item_list_li +=     '<button type="button" class="btn_remove" data-ol-id="' + item.ol_id + '" data-uc-id="' + item.uc_id + '"><img src="./images/icon/btn-delete.svg"></button>';
        item_list_li +=     '<img src="' + item.ol_img + '" class="link_img">';
        item_list_li +=     '<div class="desc_block">';
        item_list_li +=       '<p class="para1">' + item.ol_display_name + '</p>';
        item_list_li +=       '<p class="para2">' + item.od_name + '</p>';
        item_list_li +=     '</div>';
        item_list_li +=   '</a>';
        item_list_li += '</li>';
      }
      if(otId == "2"){ // 段考直播
        //alert($("#select2 option:eq(1)").val());
        item_list_li +=   '<a href="./exam_live_stream_video.html?ol_id=' + item.ol_id + '&unit_ol_id=' + item.previous_nodes[1].ol_id + '&field_name=' + item.previous_nodes[2].ol_display_name + '&chapter_name=' + item.previous_nodes[1].ol_display_name + '&part_name=' + item.previous_nodes[0].ol_display_name + '&select_1=' + item.previous_nodes[2].ol_id + '&select_2=' + item.previous_nodes[1].ol_id + '" class="item_link">';
        item_list_li +=     '<button type="button" class="btn_remove" data-ol-id="' + item.ol_id + '" data-uc-id="' + item.uc_id + '"><img src="./images/icon/btn-delete.svg"></button>';
        item_list_li +=     '<img src="' + item.ol_img + '" class="link_img">';
        item_list_li +=     '<div class="desc_block">';
        item_list_li +=       '<p class="para1">' + $("#select1 option:selected").text() + ' ' + item.ol_display_name + '</p>';
        item_list_li +=       '<p class="para2">' + item.od_name + '</p>';
        item_list_li +=     '</div>';
        item_list_li +=   '</a>';
        item_list_li += '</li>';
      }
      if(otId == "3"){ // 題目收藏
        if(item.previous_nodes.length < 7){
          console.log("層數有誤：");
          console.log(item);
        }
        //if(item.previous_nodes.length == 6){
          //item_list_li +=   '<a href="./my_learn_collection_q.html?ol_id=' + item.ol_id + '&subject_ol_id=' + item.previous_nodes[3].ol_id + '&uc_id=' + item.uc_id + '&tq_id=' + ((item.question && item.question.length > 0)?item.question[0].tq_id:"") + '&field_name=' + item.previous_nodes[4].ol_display_name + '&subject_name=' + item.previous_nodes[3].ol_display_name + '&version_name=' + (item.previous_nodes[2].ol_display_name == "不分版" ? "" : item.previous_nodes[2].ol_display_name) + '" class="item_link">';
        //}else{
        item_list_li +=   '<a href="./my_learn_collection_q.html?ol_id=' + item.ol_id + '&subject_ol_id=' + item.previous_nodes[4].ol_id + '&uc_id=' + item.uc_id + '&tq_id=' + ((item.question && item.question.length > 0)?item.question[0].tq_id:"") + '&field_name=' + item.previous_nodes[5].ol_display_name + '&subject_name=' + item.previous_nodes[4].ol_display_name + '&version_name=' + (item.previous_nodes[3].ol_display_name == "不分版" ? "" : item.previous_nodes[3].ol_display_name) + '&grade_group_name=' + item.previous_nodes[2].ol_display_name + '&chapter_name=' + item.previous_nodes[1].ol_display_name +  '&part_name=' + item.ol_display_name + '" class="item_link">';
        //}
        item_list_li +=     '<button type="button" class="btn_remove" data-ol-id="' + item.ol_id + '" data-uc-id="' + item.uc_id + '"><img src="./images/icon/btn-delete.svg"></button>';
        item_list_li +=     '<img src="' + item.ol_img + '" class="link_img">';
        item_list_li +=     '<div class="desc_block">';
        item_list_li +=       '<p class="para1">' + item.previous_nodes[1].ol_display_name + '</p>';
        item_list_li +=       '<p class="para2">' + item.ol_display_name + '</p>';
        item_list_li +=     '</div>';
        item_list_li +=   '</a>';
        item_list_li += '</li>';
      }
      if(otId == "4"){ // 電子講義
        item_list_li +=   '<a href="./m_e_paper.html?ol_id=' + item.ol_id + '&unit_ol_id=' + item.previous_nodes[2].ol_id + '&field_name=' + item.previous_nodes[5].ol_display_name + '&subject_name=' + item.previous_nodes[4].ol_display_name + '&version_name=' + (item.previous_nodes[3].ol_display_name == "不分版" ? "" : item.previous_nodes[3].ol_display_name) + '&grade_group_name=' + item.previous_nodes[2].ol_display_name + '&chapter_name=' + item.previous_nodes[1].ol_display_name +  '&part_name=' + item.ol_display_name + '" class="item_link">';
        item_list_li +=     '<button type="button" class="btn_remove" data-ol-id="' + item.ol_id + '" data-uc-id="' + item.uc_id + '"><img src="./images/icon/btn-delete.svg"></button>';
        item_list_li +=     '<img src="' + item.ol_img + '" class="link_img">';
        item_list_li +=     '<div class="desc_block">';
        item_list_li +=       '<p class="para1">' + item.previous_nodes[1].ol_display_name + '</p>';
        item_list_li +=       '<p class="para2">' + item.ol_display_name + '</p>';
        item_list_li +=     '</div>';
        item_list_li +=   '</a>';
        item_list_li += '</li>';
      }
      if(otId == "9"){ // 互動實驗
        item_list_li +=   '<a href="' + item.eps_path + '" class="item_link">';
        item_list_li +=     '<button type="button" class="btn_remove" data-ol-id="' + item.ol_id + '" data-eps-id="' + item.eps_id + '" data-uc-id="' + item.uc_id + '" data-status="1"><img src="./images/icon/btn-delete.svg"></button>';
        item_list_li +=     '<img src="' + item.ol_img + '" class="link_img">';
        item_list_li +=     '<div class="desc_block">';
        item_list_li +=       '<p class="para1">' + item.previous_nodes[1].ol_display_name + '</p>';
        item_list_li +=       '<p class="para2">' + item.ol_display_name + '</p>';
        item_list_li +=     '</div>';
        item_list_li +=   '</a>';
        item_list_li += '</li>';
      }
    });
  }else{
    $.each(render_data, function(i, item){

      //if(( otId != "2" && item.previous_nodes[1].ol_id == select_3) || (otId == "2" && item.previous_nodes[0].ol_id == select_2)){
        item_list_li += '<li>';
        if(otId == "1"){ // 名師教學
          item_list_li +=   '<a href="./f_teacher_video.html?ol_id=' + item.ol_id + '&unit_ol_id=' + item.previous_nodes[2].ol_id + '&field_name=' + item.previous_nodes[5].ol_display_name + '&subject_name=' + item.previous_nodes[4].ol_display_name + '&version_name=' + (item.previous_nodes[3].ol_display_name == "不分版" ? "" : item.previous_nodes[3].ol_display_name) + '&grade_group_name=' + item.previous_nodes[2].ol_display_name + '&chapter_name=' + item.previous_nodes[1].ol_display_name +  '&part_name=' + item.ol_display_name + '&od_id=' + item.od_id + '" class="item_link">';
          item_list_li +=     '<button type="button" class="btn_remove" data-ol-id="' + item.ol_id + '" data-uc-id="' + item.uc_id + '"><img src="./images/icon/btn-delete.svg"></button>';
          item_list_li +=     '<img src="' + item.ol_img + '" class="link_img">';
          item_list_li +=     '<div class="desc_block">';
          item_list_li +=       '<p class="para1">' + item.ol_display_name + '</p>';
          item_list_li +=       '<p class="para2">' + item.od_name + '</p>';
          item_list_li +=     '</div>';
          item_list_li +=   '</a>';
          item_list_li += '</li>';
        }
        if(otId == "2"){ // 段考直播
          item_list_li +=   '<a href="./exam_live_stream_video.html?ol_id=' + item.ol_id + '&unit_ol_id=' + item.previous_nodes[1].ol_id + '&field_name=' + item.previous_nodes[2].ol_display_name + '&chapter_name=' + item.previous_nodes[1].ol_display_name + '&part_name=' + item.previous_nodes[0].ol_display_name + '&select_1=' + item.previous_nodes[2].ol_id + '&select_2=' + item.previous_nodes[1].ol_id + '" class="item_link">';
          item_list_li +=     '<button type="button" class="btn_remove" data-ol-id="' + item.ol_id + '" data-uc-id="' + item.uc_id + '"><img src="./images/icon/btn-delete.svg"></button>';
          item_list_li +=     '<img src="' + item.ol_img + '" class="link_img">';
          item_list_li +=     '<div class="desc_block">';
          item_list_li +=       '<p class="para1">' + $("#select1 option:selected").text() + ' ' + item.previous_nodes[0].ol_display_name + '</p>';
          item_list_li +=       '<p class="para2">' + item.od_name + '</p>';
          item_list_li +=     '</div>';
          item_list_li +=   '</a>';
          item_list_li += '</li>';
        }
        if(otId == "3"){ // 題目收藏
          if(item.previous_nodes.length < 7){
            console.log("層數有誤：");
            console.log(item);
          }
          //if(item.previous_nodes.length == 6){
            //item_list_li +=   '<a href="./my_learn_collection_q.html?ol_id=' + item.ol_id + '&subject_ol_id=' + item.previous_nodes[3].ol_id + '&uc_id=' + item.uc_id + '&tq_id=' + ((item.question && item.question.length > 0)?item.question[0].tq_id:"") + '&field_name=' + item.previous_nodes[4].ol_display_name + '&subject_name=' + item.previous_nodes[3].ol_display_name + '&version_name=' + (item.previous_nodes[2].ol_display_name == "不分版" ? "" : item.previous_nodes[2].ol_display_name) + '" class="item_link">';
          //}else{
          item_list_li +=   '<a href="./my_learn_collection_q.html?ol_id=' + item.ol_id + '&subject_ol_id=' + item.previous_nodes[4].ol_id + '&uc_id=' + item.uc_id + '&tq_id=' + ((item.question && item.question.length > 0)?item.question[0].tq_id:"") + '&field_name=' + item.previous_nodes[5].ol_display_name + '&subject_name=' + item.previous_nodes[4].ol_display_name + '&version_name=' + (item.previous_nodes[3].ol_display_name == "不分版" ? "" : item.previous_nodes[3].ol_display_name) + '&grade_group_name=' + item.previous_nodes[2].ol_display_name + '&chapter_name=' + item.previous_nodes[1].ol_display_name +  '&part_name=' + item.ol_display_name + '" class="item_link">';
          //}
          item_list_li +=     '<button type="button" class="btn_remove" data-ol-id="' + item.ol_id + '" data-uc-id="' + item.uc_id + '"><img src="./images/icon/btn-delete.svg"></button>';
          item_list_li +=     '<img src="' + item.ol_img + '" class="link_img">';
          item_list_li +=     '<div class="desc_block">';
          item_list_li +=       '<p class="para1">' + item.previous_nodes[1].ol_display_name + '</p>';
          item_list_li +=       '<p class="para2">' + item.ol_display_name + '</p>';
          item_list_li +=     '</div>';
          item_list_li +=   '</a>';
          item_list_li += '</li>';
        }
        if(otId == "4"){ // 電子講義
          item_list_li +=   '<a href="./m_e_paper.html?ol_id=' + item.ol_id + '&unit_ol_id=' + item.previous_nodes[2].ol_id + '&field_name=' + item.previous_nodes[5].ol_display_name + '&subject_name=' + item.previous_nodes[4].ol_display_name + '&version_name=' + (item.previous_nodes[3].ol_display_name == "不分版" ? "" : item.previous_nodes[3].ol_display_name) + '&grade_group_name=' + item.previous_nodes[2].ol_display_name + '&chapter_name=' + item.previous_nodes[1].ol_display_name +  '&part_name=' + item.ol_display_name + '" class="item_link">';
          item_list_li +=     '<button type="button" class="btn_remove" data-ol-id="' + item.ol_id + '" data-uc-id="' + item.uc_id + '"><img src="./images/icon/btn-delete.svg"></button>';
          item_list_li +=     '<img src="' + item.ol_img + '" class="link_img">';
          item_list_li +=     '<div class="desc_block">';
          item_list_li +=       '<p class="para1">' + item.previous_nodes[1].ol_display_name + '</p>';
          item_list_li +=       '<p class="para2">' + item.ol_display_name + '</p>';
          item_list_li +=     '</div>';
          item_list_li +=   '</a>';
          item_list_li += '</li>';
        }
        if(otId == "9"){ // 互動實驗
          item_list_li +=   '<a href="' + item.eps_path + '" class="item_link" target="_blank">';
          item_list_li +=     '<button type="button" class="btn_remove" data-ol-id="' + item.ol_id + '" data-eps-id="' + item.eps_id + '" data-uc-id="' + item.uc_id + '" data-status="1"><img src="./images/icon/btn-delete.svg"></button>';
          item_list_li +=     '<img src="' + item.ol_img + '" class="link_img">';
          item_list_li +=     '<div class="desc_block">';
          item_list_li +=       '<p class="para1">' + item.previous_nodes[1].ol_display_name + '</p>';
          item_list_li +=       '<p class="para2">' + item.ol_display_name + '</p>';
          item_list_li +=     '</div>';
          item_list_li +=   '</a>';
          item_list_li += '</li>';
        }
      //}
    });
  }

  $("#item_list").html(item_list_li);

  $("#item_list").children("li").each(function(i){
    if(i > 14){
      $(this).addClass("no_display");
    }
  });

  setPager();
}

/**
 * 設定分頁
 */
function setPager() {
  var totalCount = $("#item_list").children("li").length;
  // $("#item_list").html(totalContent);
  var totalPage = Math.ceil(totalCount / 15);
  $("#total_page").html(str_pad(totalPage, 2, 0));
  if (totalPage > 1) {
    $("#pager_right_button").removeClass("-disabled");
  }
  if(totalPage == 0){
    $("#page_container").addClass("no_display");
  }
}
