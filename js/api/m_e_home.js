// 取得學生留言群組公告
function getHomeApp() {
	$(".txtNewMessage").hide();
	$(".new_solvederrata_counter").hide();
	$(".new_solved_counter").hide();
	$(".new_errata_counter").hide();
	$.ajax({
		url: g_obj.domain + '/api/home/app',
		type: 'GET',
		data: "token=" + Cookies.get('u_t'),
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					if (response.data.new_message != "0") {
						$(".txtNewMessage").show();
						$(".txtNewMessage").html('1');
					}

					if (response.data.new_solved + response.data.new_errata > 0) $(".new_solvederrata_counter").show();
					$(".new_solvederrata_counter").html((response.data.new_solved + response.data.new_errata > 99) ? '99' : response.data.new_solved + response.data.new_errata);

					if (response.data.new_solved != "0") $(".new_solved_counter").show();
					$(".new_solved_counter").html((response.data.new_solved > 99) ? '99' : response.data.new_solved);
					if (response.data.new_errata != "0") $(".new_errata_counter").show();
					$(".new_errata_counter").html((response.data.new_errata > 99) ? '99' : response.data.new_errata);
					break;
				case 1100:
				case 1101:
				case 1102:
				case 1103:
					alert(response.msg);
					location.href = "./login.html";
					break;
				default:
					alert(response.msg);
					break;
			}
		}
	});
}
