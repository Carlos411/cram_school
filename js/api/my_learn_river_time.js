var select_1 = ''; // 第一個下拉選單：領域
var select_2 = ''; // 第二個下拉選單：科目
var select_2_ol_ids = []; // 第二個下拉選單：科目
var select_3 = 2;  // 第三個下拉選單：cdt_id：年(2)、月(3)、週(4)、日(5)

var callForSelect2 = 0;

var data_arr_original = []; // 準備存資料：原始抓下來的各 ol_id 資料
var data_arr_date_and_seconds = []; // 準備存資料：準備顯示的所有日期及 vd_second(影片總秒數)、vr_second_total(影片觀看總秒數)

var totalCount = 0;

$(function(){
  // 取得 url 參數
  if($.url('?select1') != undefined){
    select_1 = $.url('?select1');
  }
  if($.url('?select2') != undefined){
    select_2 = $.url('?select2');
  }
  if($.url('?select3') != undefined){
    select_3 = $.url('?select3');
  }
  $("#select3").val(select_3);

  getOutlineRoot(g_obj.ec_id, g_obj.teacher_ot_id); // 學習記錄的 ot_id 與名師教學相同
});

/**
 * 取得大綱根節點資料
 */
function getOutlineRoot(ecId, otId) {
  $.get( g_obj.domain + '/api/outline/root?ec_id=' + ecId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    $.each(response.data, function( index, item ) {
       if(index == 0){
         getOutlineNextNodeForSelect1(item.ol_id, otId);
       }
       //return false;
    });
  });
}


/**
 * 取得領域資料
 */
function getOutlineNextNodeForSelect1(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "";
      $.each(response.data, function( index, item ) {
         if(index == 0){ // 33 為「最新版　全科」
           html += '<option value="' + item.ol_id + '" selected>' + item.ol_display_name + '</option>';
           if(select_1 == ""){
             select_1 = item.ol_id;
           }
         }else{
           html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
         }
      });
      $("#select1").append(html);
      if(select_1 != ""){
        $("#select1").val(select_1);
      }

      if(select_1 != "" && callForSelect2 == 0){
        callForSelect2++;
        getOutlineNextNodeForSelect2(select_1, otId);
      }

    }
  });
}

/**
 * 取得科目資料
 */
function getOutlineNextNodeForSelect2(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "<option value='all' selected>全部</option>";
      if(select_2 == "" || select_2 == "all"){ // 若網址無參數，或有 select_2為"all"時，將所有的 ol_ids 存至 select_2_ol_ids 陣列
        select_2 = "all";
        $.each(response.data, function( index, item ) {
          select_2_ol_ids.push(item.ol_id)
        });
      }else{
        select_2_ol_ids.push(select_2)
      }
      $.each(response.data, function( index, item ) {
         html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
      });
      $("#select2").append(html);
      $("#select2").val(select_2);
      $("#select2_text").html($("#select2 option:selected").text());
      getVideoRiverDate();
    }else{
      $("#page_container").addClass("no_display");
    }
  });
}

// 取得時間河流資料
/*
function getVideoRiverDate(){
  var call_times = 0;
  $.each(select_2_ol_ids, function( index_outer, item_ol_id ) {
    $.ajax({
      url: g_obj.domain + '/api/video/river/date?ol_id=' + item_ol_id + '&cdt_id=' + select_3 + '&token=' + Cookies.get('u_t'),
      type: 'get',
      // data: data,
      dataType: 'json',
      //headers: { 'token': Cookies.get('u_t') },
      success: function(res){
        if(res.data.length > 0){
          $.each(res.data, function( index, item_obj ) {
            data_arr_original.push(item_obj)
          });
        }
        call_times++;
        if( select_2_ol_ids.length == call_times ){
          calcDataArrOriginal();
        }
      }
    });
  });

}
*/

// 取得時間河流資料
var call_times = 0;
function getVideoRiverDate(){
  $.ajax({
    url: g_obj.domain + '/api/video/river/date?ol_id=' + select_2_ol_ids[call_times] + '&cdt_id=' + select_3 + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    //headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      if(res.data.length > 0){
        $.each(res.data, function( index, item_obj ) {
          data_arr_original.push(item_obj)
        });
      }
      call_times++;
      if( select_2_ol_ids.length == call_times ){
        calcDataArrOriginal();
      }else{
        getVideoRiverDate();
      }
    }
  });

}

// 處理 data_arr_original 裡的資料，另存至 data_arr_date_and_seconds
function calcDataArrOriginal(){
  var date_array = [];
  $.each(data_arr_original, function(index, item_obj){
    date_array.push(item_obj.vr_datetime);
  });
  date_array = _.uniq(date_array); // 排除重覆的
  if(select_3 == 5){
    date_array.sort(function(a,b){ // 日排序：最近的日期 至 最遠
      return new Date(b) - new Date(a);
    });
  }

  $.each(date_array, function(index, date_str){
    data_arr_date_and_seconds.push({
      vd_second: 0,
      vr_second_total: 0,
      vr_datetime: date_str
    });
  });

  $.each(data_arr_original, function(i, item_original){
    $.each(data_arr_date_and_seconds, function(j, item_obj){
      if(item_original.vr_datetime == item_obj.vr_datetime){
        data_arr_date_and_seconds[j].vd_second += item_original.vd_second
        data_arr_date_and_seconds[j].vr_second_total += item_original.vr_second_total
      }
    });
  });

  // 取得 vr_second_total 最大秒數
  /*
  var max_vr_second_total = 0;
  $.each(data_arr_date_and_seconds, function(i, item){
    if(item.vr_second_total > max_vr_second_total){
      max_vr_second_total = item.vr_second_total;
    }
  });
  */

  //console.log(max_vr_second_total);
  //console.log(data_arr_date_and_seconds);
  totalCount = data_arr_date_and_seconds.length;

  var html = '';
  $.each(data_arr_date_and_seconds, function(i, item){
    html += '<li class="' + (i >= 12 ? "no_display" : "") + '">';
    html +=   '<div class="bar_block">';
    html +=     '<div class="top_block">';
    html +=       '<span class="time">' + pad(Math.floor(item.vr_second_total / 60 / 60), 2, "0") + '小時 ' + pad(Math.floor(Math.floor(item.vr_second_total / 60) % 60), 2, "0") + '分鐘</span>';
    html +=     '</div>';
    html +=     '<div class="middle_block">';
    //html +=       '<div class="bar_item" style="height:' + ((item.vr_second_total / max_vr_second_total)*100) + '%;"></div>';
    html +=       '<div class="bar_item" style="height:' + ((item.vr_second_total / item.vd_second)*100) + '%;"></div>';
    html +=     '</div>';
    html +=     '<div class="bottom_block">';
    html +=       '<span class="date">' + item.vr_datetime + '</span>';
    html +=     '</div>';
    html +=   '</div>';
    html += '</li>';
  });
  $("#item_list").html(html);
  setPager();
}

// 左邊補零
function pad(input, length, padding){
  while((input = input.toString()).length + (padding = padding.toString()).length < length) {
    padding += padding;
  }
  return padding.substr(0, length - input.length) + input;
}

/**
 * 設定分頁
 */
function setPager() {
  //$("#item_list").html(totalContent);
  var totalPage = Math.ceil(totalCount / 12);
  $("#total_page").html(str_pad(totalPage, 2, 0));
  if (totalPage == 1) {
    $("#pager_right_button").addClass("-disabled");
  }
  if(totalPage == 0){
    $("#page_container").addClass("no_display");
  }
  if (totalPage > 1) {
    $("#pager_right_button").removeClass("-disabled");
  }
}
