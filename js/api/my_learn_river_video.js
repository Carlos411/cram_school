var select_1 = ''; // 第一個下拉選單：領域
var select_2 = ''; // 第二個下拉選單：科目
//var select_2_ol_ids = []; // 第二個下拉選單：科目

var callForSelect2 = 0;

var data_arr_original = []; // 準備存資料：原始抓下來的各 ol_id 資料

$(function(){
  // 取得 url 參數
  if($.url('?select1') != undefined){
    select_1 = $.url('?select1');
  }
  if($.url('?select2') != undefined){
    select_2 = $.url('?select2');
  }

  getOutlineRoot(g_obj.ec_id, g_obj.teacher_ot_id); // 學習記錄的 ot_id 與名師教學相同
});

/**
 * 取得大綱根節點資料
 */
function getOutlineRoot(ecId, otId) {
  $.get( g_obj.domain + '/api/outline/root?ec_id=' + ecId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    $.each(response.data, function( index, item ) {
      if(index == 0){
        getOutlineNextNodeForSelect1(item.ol_id, otId);
      }
       //return false;
    });
  });
}


/**
 * 取得領域資料
 */
function getOutlineNextNodeForSelect1(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "";
      $.each(response.data, function( index, item ) {
         if(index == 0){ // 33 為「最新版　全科」
           html += '<option value="' + item.ol_id + '" selected>' + item.ol_display_name + '</option>';
           if(select_1 == ""){
             select_1 = item.ol_id;
           }
         }else{
           html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
         }
      });
      $("#select1").append(html);
      if(select_1 != ""){
        $("#select1").val(select_1);
      }

      if(select_1 != "" && callForSelect2 == 0){
        callForSelect2++;
        getOutlineNextNodeForSelect2(select_1, otId);

        // 取得影片河流
        getVideoRiver();
      }

    }
  });
}

/**
 * 取得科目資料
 */
function getOutlineNextNodeForSelect2(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "<option value='all' selected>全部</option>";
      if(select_2 == "" || select_2 == "all"){ // 若網址無參數，或有 select_2為"all"時，將所有的 ol_ids 存至 select_2_ol_ids 陣列
        select_2 = "all";
        //$.each(response.data, function( index, item ) {
          //select_2_ol_ids.push(item.ol_id)
        //});
      }else{
        //select_2_ol_ids.push(select_2)
      }
      $.each(response.data, function( index, item ) {
         html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
      });
      $("#select2").append(html);
      $("#select2").val(select_2);

      //$("#select2_text").html($("#select2 option:selected").text());
      //getVideoRiver();
    }
    //else{
      //$("#page_container").addClass("no_display");
    //}
  });
}

// 取得影片河流資料
function getVideoRiver(){

  //var call_times = 0;
  $.ajax({
    url: g_obj.domain + '/api/video/river?ol_id=' + select_1 + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    //headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      //console.log(res);
      $("#select1_text").text(res.data.ol_display_name);
      $("#select1_vd_second").text(Math.floor(res.data.vd_second / 60 / 60) + '小時' + Math.floor(Math.floor(res.data.vd_second / 60) % 60) + '分鐘');
      $("#select1_vr_second_total").text(Math.floor(res.data.vr_second_total / 60 / 60) + '小時' + Math.floor(Math.floor(res.data.vr_second_total / 60) % 60) + '分鐘');

      // 大的瓶子
      var big_bottle = '';
      big_bottle += '<div class="subject_block">';
      big_bottle +=   '<div class="subject -empty" style="height: ' + (100 - res.data.percent) + '%;"></div>'; // 空的佔位
      big_bottle +=   '<div class="subject -water" style="height: ' + res.data.percent + '%;"></div>'; // 水
      big_bottle += '</div>';
      $("#main_bottle").html(big_bottle);

      if(res.data.nodes.length > 0){
        var small_bottle = '';
        $.each(res.data.nodes, function( index, item_obj ) {
          small_bottle += '<li>';
          small_bottle +=   '<div class="bottle_parent">';
          small_bottle +=     '<p class="para1 -small">' + Math.floor(item_obj.vr_second_total / 60 / 60) + '小時' + Math.floor(Math.floor(item_obj.vr_second_total / 60) % 60) + '分鐘' + '</p>';
          small_bottle +=     '<div class="bottle -small">';
          small_bottle +=       '<div class="subject_block">';
          small_bottle +=         '<div class="subject -empty" style="height: ' + (100 - item_obj.percent) + '%;"></div>'; // 空的佔位
          small_bottle +=         '<div class="subject -water" style="height: ' + item_obj.percent + '%;"></div>'; // 水
          small_bottle +=       '</div>';
          small_bottle +=       '<span class="subject_text">' + item_obj.ol_display_name + '</span>';
          small_bottle +=     '</div>';
          small_bottle +=     '<p class="para6">' + Math.floor(item_obj.vd_second / 60 / 60) + '小時' + Math.floor(Math.floor(item_obj.vd_second / 60) % 60) + '分鐘' + '</p>';
          small_bottle +=   '</div>';
          small_bottle += '</li>';
        });
        $("#bottle_list").html(small_bottle);
      }
      /*
      call_times++;
      if( select_2_ol_ids.length == call_times ){
        calcDataArrOriginal();
      }
      */
    }
  });

}
