// 取得使用者最近筆記
function getUsernoteRecent(){
  $.ajax({
    url: g_obj.domain + '/api/usernote/recent?token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    //headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      if(res.data.length > 0){
        var item_list_li = "";
        $.each(res.data, function(i, item){
          var ol_display_name_array = (item.ol_display_name).split("/");
          item_list_li += '<li>';
          item_list_li +=   '<a data-ol-id="' + item.ol_id + '" href="./my_learn_notes_list_single.html?ol_id=' + item.ol_id + '&unit_ol_id=' + item.parents[2].ol_id + '&field_name=' + (ol_display_name_array[1]).trim() + '&subject_name=' + (ol_display_name_array[2]).trim() + '&version_name=' + ((ol_display_name_array[3]).trim() == "不分版" ? "" : (ol_display_name_array[3]).trim()) + '&grade_group_name=' + (ol_display_name_array[4]).trim() + '&chapter_name=' + (ol_display_name_array[5]).trim() + '&part_name=' + (ol_display_name_array[6]).trim() + '&un_id=' + item.un_id + '" class="item_link">';
          item_list_li +=     '<img src="' + item.un_path + '" class="link_img">';
          item_list_li +=     '<div class="desc_block">';
          item_list_li +=       '<p class="para1">' + ol_display_name_array[5] + '</p>';
          item_list_li +=       '<p class="para2">' + ol_display_name_array[6] + '</p>';
          item_list_li +=     '</div>';
          item_list_li +=   '</a>';
          item_list_li += '</li>';
        });
        $("#recently_view_item_list").html(item_list_li);
      }else{
        $("#recently_view_item_list").html("");
      }
    }
  });
}
getUsernoteRecent();
