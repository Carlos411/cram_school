// 取得最近考試記錄
function getTestRecordRecent(id_name, tr_type){

  if(tr_type == 1){ // 1 是 考卷測驗
    $.get( g_obj.domain + '/api/test/record/recent?token=' + Cookies.get('u_t') + '&tr_type=' + tr_type, function( res ) {
      //console.log(res);
      if(res.data.length > 0){
        var recent_html = '';
        $.each(res.data, function(i, item){
          /*if(item.parents.length < 7){
            console.log("這");
            console.log(item);
          }*/
          recent_html += '<li>';
          recent_html +=   '<a href="./my_learn_record_score.html?ol_id=' + item.parents[2].ol_id + '&tr_id=' + item.tr_id + '&subject_name=' + item.parents[4].ol_display_name + '" class="item_link">';
          recent_html +=     '<div class="top_block">';
          recent_html +=       '<p class="para_title">' + item.parents[4].ol_display_name + ' ' + item.parents[2].ol_display_name + '</p>';
          recent_html +=     '</div>';
          recent_html +=     '<div class="middle_block">';
          recent_html +=       '<p class="para1">' + item.parents[1].ol_display_name + '</p>';
          recent_html +=       '<p class="para2">' + item.parents[0].ol_display_name + '</p>';
          recent_html +=     '</div>';
          recent_html +=     '<div class="bottom_block">';
          recent_html +=       '<p class="para_right -right">' + ((((item.tr_datetime).split(" "))[1]).split(":"))[0] + ":" + ((((item.tr_datetime).split(" "))[1]).split(":"))[1] + '</p>';
          recent_html +=       '<p class="para_left">' + (((item.tr_datetime).split(" "))[0]).replace(/-/g, "/") + '</p>';
          recent_html +=     '</div>';
          recent_html +=     '<span class="score ' + (item.grade >= 60 ?"-more":"-less") + '">' + item.grade + '</span>';
          recent_html +=   '</a>';
          recent_html += '</li>';
        });
        $("#" + id_name).html(recent_html);
      }else{
        $("#" + id_name).html("");
      }
    });
  }

  if(tr_type == 2){ // 2 是 隨堂練習
    $.get( g_obj.domain + '/api/test/record/recent?token=' + Cookies.get('u_t') + '&tr_type=' + tr_type, function( res ) {
      //console.log(res);
      if(res.data.length > 0){
        var recent_html = '';
        $.each(res.data, function(i, item){
          var test_wrong_count_total = 0;
          $.each(item.questions, function(j, q){
            if(q.ta_correct == 0){
              test_wrong_count_total++;
            }
          });
          recent_html += '<li>';
          recent_html +=   '<a href="./q_db_exam.html?tr_id=' + item.tr_id + '&ol_id=' + item.parents[0].ol_id + '&unit_ol_id=' + item.parents[2].ol_id + '&field_name=' + item.parents[5].ol_display_name + '&subject_name=' + item.parents[4].ol_display_name + '&version_name=' + (item.parents[3].ol_display_name == "不分版" ? "" : item.parents[3].ol_display_name ) + '&grade_group_name=' + item.parents[2].ol_display_name + '&chapter_name=' + item.parents[1].ol_display_name + '&part_name=' + item.parents[0].ol_display_name + '" class="item_link">';
          recent_html +=     '<ul class="wrong_or_not_list">';
          $.each(item.questions, function(j, q){
            recent_html +=     '<li><span class="' + (q.ta_correct == 1 ? "-right" : "-wrong" ) + '">' + q.number + '</span></li>';
          });
          recent_html +=     '</ul>';
          //recent_html +=     '<img src="http://via.placeholder.com/190x120" class="link_img">';
          recent_html +=     '<div class="desc_block">';
          recent_html +=       '<p class="para1">' + item.parents[4].ol_display_name + (item.parents[3].ol_display_name == "不分版" ? "" : item.parents[3].ol_display_name) + item.parents[2].ol_display_name + ' ' + item.parents[1].ol_display_name + '</p>';
          recent_html +=       '<p class="para2">' + item.parents[0].ol_display_name + '</p>';
          recent_html +=     '</div>';
          recent_html +=   '</a>';
          recent_html += '</li>';
        });
        $("#" + id_name).html(recent_html);
      }else{
        $("#" + id_name).html("");
      }
    });
  }
}
