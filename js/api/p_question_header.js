/**
 * 設定導覽列年級/群組連結
 */
function setHeaderGradeGroupHref(olId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/lastNode?ol_id=' + olId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $.each(response.data, function( index, item ) {
         setHeaderGradeGroupHref2(item.ol_id); // 真正所需的資料還要往上一個節點拿
         return false;
      });
    }
  });
}


/**
 * 設定導覽列年級/群組連結
 */
function setHeaderGradeGroupHref2(olId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/lastNode?ol_id=' + olId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $.each(response.data, function( index, item ) {
         $("#header_grade_group").attr("href", "./p_question_grade_group.html?ol_id=" + item.ol_id + "&field_name=" + fieldName + "&subject_name=" + subjectName);
         setHeaderSubjectHref(item.ol_id);
         return false;
      });
    }
  });
}


/**
 * 設定導覽列科目連結
 */
function setHeaderSubjectHref(olId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/lastNode?ol_id=' + olId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $.each(response.data, function( index, item ) {
         $("#header_subject").attr("href", "./p_question_subject.html?ol_id=" + item.ol_id + "&field_name=" + fieldName);
         return false;
      });
    }
  });
}
