var us_id;

$(function(){
	// 取得頁面間傳遞參數
	us_id = $.url('?us_id');
});

// 新增一個學生及基本資料
function newUserStuden() {
	if ($.trim($("#us_password").val()) != $.trim($("#us_password2").val())) {
		alert("二次密碼輸入不一樣");
		return;
	}
	var fData = new FormData($("#form_user_edit")[0]);	// 表單名稱請依實際修改
	fData.append("token", Cookies.get('u_t'));
	fData.append("dv_id", "4");
	fData.append("upd_id", parent_devices_upd_id);
	if ($("#birthY").val() != "" || $("#birthM").val() != "" || $("#birthD").val() != "") fData.append("us_birthday", $("#birthY").val() + "-" + ("0" + $("#birthM").val()).substring(("0" + $("#birthM").val()).length - 2,("0" + $("#birthM").val()).length) + "-" + ("0" + $("#birthD").val()).substring(("0" + $("#birthD").val()).length - 2,("0" + $("#birthD").val()).length));
	fData.append("us_phone", $("#us_phone1").val() + "-" + $("#us_phone2").val());
	$.ajax({
		url: g_obj.domain + '/api/user/student',
		type: 'post',
		data: fData,
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (parseInt(response.status, 10)) {
				case 1000:
					alert("新學生編號: " + response.data.us_id);
					window.location.reload();
					break;
				case 3015:	// 學生密碼未填
					$("#us_password").focus();
					alert(response.msg);
					break;
				case 3013:	// 學生姓名未填
				case 3014:	// 學生姓名長度過長
					$("#us_name").focus();
					alert(response.msg);
					break;
				case 3016:	// 學生行動電話未填
				case 3017:	// 學生行動電話格式無效
					$("#us_cell_phone").focus();
					alert(response.msg);
					break;
				case 3018:	// 學生電子郵件格式無效
					$("#us_mail").focus();
					alert(response.msg);
					break;
				case 3019:	// 學生生日格式無效
					$("#birthY").focus();
					alert(response.msg);
					break;
				case 3020:	// 學生學校長度過長
					$("#us_school").focus();
					alert(response.msg);
					break;
				case 3024:	// 學生年級過長
					$("#us_grade").focus();
					alert(response.msg);
					break;
				case 3021:	// 學生班別長度過長
					$("#us_class").focus();
					alert(response.msg);
					break;
				default:
					alert(response.msg);
					/*
						3800: // 裝置id無效
						3801: // 家長裝置id無效
						3022: // 學生圖片不為圖片檔
					*/
					break;
			}
		}
	});
}

// 修改一個學生基本資料
function updateUserStudent() {
	if ($.trim($("#us_password").val()) != $.trim($("#us_password2").val())) {
		$("#us_password").focus();
		alert("二次密碼輸入不一樣");
		return;
	}
	var fData = new FormData($("#form_user_edit")[0]);	// 表單名稱請依實際修改
	if ($.trim($("#us_password").val()) == "") {
		fData.delete("us_password");
		fData.delete("us_password2");
	}
	fData.append("token", Cookies.get('u_t'));
	fData.append("us_id", us_id);
	fData.append("dv_id", "4");
	fData.append("upd_id", "5");
	if ($("#birthY").val() != "" || $("#birthM").val() != "" || $("#birthD").val() != "") fData.append("us_birthday", $("#birthY").val() + "-" + ("0" + $("#birthM").val()).substring(("0" + $("#birthM").val()).length - 2,("0" + $("#birthM").val()).length) + "-" + ("0" + $("#birthD").val()).substring(("0" + $("#birthD").val()).length - 2,("0" + $("#birthD").val()).length));
	fData.append("us_phone", $("#us_phone1").val() + "-" + $("#us_phone2").val());
	$.ajax({
		url: g_obj.domain + '/api/user/student/update',
		type: 'post',
		data: fData,
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (parseInt(response.status, 10)) {
				case 1000:
					alert("修改完成");
					window.location.href = "user.html";
					break;
				case 3015:	// 學生密碼未填
					$("#us_password").focus();
					alert(response.msg);
					break;
				case 3013:	// 學生姓名未填
				case 3014:	// 學生姓名長度過長
					$("#us_name").focus();
					alert(response.msg);
					break;
				case 3016:	// 學生行動電話未填
				case 3017:	// 學生行動電話格式無效
					$("#us_cell_phone").focus();
					alert(response.msg);
					break;
				case 3018:	// 學生電子郵件格式無效
					$("#us_mail").focus();
					alert(response.msg);
					break;
				case 3019:	// 學生生日格式無效
					$("#birthY").focus();
					alert(response.msg);
					break;
				case 3020:	// 學生學校長度過長
					$("#us_school").focus();
					alert(response.msg);
					break;
				case 3024:	// 學生年級過長
					$("#us_grade").focus();
					alert(response.msg);
					break;
				case 3021:	// 學生班別長度過長
					$("#us_class").focus();
					alert(response.msg);
					break;
				default:
					alert(response.msg);
					/*
						3001: // 學生id無效
						3800: // 裝置id無效
						3801: // 家長裝置id無效
						3022: // 學生圖片不為圖片檔
					*/
					break;
			}
		}
	});
}

// 新增一個家長基本資料
function newUserParent() {
	var fData = new FormData($("#submit_user")[0]);	// 表單名稱請依實際修改
	fData.append("token", Cookies.get('u_t'));
	$.ajax({
		url: g_obj.domain + '/api/user/parent',
		type: 'post',
		data: fData,
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (parseInt(response.status, 10)) {
				case 1000:
					alert("新增完成");
					break;
				default:
					alert(response.msg);
					/*
						3003: // 家長姓名未填
						3004: // 家長姓名長度過長
						3005: // 家長密碼未填
						3006: // 家長行動電話未填
						3007: // 家長行動電話格式無效
						3008: // 家長電話長度過長
						3009: // 家長電子郵件格式無效
						3010: // 家長地址(城市)長度過長
						3011: // 家長地址(鄉鎮市區)長度過長
						3012: // 家長行動電話格式無效
					*/
					break;
			}
		}
	});
}

// 修改一個家長基本資料
function updateUserParent() {
	if ($.trim($("#up_password").val()) != $.trim($("#up_password2").val())) {
		$("#up_password").focus();
		alert("二次密碼輸入不一樣");
		return;
	}
	var fData = new FormData($("#form_user_edit")[0]);	// 表單名稱請依實際修改
	if ($.trim($("#up_password").val()) == "") {
		fData.delete("up_password");
		fData.delete("up_password2");
	}
	fData.append("token", Cookies.get('u_t'));
	// fData.append("up_birthday", $("#birthY").val() + "-" + ("0" + $("#birthM").val()).substring(("0" + $("#birthM").val()).length - 2,("0" + $("#birthM").val()).length) + "-" + ("0" + $("#birthD").val()).substring(("0" + $("#birthD").val()).length - 2,("0" + $("#birthD").val()).length));
	if ($("#birthY").val() != "" || $("#birthM").val() != "" || $("#birthD").val() != "") fData.append("up_birthday", $("#birthY").val() + "-" + ("0" + $("#birthM").val()).substring(("0" + $("#birthM").val()).length - 2,("0" + $("#birthM").val()).length) + "-" + ("0" + $("#birthD").val()).substring(("0" + $("#birthD").val()).length - 2,("0" + $("#birthD").val()).length));
	// else fData.append("up_birthday", "");
	fData.append("up_phone", $("#up_phone1").val() + "-" + $("#up_phone2").val());
	fData = fData.entries();
	var qString = "";
	var obj = fData.next();
	while(undefined !== obj.value) {    
		qString += ((qString != "") ? "&" : "") + obj.value[0] + "=" + obj.value[1];
		obj = fData.next();
	}
	$.ajax({
		url: g_obj.domain + '/api/user/parent',
		type: 'PUT',
		data: qString,
		headers: {
            "token": Cookies.get('u_t'),
            "Content-Type": "application/x-www-form-urlencoded"
        },
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (parseInt(response.status, 10)) {
				case 1000:
					alert("修改完成");
					window.location.href = "user_parents.html";
					break;
				default:
					alert(response.msg);
					/*
						3000: // 家長id無效
						3003: // 家長姓名未填
						3004: // 家長姓名長度過長
						3005: // 家長密碼未填
						3006: // 家長行動電話未填
						3007: // 家長行動電話格式無效
						3008: // 家長電話長度過長
						3009: // 家長電子郵件格式無效
						3010: // 家長地址(城市)長度過長
						3011: // 家長地址(鄉鎮市區)長度過長
						3012: // 家長行動電話格式無效
					*/
					break;
			}
		}
	});
}

// 新增一個家長裝置
function newUserParentDevice() {
	var fData = new FormData($("#submit_user")[0]);	// 表單名稱請依實際修改
	fData.append("token", Cookies.get('u_t'));
	$.ajax({
		url: g_obj.domain + '/api/user/parent/device',
		type: 'post',
		data: fData,
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (parseInt(response.status, 10)) {
				case 1000:
					alert("新增完成");
					break;
				default:
					alert(response.msg);
					/*
						3000: // 家長id無效
						3800: // 裝置id無效
						3803: // 使用者家長裝置id父節點無效
						3804: // 使用者家長裝置唯一碼過長
					*/
					break;
			}
		}
	});
}

// 修改一個家長裝置
function updateUserParentDevice() {
	var fData = new FormData($("#submit_user")[0]);	// 表單名稱請依實際修改
	fData.append("token", Cookies.get('u_t'));
	fData = fData.entries();
	var qString = "";
	var obj = fData.next();
	while(undefined !== obj.value) {    
		qString += ((qString != "") ? "&" : "") + obj.value[0] + "=" + obj.value[1];
		obj = fData.next();
	}
	$.ajax({
		url: g_obj.domain + '/api/user/parent/device',
		type: 'PUT',
		data: qString,
		headers: {
            "token": Cookies.get('u_t'),
            "Content-Type": "application/x-www-form-urlencoded"
        },
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (parseInt(response.status, 10)) {
				case 1000:
					alert("修改完成");
					break;
				default:
					alert(response.msg);
					/*
						3000: // 家長id無效
						3001: // 家長裝置id無效
						3800: // 裝置id無效
						3803: // 使用者家長裝置id父節點無效
						3804: // 使用者家長裝置唯一碼過長
					*/
					break;
			}
		}
	});
}

// 取得學生年級列表
function getUserStudentEducation() {
	$.ajax({
		url: g_obj.domain + '/api/user/student/education',
		type: 'GET',
		data: "token=" + Cookies.get('u_t'),
		dataType: 'json',
		async: false,
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (parseInt(response.status, 10)) {
				case 1000:
					console.log(response.data);
					var tmpEdu = "";
					var educationData = response.data;
					for(var i = 0; i < educationData.length; i++) {
						if (educationData[i].ec_id == 3) tmpEdu += "<option value='" + educationData[i].usec_id + "'>" + educationData[i].usec_name + "</option>";
					}
					$("#us_grade").html(tmpEdu);
					break;
				default:
					alert(response.msg);
					break;
			}
		}
	});
}

// 切換身份
function changeStudent(us_id) {
	$.ajax({
		url: g_obj.domain + '/api/user/parent/cloud',
		type: 'GET',
		data: "token=" + Cookies.get('u_t') + "&us_id=" + us_id,
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (parseInt(response.status, 10)) {
				case 1000:
					Cookies.set('u_t', response.data.token);
					// console.log(response.data.token);
					// window.location.href = "./user.html";
					window.location.href = "./";
					break;
				default:
					alert(response.msg);
					break;
			}
		}
	});
}

// 取得家長忘記密碼
function userParentForget() {
	if ($.trim($("#forget_pwd_user").val()) == "") {
		alert("請輸入使用者帳號");
		$("#forget_pwd_user").focus();
		return false;
	}
	if ($.trim($("#forget_pwd_user_phone").val()) == "") {
		alert("請輸入手機號碼");
		$("#forget_pwd_user_phone").focus();
		return false;
	}
	$.ajax({
		url: g_obj.domain + '/api/user/parent/forget',
		type: 'GET',
		data: "up_name=" + $.trim($("#forget_pwd_user").val()) + "&up_cell_phone=" + $.trim($("#forget_pwd_user_phone").val()),
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (parseInt(response.status, 10)) {
				case 1000:
					$(".h1password").html("您的密碼是" + response.data.up_password + "，不要忘記囉！");
					$('#forget_pwd_success_modal').modal();
					break;
				default:
					$('#forget_pwd_fail_modal').modal();
					break;
			}
		}
	});
}

// 取得學生忘記密碼
function userStudentForget() {
	if ($.trim($("#forget_pwd_user").val()) == "") {
		alert("請輸入使用者帳號");
		$("#forget_pwd_user").focus();
		return false;
	}
	if ($.trim($("#forget_pwd_user_phone").val()) == "") {
		alert("請輸入手機號碼");
		$("#forget_pwd_user_phone").focus();
		return false;
	}
	$.ajax({
		url: g_obj.domain + '/api/user/student/forget',
		type: 'GET',
		data: "us_name=" + $.trim($("#forget_pwd_user").val()) + "&us_cell_phone=" + $.trim($("#forget_pwd_user_phone").val()),
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (parseInt(response.status, 10)) {
				case 1000:
					$(".h1password").html("您的密碼是" + response.data.us_password + "，不要忘記囉！");
					$('#forget_pwd_success_modal').modal();
					break;
				default:
					$('#forget_pwd_fail_modal').modal();
					break;
			}
		}
	});
}

// 學生登出
function userLogout() {
	if (confirm("確定要登出嗎?")) {
		Cookies.set('u_t', '');
		window.location.href = "./login.html";
	}
}

// 家長登出
function parentLogout() {
	if (confirm("確定要登出嗎?")) {
		Cookies.set('u_t', '');
		window.location.href = "./login_parents.html";
	}
}