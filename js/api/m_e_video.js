var ol_id;

$(function(){
	// 取得頁面間傳遞參數
	ol_id = $.url('?ol_id');
	if (typeof ol_id == "undefined") ol_id = "";
});

// 新增影片觀看記錄
function newVideoRecord(vd_id, vr_second_from, vr_second_to, vr_focus) {
	if (vr_second_from < vr_second_to && vd_id > 0) {
		var fData = new FormData();	// 表單名稱請依實際修改
		fData.append("token", Cookies.get('u_t'));
		fData.append("ol_id", ol_id);
		fData.append("vd_id", vd_id);
		fData.append("vr_second_from", vr_second_from);
		fData.append("vr_second_to", vr_second_to);
		fData.append("vr_focus", vr_focus);
		$.ajax({
			url: g_obj.domain + '/api/video/record',
			type: 'post',
			data: fData,
			dataType: 'json',
			processData: false,
			contentType: false,
			statusCode: {
				200: function (response) {
					// console.log(response);
					// 成功
				}
			},
			success: function(response){
				console.log(response);
				switch (response.status) {
					case 1000:
						// 執行成功要做的事
						// alert(response.msg);
						break;
					default:
						// alert(response.msg);
						/*
							2400: // 影片id無效
							2407: // 影片記錄開始秒數無效
							2408: // 影片記錄結束秒數無效
						*/
						break;
				}
			}
		});
	}
}

// 新增影片觀看記錄
function newVideoRecordList(vd_id, vr_second_from, vr_second_to, vr_focus) {
	/* 參考 JSON: {"token": "testToken", "records": [{"ol_id": 58, "vd_id": 6, "vr_second_from": 0, "vr_second_to": 10, "vr_focus": 100}]} */
	var token = Cookies.get('u_t');
	var records = [];
	var vd_id = vd_id;
	var vr_second_from = vr_second_from;
	var vr_second_to = vr_second_to;
	var vr_focus = vr_focus;
	var record = { ol_id, vd_id, vr_second_from, vr_second_to, vr_focus };
	records.push(record);
	var olIDArray = { token, records };
	var myJSON = JSON.stringify(olIDArray);
	console.log("ol_id: " + ol_id);
	console.log(myJSON);
	if (vr_second_from < vr_second_to && vd_id > 0) {
		$.ajax({
			url: g_obj.domain + '/api/video/record/list',
			type: 'POST',
			data: myJSON,
			dataType: 'json',
			contentType: "application/json; charset=utf-8",
			statusCode: {
				200: function (response) {
					// console.log(response);
					// 成功
				}
			},
			success: function(response){
				console.log(response);
				switch (response.status) {
					case 1000:
						// 執行成功要做的事
						// alert(response.msg);
						break;
					default:
						// alert(response.msg);
						/*
							2400: // 影片id無效
							2407: // 影片記錄開始秒數無效
							2408: // 影片記錄結束秒數無效
						*/
						break;
				}
			}
		});
	}
}

// 取得影片縮圖
function getVideoThumb(vd_id, second) {
	$("#videoPreviewThumb").hide();
	$("#videoPreviewThumb").attr("src", g_obj.domain + "/api/video/thumb?token=" + Cookies.get('u_t') + "&vd_id=" + vd_id + "&second=" + second).on('load', function() {
		if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
            // 失敗不處理
        } else {
			$("#videoPreviewThumb").show();
        }
    });
}

// 新增/修改一個影片評分
function newVideoRating(vd_id, vdrating_is_spontaneous, vdrating_rating, vdrating_content) {
	var fData = new FormData();	// 表單名稱請依實際修改
	fData.append("token", Cookies.get('u_t'));
	fData.append("dv_id", 4);
	fData.append("vd_id", vd_id);
	fData.append("vdrating_is_spontaneous", vdrating_is_spontaneous);
	fData.append("vdrating_rating", vdrating_rating);
	fData.append("vdrating_content", vdrating_content);
	$.ajax({
		url: g_obj.domain + '/api/video/rating',
		type: 'post',
		data: fData,
		dataType: 'json',
		async: false,
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					break;
				default:
					// alert(response.msg);
					/*
						2400: // 影片id無效
						2407: // 影片記錄開始秒數無效
						2408: // 影片記錄結束秒數無效
					*/
					break;
			}
		}
	});
}