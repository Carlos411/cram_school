var olId;
var fieldName;

$(function(){
  // 取得頁面間傳遞參數
  olId = $.url('?ol_id');
  fieldName = $.url('?field_name');

  // 設定導覽列
  checkHeaderTextField(olId);
  //$("#header_field").text(fieldName);
  $("#header_subject").attr("href", "#");

  getOutlineNextNode(olId, g_obj.my_learn_notes_ot_id);

  // 最近觀看
  //get_video_recent("video_recent_item_list");

  left_aside_link(1); // 更換大類別連結
});

/**
 * 取得科目資料
 */
function getOutlineNextNode(olId, otId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        var html = "";

        $.each(response.data, function( index, item ) {
           // console.log(item.ol_id);
           html += '<li><a href="./my_learn_notes_grade_group.html?ol_id=' + item.ol_id + '&field_name=' + fieldName + '&subject_name=' + item.ol_display_name + '"><img src="'+ item.ol_img +'" class="bg_img"><span class="-text">' + item.ol_display_name + '</span></a></li>';
        });

        $("#tab_content_list").html(html);
      }else{
        $("#tab_content_list").html("");
      }
    }
  });
}
