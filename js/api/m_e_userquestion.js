var ol_id, uq_id;

$(function(){
	// 取得頁面間傳遞參數
	ol_id = $.url('?ol_id');
	if (typeof ol_id == "undefined") ol_id = "";
	uq_id = $.url('?uq_id');
	if (typeof uq_id == "undefined") uq_id = "";
});

// 新增發問
var new_uq_id = "";
function newUserQuestion(imgBase64_1, imgBase64_2, imgBase64_3, title, content, ot_id, tq_id, uq_type, t_ol_id) {
	var rtnStatus = false;
	var fData = new FormData();
	fData.append("token", Cookies.get('u_t'));
	if (uq_id != "") fData.append("uq_id_parent", uq_id);
	fData.append("ol_id", (typeof t_ol_id != "undefined" && t_ol_id != "") ? t_ol_id : ((ol_id == "" && $('#tmp_ol_id').length > 0) ? $('#tmp_ol_id').val() : ol_id));
	fData.append("ot_id", ot_id);
	fData.append("dv_id", "4");
	if (tq_id != "") fData.append("tq_id", tq_id);
	fData.append("uq_type", (uq_type != 1) ? 0 : uq_type);
	fData.append("uq_title", title);
	fData.append("uq_question_content", content);
	fData.append("uq_question_img_1_base64", imgBase64_1);
	fData.append("uq_question_img_2_base64", imgBase64_2);
	fData.append("uq_question_img_3_base64", imgBase64_3);
	$.ajax({
		url: g_obj.domain + '/api/userQuestion/student',
		type: 'post',
		data: fData,
		dataType: 'json',
		async: false,
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					new_uq_id = response.data.uq_id;
					rtnStatus = true;
					break;
				default:
					alert(response.msg);
					/*
						3701: // 學生發問父節點id無效
						2009: // 大綱id無效
						2002: // 大綱類型id無效
						3702: // 學生發問標題未填
						3703: // 學生發問標題長度過長
						3704: // 學生發問內容未填
						3705: // 學生發問內容長度過長
						3706: // 學生發問圖片不為圖片檔
						3800: // 裝置id無效
					*/
					break;
			}
		}
	});
	return rtnStatus;
}
// 取得是否為解題時間
function userQuestionSolvetimeStudent() {
	var rtnStatus = false;
	$.ajax({
		url: g_obj.domain + '/api/userQuestion/solvetime/student',
		type: 'GET',
		data: "token=" + Cookies.get('u_t'),
		dataType: 'json',
		async: false,
		statusCode: {
			200: function (response) {
				
			}
		},
		success: function(response){
			// console.log(response);
			rtnStatus = response.data.is_solve_time;
		}
	});
	return rtnStatus;
}

// 新增修改一個學生發問評分
function newUserQuestionRating(t_rating) {
	var fData = new FormData();
	fData.append("token", Cookies.get('u_t'));
	fData.append("uq_id", uq_id);
	fData.append("dv_id", "4");
	fData.append("uqrs_rating", t_rating);
	$.ajax({
		url: g_obj.domain + '/api/userQuestion/rating',
		type: 'post',
		data: fData,
		dataType: 'json',
		async: false,
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					break;
				default:
					alert(response.msg);
					break;
			}
		}
	});	
}

// 取得使用者點數
function getUserPoint() {
	$.get( g_obj.domain + "/api/userpoint?token=" + Cookies.get('u_t'), function( response ) {
		if(response.data != null){
			var item = response.data;
			$("#user_points").html(item.ups_point);
		}
	});
}