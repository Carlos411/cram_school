var token;
var countPerPage = 10;
var userQuestionStudentData = null;
var star_value = 0;
$(function(){
  token = Cookies.get('u_t');
  getUserQuestionStudent(token);
  reset_status();
});

$("#answered").change(function() {
    if (userQuestionStudentData != null) {
      $("#item_list").html(setListView(userQuestionStudentData, this.checked, star_value));
    }
});
$("input[name=star_check]").change(function(){
  star_value = $(this).val();
  if (userQuestionStudentData != null) {
    $("#item_list").html(setListView(userQuestionStudentData, $("#answered").is(":checked"), star_value));
  }
});

/**
 * 取得問題列表
 */
function getUserQuestionStudent(token) {
  var url = g_obj.domain + '/api/userQuestion/student?token=' + token + '&is_mine=1';

  $.ajax({
    url: url,
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        userQuestionStudentData = response.data;
        var checked = $("#answered").is(":checked");
        $("#item_list").html(setListView(userQuestionStudentData, checked, star_value));
      } else {
        $("#item_list").html("");
        setPager(0);
      }
    }
  });
}

/**
 * 根據模式顯示列表資料
 */
function setListView(data, onlyShowIsSolved, star_value) {
  var html = "";
  var count = 0;

  if (onlyShowIsSolved) {  // 只顯示已解答資料
      $.each(data, function( index, item ) {
        if (item.is_solved) {
          if(Math.floor(item.uqrs_rating_avg) >= star_value){

            html += '<li class="' + (count < countPerPage? "" : "no_display") + '"><div class="item_block"><div class="item_right_block"><ul class="span_list">' + ((item.is_solved)?'<li><span class="hint_text"><img src="./images/icon/icon_answer.svg"></span></li>':'');
            switch(Math.floor(item.uqrs_rating_avg)){
              case 1:
                html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_1.svg"></span></li>';
                break;
              case 2:
                html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_2.svg"></span></li>';
                break;
              case 3:
                html += '<li><span class="hint_text"><img src="./images/icon/icon_star_3.svg"></span></li>';
                break;
              case 4:
                html += '<li><span class="hint_text"><img src="./images/icon/icon_star_4.svg"></span></li>';
                break;
              case 5:
                html += '<li><span class="hint_text"><img src="./images/icon/icon_star_5.svg"></span></li>';
                break;
              default:
                html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_1.svg"></span></li>';
            }
            html += '</ul><a href="./p_record_ask_single.html?uq_id=' + item.uq_id + '" class="para">' + item.uq_title + '</a></div></div></li>';

            count++;
          }
        }
      });
  } else {  // 顯示全部資料
      $.each(data, function( index, item ) {
        if(Math.floor(item.uqrs_rating_avg) >= star_value){

          html += '<li class="' + (count < countPerPage ? "" : "no_display") + '"><div class="item_block"><div class="item_right_block"><ul class="span_list">' + ((item.is_solved)?'<li><span class="hint_text"><img src="./images/icon/icon_answer.svg"></span></li>':'');
          switch(Math.floor(item.uqrs_rating_avg)){
            case 1:
              html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_1.svg"></span></li>';
              break;
            case 2:
              html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_2.svg"></span></li>';
              break;
            case 3:
              html += '<li><span class="hint_text"><img src="./images/icon/icon_star_3.svg"></span></li>';
              break;
            case 4:
              html += '<li><span class="hint_text"><img src="./images/icon/icon_star_4.svg"></span></li>';
              break;
            case 5:
              html += '<li><span class="hint_text"><img src="./images/icon/icon_star_5.svg"></span></li>';
              break;
            default:
              html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_1.svg"></span></li>';
          }
          html += '</ul><a href="./p_record_ask_single.html?uq_id=' + item.uq_id + '" class="para">' + item.uq_title + '</a></div></div></li>';

          count++;
        }
      });
  }

  setPager(count);

  return html;
}

/**
 * 設定分頁
 */
function setPager(count) {
  $("#current_page").html(str_pad(1, 2, 0));
  $("#pager_left_button").addClass("-disabled");
  $("#pager_tri_left_button").addClass("-disabled");

  var totalPage = Math.ceil(count / countPerPage);

  if (totalPage > 1) {
    $("#total_page").html(str_pad(totalPage, 2, 0));
    $("#pager_right_button").removeClass("-disabled");
    $("#pager_tri_right_button").removeClass("-disabled");
  } else {
    $("#total_page").html(str_pad(1, 2, 0));
    $("#pager_right_button").addClass("-disabled");
    $("#pager_tri_right_button").addClass("-disabled");
  }
  $("div.page_container").removeClass("no_display");
}

// 頁面狀態 reset
function reset_status(){
  if(!$("#check_all").prop("checked")){
    $("#check_all").click();
  }
  if($("#answered").prop("checked")){
    $("#answered").click();
  }
}
