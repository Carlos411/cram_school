var q_db_exam_olId;
var q_db_exam_unitOlId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterName;
var partName;

$(function(){
  // 取得頁面間傳遞參數
  q_db_exam_olId = $.url('?ol_id');
  q_db_exam_unitOlId = $.url('?unit_ol_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');
  versionName = $.url('?version_name');
  gradeGroupName = $.url('?grade_group_name');
  chapterName = $.url('?chapter_name');
  partName = $.url('?part_name');

  // 設定導覽列
  checkHeaderTextAfterUnit(q_db_exam_unitOlId);

  $("#header_unit").attr("href", "./q_db_practice_unit.html?ol_id=" + q_db_exam_unitOlId + "&field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + versionName + "&grade_group_name=" + gradeGroupName);
  $("#main_title").text(subjectName + " " + ((versionName)?versionName + " " : "") + gradeGroupName + " " + chapterName + " " + partName);

  // 設定導覽列連結
  setHeaderGradeGroupHref(q_db_exam_unitOlId);

  // 最近測驗結果
  getTestRecordRecent("recently_view_item_list", 2);
});

function getTestRecord(tr_id){
  var url = g_obj.domain + '/api/test/record?token=' + Cookies.get('u_t') + '&tr_id=' + tr_id;
  var question_data;
  $.ajax({
    url: url,
    type: 'get',
    // data: data,
    dataType: 'json',
    async: false,
    success: function(response){
      var tmp_result_q = response.data.questions;
      $.each(tmp_result_q, function( index, item ) {
         $.ajax({
           url: item.tq_html_link,
           async: false,
           success: function(rtnHtml){
             //tmpQuestionHtml = rtnHtml;
             tmp_result_q[index].tq_html = rtnHtml;
           }
         });
         // 取得說明
         $.ajax({
           url: item.tq_explain,
           async: false,
           success: function(rtnHtml){
             //tmpExplain = rtnHtml;
             tmp_result_q[index].tq_explain_html = rtnHtml;
           }
         });
         tmp_result_q[index].tq_video = item.tq_video_path;
      });
      question_data = response.data.questions;
    }
  });
  return question_data;
}
