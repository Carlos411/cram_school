var select_1 = ''; // 第一個下拉選單：領域
var select_2 = ''; // 第二個下拉選單：科目
var select_3 = ''; // 第三個下拉選單：年級群組
var select_2_ol_ids = []; // 第二個下拉選單：科目


var version_data = [];
var select3_data = []; // 第三個下拉選單準備放進去的資料

var callForSelect2 = 0;
var callForSelect3 = 0;

var countPerPage = 15;

$(function(){
  // 取得 url 參數
  if($.url('?select1') != undefined){
    select_1 = $.url('?select1');
  }
  if($.url('?select2') != undefined){
    select_2 = $.url('?select2');
  }
  if($.url('?select3') != undefined){
    select_3 = $.url('?select3');
  }

  getOutlineRoot(g_obj.ec_id, g_obj.db_practice_ot_id);
});

/**
 * 取得大綱根節點資料
 */
function getOutlineRoot(ecId, otId) {
  $.get( g_obj.domain + '/api/outline/root?ec_id=' + ecId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    $.each(response.data, function( index, item ) {
      if(index == 0){
        getOutlineNextNodeForSelect1(item.ol_id, otId);
      }
       //return false;
    });
  });
}

/**
 * 取得領域資料
 */
function getOutlineNextNodeForSelect1(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "";
      $.each(response.data, function( index, item ) {
         if(index == 0){ // 33 為「最新版　全科」
           html += '<option value="' + item.ol_id + '" selected>' + item.ol_display_name + '</option>';
           if(select_1 == ""){
             select_1 = item.ol_id;
           }
         }else{
           html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
         }
      });
      $("#select1").append(html);
      if(select_1 != ""){
        $("#select1").val(select_1);
      }

      if(select_1 != "" && callForSelect2 == 0){
        callForSelect2++;
        //alert(select_1);
        getOutlineNextNodeForSelect2(select_1, otId);
      }

    }
  });
}

/**
 * 取得科目資料
 */
function getOutlineNextNodeForSelect2(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "<option value='all' selected>全部</option>";
      if(select_2 == "" || select_2 == "all"){ // 若網址無參數，或有 select_2為"all"時，將所有的 ol_ids 存至 select_2_ol_ids 陣列
        select_2 = "all";
        //$.each(response.data, function( index, item ) {
          //select_2_ol_ids.push(item.ol_id)
        //});
      }else{
        //select_2_ol_ids.push(select_2)
      }

      $.each(response.data, function( index, item ) {
        html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
      });
      $("#select2").append(html);
      $("#select2").val(select_2);

      if(select_2 == "all"){
        $.each(response.data, function( index, item ) {
          select_2_ol_ids.push({
            ol_id: item.ol_id,
            ol_display_name: item.ol_display_name
          });
        });

        getTestRecordList(olId);

      }else{
        select_2_ol_ids.push({
          ol_id: select_2,
          ol_display_name: $("#select2 option:selected").text()
        });
        $("#select3").closest("div.option_inner").removeClass("no_display");
        getOutlineNextNodeForSelect3(select_2, otId);
      }

    }
  });
}


/**
 * 取得年級群組資料：第三個下拉選單
 */
function getOutlineNextNodeForSelect3(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      //var version_data = [];
      $.each(response.data, function( index, item ) {
        version_data.push({
          ol_id: item.ol_id,
          ol_display_name: item.ol_display_name
        });
      });
      getOutlineNextNodeForSelect3Again(otId);
    }
  });
}

function getOutlineNextNodeForSelect3Again(otId) {

  //console.log(version_data);
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + version_data[callForSelect3].ol_id + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( res ) {
    //console.log(res);
    if(res.data.length > 0){
      $.each(res.data, function( i, item ) {
        //console.log(item);
        //alert(callForSelect3);
        select3_data.push({
          ol_id: item.ol_id,
          ol_display_name: item.ol_display_name,
          version_name: (version_data[callForSelect3].ol_display_name == "不分版" ? "" : version_data[callForSelect3].ol_display_name )
        });
      });
      callForSelect3++;
      if(callForSelect3 < version_data.length){
        getOutlineNextNodeForSelect3Again(otId);
      }
      if(callForSelect3 == version_data.length){
        //console.log(select3_data);
        // 將 select3_data 裡的資料，放入第三個下拉選單
        var select3_html = "";
        $.each(select3_data, function( i, item ) {
          if( i == 0 ){
            if(select_3 == ""){
              select_3 = item.ol_id;
            }
          }
          select3_html += '<option value="' + item.ol_id + '">' + item.version_name + item.ol_display_name + '</option>';
        });
        $("#select3").html(select3_html);
        $("#select3").val(select_3);

        getTestRecordList(select_3);
      }
    }
  });

}

/**
 * 取得成績單列表
 */
function getTestRecordList(olId) {
  var url = g_obj.domain + '/api/test/record/list?token=' + Cookies.get('u_t') + '&ol_id=' + olId;

  $.ajax({
    url: url,
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      var html = "";
      var count = response.data.length;

      if(count > 0){
        $.each(response.data, function( index, item ) {
          if(item.parents.length < 7){
            console.log("tr_id: " + item.tr_id + " 資料有誤。");
            console.log(item);
          }
           if (index < countPerPage) {
              html += '<li>';
           } else {
              html += '<li class="no_display">';
           }

           html += '<a href="./my_learn_record_score.html?ol_id=' + item.parents[2].ol_id + '&tr_id=' + item.tr_id + '&subject_name=' + item.parents[4].ol_display_name + '" class="item_link">';
           html += '<div class="top_block"><p class="para_title">' + item.parents[4].ol_display_name + ' ' + item.parents[2].ol_display_name + '</p></div>';
           html += '<div class="middle_block"><p class="para1">' + item.parents[1].ol_display_name + '</p><p class="para2">' + item.parents[0].ol_display_name + '</p></div>';
           html += '<div class="bottom_block"><p class="para_right -right">' + item.tr_datetime.substr(11,5) + '</p><p class="para_left">' + item.tr_datetime.substr(0,10).replace(/-/g, '/') + '</p></div>';
           html += ((item.grade >= 60)?'<span class="score -more">':'<span class="score -less">') + item.grade + '</span></a></li>';
        });

        $("#item_list").html(html);

      }else{
        $("#item_list").html("");
      }

      setPager(count);
    }
  });
}

/**
 * 設定分頁
 */
function setPager(count) {
  $("#current_page").html(str_pad(1, 2, 0));
  $("#pager_left_button").addClass("-disabled");

  var totalPage = Math.ceil(count / countPerPage);

  if (totalPage > 1) {
    $("#total_page").html(str_pad(totalPage, 2, 0));
    $("#pager_right_button").removeClass("-disabled");
  } else {
    $("#total_page").html(str_pad(1, 2, 0));
    $("#pager_right_button").addClass("-disabled");
  }
}
