$(function(){
	
});

// 新增修改註記
function updateUserplanComent(upcs_id, upcs_content, upcs_datetime) {
	var rtnStatus = false;
	var fData = new FormData();
	fData.append("token", Cookies.get('u_t'));
	if (upcs_id != "") fData.append("upcs_id", upcs_id);
	fData.append("upcs_content", upcs_content);
	fData.append("upcs_datetime", upcs_datetime);
	$.ajax({
		url: g_obj.domain + '/api/userplan/comment',
		type: 'post',
		data: fData,
		dataType: 'json',
		async: false,
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// alert("更新完成");
					rtnStatus = true;
					break;
				default:
					alert(response.msg);
					/*
						9001: // 使用者學習計畫註記id無效
						9003: // 使用者學習計畫註記時間無效
						9004: // 使用者學習計畫註記內容過長
						9006: // 使用者學習計畫註記時間未填
					*/
					break;
			}
		}
	});
	return rtnStatus;
}

// 刪除使用者學習計畫
function deleteUserplan(upls_id) {
	var rtnStatus = false;
	$.ajax({
		url: g_obj.domain + '/api/userplan',
		type: 'DELETE',
		data: "upls_id=" + upls_id,
		headers: {
            "token": Cookies.get('u_t'),
            "Content-Type": "application/x-www-form-urlencoded"
        },
		dataType: 'json',
		async: false,
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					// window.location.reload();
					rtnStatus = true;
					break;
				default:
					// alert(response.msg);
					/*
						9000: // 使用者學習計畫id無效
					*/
					break;
			}
		}
	});
	return rtnStatus;
}

// 新增使用者學習計畫
function addUserplan(ol_id, ot_id, upls_datetime) {
	var rtnStatus = false;
	var fData = new FormData();
	fData.append("token", Cookies.get('u_t'));
	fData.append("ol_id", ol_id);
	fData.append("ot_id", ot_id);
	fData.append("upls_datetime", upls_datetime);
	$.ajax({
		url: g_obj.domain + '/api/userplan',
		type: 'post',
		data: fData,
		dataType: 'json',
		async: false,
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// alert(response.msg);
					// window.location.reload();
					rtnStatus = true;
					break;
				default:
					alert(response.msg);
					/*
						9001: // 使用者學習計畫註記id無效
						9003: // 使用者學習計畫註記時間無效
						9004: // 使用者學習計畫註記內容過長
						9006: // 使用者學習計畫註記時間未填
					*/
					break;
			}
		}
	});
	return rtnStatus;
}