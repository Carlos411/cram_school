var olId;
var uqId;
var token;

$(function(){
	// 取得頁面間傳遞參數
	uqId = $.url('?uq_id');
	token = Cookies.get('u_t');

	getUserQuestion(token, uqId);
});

// 筆記截圖
function getUsernoteListData(){
  $.ajax({
    url: g_obj.domain + '/api/usernote/list?ol_id=' + olId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    //headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      //alert(res.data.length);
      if(res.data.length > 0){
        var html = '';
        $.each(res.data, function(index, item){
          html += '<li><button type="button" class="btn_note_choose"><img src="' + item.un_path + '" data-un-id="' + item.un_id + '"></button></li>'
        });
        $("ul.note_list").html(html);
      }else{
        $("button.btn_upload_confirm").addClass("-disabled");
      }
    }
  });
}

/**
 * 取得學生發問串
 */
function getUserQuestion(token, uqId) {
	var url = g_obj.domain + '/api/userQuestion?token=' + token + '&uq_id=' + uqId;

	$.ajax({
		url: url,
		type: 'get',
		// data: data,
		dataType: 'json',
		statusCode: {
			200: function (response) {
				// console.log(response);
			}
		},
		success: function(response){
			var html = "";
			if (response.data.length >= 3) {
				$("#ask_again_block").hide();
			}

			$('#tmp_ot_id').val(response.data[0].ot_id);
			$('#tmp_ol_id').val(response.data[0].ol_id);

			$.each(response.data, function( index, item ) {
				if(index == 0){
					$("#title2").html('<span class="score_average">平均評分：' + item.uqrs_rating_avg + '</span>' + item.uq_title);
					$("#ask_modal").find("input.ask_title").val(item.uq_title);
				}

				var tmpQuestionHtml = '', tmpExplain = '';
				var tq_html_link = '', tq_explain_link = '';
				if (item.question.length > 0) {
					$.ajax({
						url: item.question[0].tq_html_link,
						async: false,
						success: function(rtnHtml){
							tmpQuestionHtml = rtnHtml;
						}
					});
					$.ajax({
						url: item.question[0].tq_explain_link,
						async: false,
						success: function(rtnHtml){
							tmpExplain = rtnHtml;
						}
					});
				}

				html += '<p class="general_para"><span class="date_time_text">' + item.uq_question_datetime + '</span>發問人：' + item.us_name + '</p>';
				html += '<div class="q_block"><p class="q_para">問題: ' + item.uq_question_content + '</p>';
				if (item.uq_question_img_1 != '' || item.uq_question_img_2 != '' || item.uq_question_img_3 != '') {
					html += '<div class="q_images_block"><ul class="q_images_list">';
					html += ((item.uq_question_img_1)?'<li><div class="item_block"><p class="img_title">問題圖片一</p><div class="img_parent"><img src="' + item.uq_question_img_1 + '" class="data_img_preview"></div></div></li>':'') + ((item.uq_question_img_2)?'<li><div class="item_block"><p class="img_title">問題圖片二</p><div class="img_parent"><img src="' + item.uq_question_img_2 + '" class="data_img_preview"></div></div></li>':'') + ((item.uq_question_img_3)?'<li><div class="item_block"><p class="img_title">問題圖片三</p><div class="img_parent"><img src="' + item.uq_question_img_3 + '" class="data_img_preview"></div></div></li>':'');
					html += '</ul></div>';
				}
				html += '</div>';
				if (item.question.length > 0) {
					html += '<div class="q_and_a_block">';
					html += '<div id="question' + item.tq_id + '" data-tq-html="' + tq_html_link + '" class="left_block">' + tmpQuestionHtml + '</div>';
					html += '<div id="explain' + item.tq_id + '" data-tq-explain="' + tq_explain_link + '" class="right_block">' + tmpExplain + '</div>';
					html += '</div>';
				}
				if (item.admin_id != null) {
					html += '<p class="general_para -for-answer"><span class="date_time_text">' + item.uq_answer_datetime + '</span>解題老師：' + item.admin_name + '</p>';
					html += '<div class="a_block"><p class="q_para">回答: ' + item.uq_answer_content + '</p>';
					if (item.uq_answer_img_1 != '' || item.uq_answer_img_2 !='' || item.uq_answer_img_3 != '') {
						html += '<div class="q_images_block"><ul class="q_images_list">';
						html += ((item.uq_answer_img_1)?'<li><div class="item_block"><p class="img_title">問題圖片一</p><div class="img_parent"><img src="' + item.uq_answer_img_1 + '" class="data_img_preview"></div></div></li>':'') + ((item.uq_answer_img_2)?'<li><div class="item_block"><p class="img_title">問題圖片二</p><div class="img_parent"><img src="' + item.uq_answer_img_2 + '" class="data_img_preview"></div></div></li>':'') + ((item.uq_answer_img_3)?'<li><div class="item_block"><p class="img_title">問題圖片三</p><div class="img_parent"><img src="' + item.uq_answer_img_3 + '" class="data_img_preview"></div></div></li>':'');
						html += '</ul></div>';
					}
					html += '</div><br>';
				} else {
					$("#ask_again_block").hide();
				}

				if(index == 0){ // 使用第一筆
					olId = item.ol_id;
					show_star(item.uqrs_rating);
					// 我要評分是否顯示：第一筆，且如果 admin_id 是 null，就隱藏
					if(item.admin_id == null){
						$("div.rate_block").hide();
					}
				}
			});

			$("#qa_area").html(html);
			// loadQuestionAndExplain(response.data);
			// 筆記截圖
		  getUsernoteListData();
		}
	});
}

// 顯示評分數
function show_star(show_star){
	$("ul.star_list").find("button.btn_star").each(function(){
		if(parseInt($(this).attr("data-star")) <= show_star){
			$(this).addClass("-on");
		}
	});
}

/**
 * 取得題目
 */
function getQuestion(tqId) {
  $.ajax({
    url: $("#question" + tqId).data("tq-html"),
    type: 'get',
    // data: data,
    // dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $("#question" + tqId).html(response);
    }
  });
}

/**
 * 取得詳解
 */
function getExplain(tqId) {
  $.ajax({
    url: $("#explain" + tqId).data("tq-explain"),
    type: 'get',
    // data: data,
    // dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $("#explain" + tqId).html(response);
    }
  });
}

/**
 * 載入題目和詳解
 */
function loadQuestionAndExplain(listData) {
  $.each(listData, function( index, item ) {
      if ($("#question" + item.tq_id).data("tq-html") != "") {
        getQuestion(item.tq_id);
      }

      if ($("#explain" + item.tq_id).data("tq-explain") != "") {
        getExplain(item.tq_id);
      }
  });
}
