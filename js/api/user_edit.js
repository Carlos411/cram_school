function getApiUserStudent(){
  $.ajax({
    url: g_obj.domain + '/api/user/student?token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    //headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      if(res.data.length > 0){
        $("#us_name").val(res.data[0].us_name); // 學生姓名

        if(res.data[0].us_img != ""){
          //$("#us_img_tag").attr("src", res.data[0].us_img); // 使用者圖片路徑
          $("#us_img_tag").addClass("no_display");
          $("#us_img_tag").closest("span.avatar_img").addClass("-add-border");
          $("#us_img_tag").closest("span.avatar_img").attr("style", "background-image: url('" + res.data[0].us_img + "');");
        }

        $("#us_is_male").val(res.data[0].us_is_male); // 性別

        // 使用者生日
        if(res.data[0].us_birthday != null){
          $("#birthY").val( parseInt(((((res.data[0].us_birthday).split(" "))[0]).split("-"))[0]) );
          $("#birthM").val( ((((res.data[0].us_birthday).split(" "))[0]).split("-"))[1] );
          $("#birthD").val( ((((res.data[0].us_birthday).split(" "))[0]).split("-"))[2] );

          $("input.temp_birtyday").val(parseInt(((((res.data[0].us_birthday).split(" "))[0]).split("-"))[0]) + "/" + ((((res.data[0].us_birthday).split(" "))[0]).split("-"))[1] + "/" + ((((res.data[0].us_birthday).split(" "))[0]).split("-"))[2]);
        }

        $("#us_cell_phone").val(res.data[0].us_cell_phone); // 使用者行動電話

        // 市話
        if( (res.data[0].us_phone).includes("-") ){
          $("#us_phone1").val( ((res.data[0].us_phone).split("-"))[0] ); // 使用者電話
          $("#us_phone2").val( ((res.data[0].us_phone).split("-"))[1] ); // 使用者電話
        }else{
          $("#us_phone2").val( res.data[0].us_phone ); // 使用者電話
        }


        $("#us_mail").val(res.data[0].us_mail); // 使用者電子郵件

        $("#us_school").val(res.data[0].us_school); // 使用者學校

        // 使用者年級
        // $("#us_grade").html("<option value='" + res.data[0].us_grade + "'>" + res.data[0].us_grade + "</option>")
		$("#us_grade").val(res.data[0].us_grade);

        //$("#us_grade").val(res.data[0].us_grade); // 使用者年級

        $("#us_class").val(res.data[0].us_class); // 使用者班級

        $("#up_name").val(res.data[0].up_name); // 使用者家長姓名
        $("#up_cell_phone").val(res.data[0].up_cell_phone); // 使用者家長行動電話
      }else{
        alert("無使用者資料");
      }
      setting_temp_birtyday();
    }
  });
}

function setting_temp_birtyday(){
  $('input.temp_birtyday').each(function(){
    $(this).dateRangePicker({
      //separator : ' 到 ',
      language: 'tc',
      format: 'YYYY/MM/DD',
      autoClose: true,
      singleDate : true,
      showShortcuts: false,
      singleMonth: true,
      yearSelect: function(current) {
        return [1900, (new Date()).getFullYear()];
      },
      inline:true,
      container: "div.invisible_birthday_block",
      alwaysOpen: true
    }).bind('datepicker-change',function(event,obj){
      var user_new_birthday = new Date(obj.date1);
      $('.-birthday.-year').val(user_new_birthday.getFullYear());
      $('.-birthday.-month').val(str_pad(user_new_birthday.getMonth() + 1, 2, 0));
      $('.-birthday.-date').val(str_pad(user_new_birthday.getDate(), 2, 0));
      $("div.invisible_birthday_block").hide();
    });
  });
}
