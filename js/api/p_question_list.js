var olId;
var unitOlId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterName;
var partName;
var token;
var countPerPage = 10;
var userQuestionStudentData = null;
var star_value = 0;
$(function(){
  reset_status();
  // 取得頁面間傳遞參數
  olId = $.url('?ol_id');
  unitOlId = $.url('?unit_ol_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');
  versionName = $.url('?version_name');
  gradeGroupName = $.url('?grade_group_name');
  chapterName = $.url('?chapter_name');
  partName = $.url('?part_name');
  token = Cookies.get('u_t');

  // 設定導覽列
  checkHeaderTextAfterUnit(unitOlId);
  //$("#header_field").text(fieldName);
  //$("#header_subject").text(subjectName);
  //$("#header_grade_group").text(versionName + gradeGroupName);
  $("#header_unit").attr("href", "./p_question_unit.html?ol_id=" + unitOlId + "&field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + versionName + "&grade_group_name=" + gradeGroupName);

  $("#title1").text(subjectName + " " + ((versionName)?versionName + " " : "") + gradeGroupName + " " + chapterName + " " + partName);

  // 設定導覽列連結
  setHeaderGradeGroupHref(unitOlId);

  getUserQuestionStudent(token, olId);

  left_aside_link(4); // 更換大類別連結
});

$("#answered").change(function() {
    if (userQuestionStudentData != null) {
      var isMine = $("#my_q").is(":checked");
      $("#item_list").html(setListView(userQuestionStudentData, this.checked, isMine, star_value));
    }
});

$("#my_q").change(function() {
    if (userQuestionStudentData != null) {
      var isSolved = $("#answered").is(":checked");
      $("#item_list").html(setListView(userQuestionStudentData, isSolved, this.checked, star_value));
    }
});

$("input[name=star_check]").change(function(){
  star_value = $(this).val();
  if (userQuestionStudentData != null) {
    $("#item_list").html(setListView(userQuestionStudentData, $("#answered").is(":checked"), $("#my_q").is(":checked"), star_value));
  }
});

/**
 * 取得問題列表
 */
function getUserQuestionStudent(token, olId) {
  var url = g_obj.domain + '/api/userQuestion/student?token=' + token + '&ol_id=' + olId;

  $.ajax({
    url: url,
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        userQuestionStudentData = response.data;
        var isSolved = $("#answered").is(":checked");
        var isMine = $("#my_q").is(":checked");
        $("#item_list").html(setListView(userQuestionStudentData, isSolved, isMine, star_value));
      } else {
        $("#item_list").find("span.-loading").text("此單元無問題");
        setPager(0);
      }
    }
  });
}

/**
 * 根據模式顯示列表資料
 */
function setListView(data, isSolved, isMine, star_value) {
  var html = "";
  var count = 0;
  if (isSolved && isMine) {
      $.each(data, function( index, item ) {
        if (item.is_solved && item.is_mine) {
          if(Math.floor(item.uqrs_rating_avg) >= star_value){
            html += '<li class="' + (count < countPerPage ? "" : "no_display") + '"><div class="item_block"><div class="item_right_block"><ul class="span_list"><li style="'+(item.is_mine?"":"visibility: hidden;")+'"><span class="hint_text"><img src="./images/icon/icon_my_q.svg"></span></li><li style="' + (item.is_solved?"":"visibility: hidden;") + '"><span class="hint_text"><img src="./images/icon/icon_answer.svg"></span></li>';
            switch(Math.floor(item.uqrs_rating_avg)){
              case 1:
                html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_1.svg"></span></li>';
                break;
              case 2:
                html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_2.svg"></span></li>';
                break;
              case 3:
                html += '<li><span class="hint_text"><img src="./images/icon/icon_star_3.svg"></span></li>';
                break;
              case 4:
                html += '<li><span class="hint_text"><img src="./images/icon/icon_star_4.svg"></span></li>';
                break;
              case 5:
                html += '<li><span class="hint_text"><img src="./images/icon/icon_star_5.svg"></span></li>';
                break;
              default:
                html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_1.svg"></span></li>';
            }
            html += '</ul>' + '<a href="./p_question_single.html?ol_id=' + olId + '&unit_ol_id=' + unitOlId + '&field_name=' + fieldName + '&subject_name=' + subjectName + '&version_name=' + versionName + '&grade_group_name=' + gradeGroupName + '&chapter_name=' + chapterName + '&part_name=' + partName + '&uq_id=' + item.uq_id + '&is_mine=' + item.is_mine + '" class="para">' + item.uq_title + '</a></div></div></li>';
            count++;
          }
        }
      });
  } else if (isSolved) {
      $.each(data, function( index, item ) {
        if (item.is_solved) {
          if(Math.floor(item.uqrs_rating_avg) >= star_value){
            html += '<li class="' + (count < countPerPage ? "" : "no_display") + '"><div class="item_block"><div class="item_right_block"><ul class="span_list"><li style="'+(item.is_mine?"":"visibility: hidden;")+'"><span class="hint_text"><img src="./images/icon/icon_my_q.svg"></span></li><li style="' + (item.is_solved?"":"visibility: hidden;") + '"><span class="hint_text"><img src="./images/icon/icon_answer.svg"></span></li>';
            switch(Math.floor(item.uqrs_rating_avg)){
              case 1:
                html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_1.svg"></span></li>';
                break;
              case 2:
                html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_2.svg"></span></li>';
                break;
              case 3:
                html += '<li><span class="hint_text"><img src="./images/icon/icon_star_3.svg"></span></li>';
                break;
              case 4:
                html += '<li><span class="hint_text"><img src="./images/icon/icon_star_4.svg"></span></li>';
                break;
              case 5:
                html += '<li><span class="hint_text"><img src="./images/icon/icon_star_5.svg"></span></li>';
                break;
              default:
                html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_1.svg"></span></li>';
            }
            html += '</ul>' + '<a href="./p_question_single.html?ol_id=' + olId + '&unit_ol_id=' + unitOlId + '&field_name=' + fieldName + '&subject_name=' + subjectName + '&version_name=' + versionName + '&grade_group_name=' + gradeGroupName + '&chapter_name=' + chapterName + '&part_name=' + partName + '&uq_id=' + item.uq_id + '&is_mine=' + item.is_mine + '" class="para">' + item.uq_title + '</a></div></div></li>';
            count++;
          }
        }
      });
  } else if (isMine) {
      $.each(data, function( index, item ) {
        if (item.is_mine) {
          if(Math.floor(item.uqrs_rating_avg) >= star_value){
            html += '<li class="' + (count < countPerPage ? "" : "no_display") + '"><div class="item_block"><div class="item_right_block"><ul class="span_list"><li style="'+(item.is_mine?"":"visibility: hidden;")+'"><span class="hint_text"><img src="./images/icon/icon_my_q.svg"></span></li><li style="' + (item.is_solved?"":"visibility: hidden;") + '"><span class="hint_text"><img src="./images/icon/icon_answer.svg"></span></li>';
            switch(Math.floor(item.uqrs_rating_avg)){
              case 1:
                html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_1.svg"></span></li>';
                break;
              case 2:
                html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_2.svg"></span></li>';
                break;
              case 3:
                html += '<li><span class="hint_text"><img src="./images/icon/icon_star_3.svg"></span></li>';
                break;
              case 4:
                html += '<li><span class="hint_text"><img src="./images/icon/icon_star_4.svg"></span></li>';
                break;
              case 5:
                html += '<li><span class="hint_text"><img src="./images/icon/icon_star_5.svg"></span></li>';
                break;
              default:
                html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_1.svg"></span></li>';
            }
            html += '</ul>' + '<a href="./p_question_single.html?ol_id=' + olId + '&unit_ol_id=' + unitOlId + '&field_name=' + fieldName + '&subject_name=' + subjectName + '&version_name=' + versionName + '&grade_group_name=' + gradeGroupName + '&chapter_name=' + chapterName + '&part_name=' + partName + '&uq_id=' + item.uq_id + '&is_mine=' + item.is_mine + '" class="para">' + item.uq_title + '</a></div></div></li>';
            count++;
          }
        }
      });
  } else {
      $.each(data, function( index, item ) {
        if(Math.floor(item.uqrs_rating_avg) >= star_value){
          html += '<li class="' + (count < countPerPage ? "" : "no_display") + '"><div class="item_block"><div class="item_right_block"><ul class="span_list"><li style="'+(item.is_mine?"":"visibility: hidden;")+'"><span class="hint_text"><img src="./images/icon/icon_my_q.svg"></span></li><li style="' + (item.is_solved?"":"visibility: hidden;") + '"><span class="hint_text"><img src="./images/icon/icon_answer.svg"></span></li>';
          switch(Math.floor(item.uqrs_rating_avg)){
            case 1:
              html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_1.svg"></span></li>';
              break;
            case 2:
              html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_2.svg"></span></li>';
              break;
            case 3:
              html += '<li><span class="hint_text"><img src="./images/icon/icon_star_3.svg"></span></li>';
              break;
            case 4:
              html += '<li><span class="hint_text"><img src="./images/icon/icon_star_4.svg"></span></li>';
              break;
            case 5:
              html += '<li><span class="hint_text"><img src="./images/icon/icon_star_5.svg"></span></li>';
              break;
            default:
              html += '<li style="visibility: hidden;"><span class="hint_text"><img src="./images/icon/icon_star_1.svg"></span></li>';
          }
          html += '</ul>' + '<a href="./p_question_single.html?ol_id=' + olId + '&unit_ol_id=' + unitOlId + '&field_name=' + fieldName + '&subject_name=' + subjectName + '&version_name=' + versionName + '&grade_group_name=' + gradeGroupName + '&chapter_name=' + chapterName + '&part_name=' + partName + '&uq_id=' + item.uq_id + '&is_mine=' + item.is_mine + '" class="para">' + item.uq_title + '</a></div></div></li>';
          count++;
        }
      });
  }

  setPager(count);

  return html;
}

/**
 * 設定分頁
 */
function setPager(count) {
  $("#current_page").html(str_pad(1, 2, 0));
  $("#pager_left_button").addClass("-disabled");
  $("#pager_tri_left_button").addClass("-disabled");

  var totalPage = Math.ceil(count / countPerPage);

  if (totalPage > 1) {
    $("#total_page").html(str_pad(totalPage, 2, 0));
    $("#pager_right_button").removeClass("-disabled");
    $("#pager_tri_right_button").removeClass("-disabled");
  } else {
    $("#total_page").html(str_pad(1, 2, 0));
    $("#pager_right_button").addClass("-disabled");
    $("#pager_tri_right_button").addClass("-disabled");
  }
}

// 頁面狀態 reset
function reset_status(){
  if(!$("#check_all").prop("checked")){
    $("#check_all").click();
  }
  if($("#answered").prop("checked")){
    $("#answered").click();
  }
  if($("#my_q").prop("checked")){
    $("#my_q").click();
  }
}
