var olId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterCount;
var totalCount = 0;
var totalContent = "";

$(function(){
  // 取得頁面間傳遞參數
  olId = $.url('?ol_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');
  versionName = $.url('?version_name');
  gradeGroupName = $.url('?grade_group_name');

  // 設定導覽列
  checkHeaderTextUnit(olId);
  //$("#header_field").text(fieldName);
  //$("#header_subject").text(subjectName);
  //$("#header_grade_group").text(versionName + gradeGroupName);
  $("#header_unit").attr("href", "#");

  getUsernoteOutlines(olId);

  // 設定導覽列連結
  setHeaderGradeGroupHref(olId);

  left_aside_link(3); // 更換大類別連結
});


// 取得使用者筆記大綱列表
function getUsernoteOutlines(olId){
  $.ajax({
    url: g_obj.domain + "/api/usernote/outlines?ol_id=" + olId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    //headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      if(res.data.length > 0){


        $.each(res.data, function( index, item ) {
           // console.log(item.ol_id);
           if (totalCount <= 14) {
              // -no-notes 表示沒有筆記，做淡
              totalContent += '<li class="' + (item.is_note == 0 ? "-no-notes" : "" ) + '"><a href="./my_learn_notes_list.html?ol_id=' + item.ol_id + '&unit_ol_id=' + olId + '&field_name=' + fieldName + '&subject_name=' + subjectName + '&version_name=' + versionName + '&grade_group_name=' + gradeGroupName + '&chapter_name=' + item.ol_display_name_1 + '&part_name=' + item.ol_display_name_2 + '" class="item_link">';
              totalContent +=   '<img src="' + item.ol_img + '" class="link_img">';
              totalContent +=   '<div class="desc_block">';
              totalContent +=   '<p class="para1">' + item.ol_display_name_1 + '</p>';
              totalContent +=   '<p class="para2">' + item.ol_display_name_2 + '</p>';
              totalContent += '</div>';
              totalContent += '</a></li>';
           } else {
             totalContent += '<li class="no_display ' + (item.is_note == 0 ? "-no-notes" : "" ) + '"><a href="./my_learn_notes_list.html?ol_id=' + item.ol_id + '&unit_ol_id=' + olId + '&field_name=' + fieldName + '&subject_name=' + subjectName + '&version_name=' + versionName + '&grade_group_name=' + gradeGroupName + '&chapter_name=' + item.ol_display_name_1 + '&part_name=' + item.ol_display_name_2 + '" class="item_link">';
             totalContent +=   '<img src="' + item.ol_img + '" class="link_img">';
             totalContent +=   '<div class="desc_block">';
             totalContent +=   '<p class="para1">' + item.ol_display_name_1 + '</p>';
             totalContent +=   '<p class="para2">' + item.ol_display_name_2 + '</p>';
             totalContent += '</div>';
             totalContent += '</a></li>';
           }

           totalCount++;
        });


      } else {
        $("#item_list").html("");
        $("#total_page").html(str_pad(1, 2, 0));
        $("#pager_right_button").addClass("-disabled");
      }
      setPager();
    }
  });

}

/**
 * 設定分頁
 */
function setPager() {
  $("#item_list").html(totalContent);
  var totalPage = Math.ceil(totalCount / 15);
  $("#total_page").html(str_pad(totalPage, 2, 0));
  if (totalPage == 1) {
    $("#pager_right_button").addClass("-disabled");
  }
  if (totalPage > 1) {
    $("#pager_right_button").removeClass("-disabled");
  }
}
