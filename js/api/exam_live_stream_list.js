﻿var olId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterCount;
var totalCount = 0;
var totalContent = "";

var select_1 = "";
var select_2 = "";

$(function(){

  //  移除選擇的項目
  $("#select_item_A").empty();
  $("#select_item_B").empty();

  // 取得頁面間傳遞參數
  if($.url('?select_1') != undefined){
    select_1 = $.url('?select_1');
  }
  if($.url('?select_2') != undefined){
    select_2 = $.url('?select_2');
  }

  //var olId = 20; //直接抓取 ol_id=20 (107 段考直播-年度) 之值套入 /outline/nextNode 使用
  //getOutlineRoot(olId , g_obj.exam_live_stream_ot_id);
  getOutlineRootInit(g_obj.ec_id , g_obj.exam_live_stream_ot_id);


   // 最近觀看
   get_video_recent("video_recent_item_list", g_obj.exam_live_stream_ot_id);


});

function setFieldName(data){
	fieldName = "" ;
	fieldName = data;
}

/*
function setLocalStorage(data1, data2){
	//因為SELECT B 的值，都沒辦法帶到下一頁，所以只能這樣的寫法
	window.localStorage["Select_A_Value"] = "";
	window.localStorage["Select_B_Value"] = "";

	if( data1==""){
		window.localStorage["Select_A_Value"] = $('#select_item_A').find("option:selected").val();
	}else{
		window.localStorage["Select_A_Value"] = data1;
	}

	if( data2==""){
		window.localStorage["Select_B_Value"] = $('#select_item_B').find("option:selected").val();
	}else{
		window.localStorage["Select_B_Value"] = data2;
	}

}
*/

function getOutlineRootInit(ecId, otId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/root?ec_id=' + ecId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        console.log(response);
      }
    },
    success: function(response){
      $.each(response.data, function( index, item ) {
         //console.log(item.ol_id);
         if(index == 0){
           //getOutlineNextNode(item.ol_id, otId);
           getOutlineRoot(item.ol_id, otId);
         }
         //return false;
      });
    }
  });
}

/**
 * 取得大綱根節點資料
 */
 //產生科目的下拉選項
function getOutlineRoot(olId, otId) {

  //var runOnce=true;

  $.ajax({
    url: g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $.each(response.data, function( index, item ) {
         // console.log(item.ol_id);
         //create option item by Myron
         //alert("item.ol_id=["+ item.ol_id +"] , item.ol_display_name=["+ item.ol_display_name +"] ");
         $('#select_item_A').append( $('<option></option>').val(item.ol_id).text(item.ol_display_name) );
         /*if ( runOnce==true ){
         	setFieldName(item.ol_display_name); //設定初使值
         	getOptionNode( item.ol_id , g_obj.exam_live_stream_ot_id);
         	setLocalStorage(item.ol_id, "" );
         	runOnce=false; //只跑一次，就可以，啟始值
        }*/
         //return false;
      });
      if(select_1 == ""){
        select_1 = $('#select_item_A').val();
      }else{
        $('#select_item_A').val(select_1);
      }
      setFieldName($("#select_item_A option:selected").text());
      getOptionNode( select_1 , g_obj.exam_live_stream_ot_id);
    }
  });

}


//add by Myron 2018.05.19
function SubmitForm(){
  select_1 = $('#select_item_A').val();
  select_2 = "";

	//var ol_id_value = 0;

	//ol_id_value = $('#select_item_A').val();
	setFieldName( $('#select_item_A').find("option:selected").text() );

	//  移除選擇的項目
	$("#select_item_B").empty();
	getOptionNode(select_1, g_obj.exam_live_stream_ot_id);

	//setLocalStorage("","");
}



/**
 * 取得資料
 */
 //產生年級的下拉選項

//var runGetOptionNodeOnce=true;
function getOptionNode(olId, otId) {

  //$('#select_item_B').append( $('<option></option>').val("").text('年級') );


  $.ajax({
    url: g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        var html = "";
        $.each(response.data, function( index, item ) {
        	//alert("item_B item.ol_id=["+ item.ol_id +"] , item.ol_display_name=["+ item.ol_display_name +"] ");
           $('#select_item_B').append( $('<option></option>').val(item.ol_id).text(item.ol_display_name) );

           	//if (runGetOptionNodeOnce==true){
           		// getOutlineNextNode(
           		// 	item.ol_id,
           		// 	g_obj.exam_live_stream_ot_id ,
           		// 	$('#select_item_B').find("option:selected").text(),
           		// 	$('#select_item_B').find("option:selected").val()
           		// );
           		//setLocalStorage("", item.ol_id );
           		//runGetOptionNodeOnce=false;
        	//}

        });
        if(select_2 == ""){
          select_2 = $('#select_item_B').val();
        }else{
          $('#select_item_B').val(select_2);
        }



        getOutlineNextNode(
          select_2,
          g_obj.exam_live_stream_ot_id ,
          $('#select_item_B').find("option:selected").text(),
          $('#select_item_B').find("option:selected").val()
        );
      }
    }
  });
}



//add by Myron 2018.05.19
function SubmitFormB(){
	//var ol_id_value = 0;
	//ol_id_value = $('#select_item_B').val();
  select_2 = $('#select_item_B').val();

	getOutlineNextNode(
		select_2,
		g_obj.exam_live_stream_ot_id ,
		$('#select_item_B').find("option:selected").text(),
		$('#select_item_B').find("option:selected").val(),
		$('#select_item_A').find("option:selected").text(),
		$('#select_item_A').find("option:selected").val()
	);
	//setLocalStorage("","");
}


/**
 * 取得資料
 */
function getOutlineNextNode(olId, otId ,select_item_B_Text, select_item_B_Val, select_item_A_Text, select_item_A_Val ) {

  var unitOlId = select_item_B_Val;
  var chapterName = select_item_B_Text;
  var chapterName2 = ""; //20180530 改成不顯示

  var local_fieldName = fieldName ;

  // console.log(olId + ' ' + otId);

  $.ajax({
    url: g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        console.log(response);
      }
    },
    success: function(response){
      //console.log(response);
      if(response.data.length > 0){
        var html = "";

        $.each(response.data, function( index, item ) {
           //html += '<li><a href="#" class="item_link"><img src="'+ item.ol_img +'" class="link_img"><div class="desc_block"><p class="para1">'+ select_item_A_Text +'</p><p class="para2">' + item.ol_display_name + '</p></div></a></li>';
           html += '<li class="' + (index<15?"":"no_display") + '"><a href="./exam_live_stream_video.html?ol_id=' + item.ol_id + '&unit_ol_id=' + unitOlId + '&field_name='+ local_fieldName + '&chapter_name=' + chapterName + '&part_name=' + item.ol_display_name + '&select_1=' + select_1 + '&select_2=' + select_2 + '" class="item_link"><img src="' + item.ol_img + '" class="link_img"><div class="desc_block"><p class="para1">' + chapterName2 + '</p><p class="para2">' + item.ol_display_name + '</p></div></a></li>';
        });

        $("#item_list").html(html);
        $("#page_container").removeClass("no_display");
      }else{
        $("#item_list").html("");
        $("#page_container").addClass("no_display");
      }
      setPager();
	  //setLocalStorage("", select_item_B_Val );
    }
  });

}




/**
 * 設定分頁
 */
function setPager() {
  totalCount = $("#item_list").children("li").length
  var totalPage = Math.ceil(totalCount / 15);
  $("#total_page").html(str_pad(totalPage, 2, 0));
  if (totalPage == 1) {
    $("#pager_right_button").addClass("-disabled");
  }
  if (totalPage > 1) {
    $("#pager_right_button").removeClass("-disabled");
  }
}
