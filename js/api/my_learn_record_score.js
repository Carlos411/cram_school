var olId;
var trId;
var subjectName;
var token;
var countPerPage = 10;
var listData = null;
var questionArr = [];
var explainArr = [];
var collectionArr = [];
var objIndex;
var print_filename = '';

$(function(){
  // 取得頁面間傳遞參數
  trId = $.url('?tr_id');
  olId = $.url('?ol_id');
  subjectName = $.url('?subject_name');
  token = Cookies.get('u_t');

  getTestRecord(token, trId);
});

$(".option_radio").change(function() {
  pause_video_func();
    $("button.btn_open_all_answer").removeClass("-on");

    var value = $('input[name=option_check]:checked').val();

    if (listData != null) {
      $("#item_list").html(setListView(listData, value));
      preloadQuestionAndExplain(listData);
    }
});

/**
 * 取得單一成績單
 */
function getTestRecord(token, trId) {
  var url = g_obj.domain + '/api/test/record?token=' + token + '&tr_id=' + trId;

  $.ajax({
    url: url,
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      var item = response.data;

      if (item != null) {
        if (item.outlineDetails != null && item.outlineDetails.length > 0) $('#tmp_ol_id').val(item.outlineDetails[0].ol_id);

        // 檔名
        var print_filename_arr = item.outlines.split("/");
        print_filename = print_filename_arr[1] + "_" + print_filename_arr[2] + (print_filename_arr[3] == "不分版" ? "" : "_" + print_filename_arr[3]) + "_" + print_filename_arr[4] + "_" + print_filename_arr[5];
        $("button.btn_print").removeClass("-disabled");

        // 將節的 ol_id 存入 localStorage
        window.localStorage["String_Check_Value"] = ( (item.chapter).join(",") ) + ',' ;

        listData = item.questions;
        if(listData.length > 0){
          $.each(listData, function( index, item ) {
             if (item.is_collection > 0) {
                collectionArr.push({
                      tqId: item.tq_id,
                      isCollection: true
                });
             } else {
                collectionArr.push({
                      tqId: item.tq_id,
                      isCollection: false
                });
             }
          });

          $("#us_name").html(item.us_name);
          $("#subject_name").html(subjectName);
          var ranges = item.ol_display_name.split(",");
          var rangeList = '';
          $.each(ranges, function( index, item ) {
             rangeList += '<li>' + item.replace("/", " ") + '</li>';
          });
          $("#range_list").html(rangeList);
          $("#tr_date").html(item.tr_datetime.substr(0,10).replace(/-/g, '/'));
          $("#tr_time").html(item.tr_datetime.substr(11,5));
          $("#tqr_used_second_total").html(secondsTimeSpanToMS(item.tqr_used_second_total));
          $("#tqr_used_second_all").html(secondsTimeSpanToMS(item.tqr_used_second_all));
          $("#grade_total").html(item.grade_total);
          $("#grade_all").html(item.grade_all);

          var $radios = $('input:radio[name=option_check]');
          $radios.filter('[value=all]').prop('checked', true);
          $("#item_list").html(setListView(listData, "all"));
          preloadQuestionAndExplain(listData);

          // 學習適性診斷
          learn_detect(item.outlineDetails);

          // 再挑戰
          q_db_test_again(item.questions, item.outlines);
        }else{
          alert("無題目資料");
        }
      } else {
        setPager(0);
      }
    }
  });
}

function q_db_test_again(questions, outlines){
  var tq_ids = "";
  $.each(questions, function( index, item ) {
    if(index == 0){
      tq_ids += item.tq_id;
    }else{
      tq_ids += ("," + item.tq_id);
    }
  });
  var outlines_arr = outlines.split("/"); // outlines:"106/全科/英文/遠東/陳版本選文/第一冊/第一課/課文說明"
  var q_db_test_again_link = "./q_db_test.html?ol_id=" + olId + "&field_name=" + outlines_arr[1] + "&subject_name=" + outlines_arr[2] + "&version_name=" + (outlines_arr[3] == "不分版" ? "" : outlines_arr[3]) + "&grade_group_name=" + outlines_arr[4] + "&difficult_easy_qty=9999&difficult_medium_qty=9999&difficult_hard_qty=9999&tq_ids=" + tq_ids;
  $("#link_to_q_db_test").attr("href", q_db_test_again_link);
}

/**
 * 根據模式顯示列表資料
 */
function setListView(data, mode) {
  var html = "";
  var count = 0;

  if (mode == 'all') {
      $.each(data, function( index, item ) {
         if (count < countPerPage) {
            html += '<li id="score_question_' + index + '" data-ol-id="' + item.ol_id + '">';
         } else {
            html += '<li id="score_question_' + index + '" data-ol-id="' + item.ol_id + '" class="no_display">';
         }

         objIndex = collectionArr.findIndex((obj => obj.tqId == item.tq_id));

		 html += '<div class="-row">';
		 html += '<div class="-col">';
         html += ((collectionArr[objIndex].isCollection) ?
			'<span class="img_span collect_action" data-status="1" data-ol_id="' + item.ol_id + '" data-tq_id="' + item.tq_id + '" data-uc-id="' + item.is_collection + '"><img src="./images/icon/icon-collect.svg" class="-no-collect -hide"><img src="./images/icon/icon-collect-check.svg" class="-collect">'
			:
			'<span class="img_span collect_action" data-status="0" data-ol_id="' + item.ol_id + '" data-tq_id="' + item.tq_id + '"><img src="./images/icon/icon-collect.svg" class="-no-collect"><img src="./images/icon/icon-collect-check.svg" class="-collect -hide">');
		 html += '</span>';
		 html += '</div>';
         html += '<div class="-col">' + item.number + '</div>';
         html += ((item.user_is_correct == 1)?'<div class="-col"><span class="img_span"><img src="./images/icon/icon-answer-o.svg" class="-answer"></span></div>':'<div class="-col"><span class="img_span"><img src="./images/icon/icon-answer-x.svg" class="-answer"></span></div>');
         html += '<div class="-col">' + item.all_correct + '%</div>';
         html += '<div class="-col">' + item.tqr_used_second + '秒</div>';
         html += '<div class="-col">' + item.all_used_second + '秒</div>';
         html += '<div class="-col">' + item.tqt_name + '</div>';
         html += '<div class="-col"><p class="para_concept">' + item.tq_navi + '</p></div>';
         html += '<div class="-col"><button type="button" class="btn_open"><span class="-text-on">ON</span><span class="-text-off">OFF</span></button></div></div>';
         //html += '<div class="drawer_block"><div class="q_and_a_block"><div id="question' + item.tq_id + '" data-tq-html="' + item.tq_html_link + '" class="left_block"><a href="javascript:getQuestion(' + item.tq_id + ')">題目</a></div>';
         html += '<div class="drawer_block"><div class="q_and_a_block"><div id="question' + item.tq_id + '" class="left_block">' + item.tq_content_static + '</div>';

         var correctAnswer = '';
         var myAnswer = '';

         $.each(item.answers, function( index2, item2 ) {
           if (item2.ta_correct == 1) {
              correctAnswer += (item2.option).toString();
           }

           if (item2.ta_choose == 1) {
              myAnswer += (item2.option).toString();
           }
         });

         html += '<div class="right_block"><div class="answer_block">';

         html += '<div class="answer_container">';

         html += '<div class="answer_left_block"><p class="para_correct_answer">正確答案：' + correctAnswer + '</p></div>';
         html += '<div class="answer_right_block"><p class="para_my_answer">我的答案：' + myAnswer + '</p></div>';

         html += '<div class="video_container no_display"><video class="answer_video" width="410" controls controlsList="nodownload" preload="auto" disablePictureInPicture="true"><source src="' + item.tq_video_path + '" type="video/mp4"></video></div>';

         html += '</div>';

         //html += '</div><div id="explain' + item.tq_id + '" data-tq-explain="' + item.tq_explain + '"><a href="javascript:getExplain(' + item.tq_id + ')">詳解</a></div></div></div>';
         html += '</div><div id="explain' + item.tq_id + '">' + item.tq_explain_static + '</div></div></div>';
         html += '<div class="middle_block"><p class="para">各選項回答比率：</p><ul class="answer_ratio_list">';
         $.each(item.answers, function( index3, item3 ) {
           html += '<li><div class="ratio_item"><span class="-a"><span class="-a-text">' + item3.option + '</span></span><span class="-ratio">' + item3.ta_choose_percent + '%</span></div></li>';
         });
         html += '</ul><button type="button" class="btn_img_answer ' + (item.tq_video_path == "" ?"-off":"") + '">影像詳解</button></div>';
         html += '<div class="bottom_block"><p class="para">' + item.ts_number + '</p><ul class="action_block">';
         html += '<li><button type="button" onclick="setQuestionNo(' + index + ');" class="btn_action btn_wrong" data-target-modal="wrong_modal">送出勘誤</button></li>';
         html += '<li><button type="button" onclick="setQuestionNo(' + index + ');" class="btn_action btn_confuse" data-target-modal="confuse_modal">送出解惑</button></li>';
         html += '<li><button type="button" onclick="setQuestionNo(' + index + ');" class="btn_action btn_note">儲存筆記</button></li>';
         html += '</ul></div></div></li>';

         count++;
      });
  } else if (mode == 'wrong_item') {
      $.each(data, function( index, item ) {
        if (item.user_is_correct == 0) {
           if (count < countPerPage) {
              html += '<li id="score_question_' + index + '" data-ol-id="' + item.ol_id + '">';
           } else {
              html += '<li id="score_question_' + index + '" data-ol-id="' + item.ol_id + '" class="no_display">';
           }

           objIndex = collectionArr.findIndex((obj => obj.tqId == item.tq_id));
           html += '<div class="-row">';
           html += '<div class="-col">';
           html += ((collectionArr[objIndex].isCollection) ?
               '<span class="img_span collect_action" data-status="1" data-ol_id="' + item.ol_id + '" data-tq_id="' + item.tq_id + '" data-uc-id="' + item.is_collection + '"><img src="./images/icon/icon-collect.svg" class="-no-collect -hide"><img src="./images/icon/icon-collect-check.svg" class="-collect">'
               :
               '<span class="img_span collect_action" data-status="0" data-ol_id="' + item.ol_id + '" data-tq_id="' + item.tq_id + '"><img src="./images/icon/icon-collect.svg" class="-no-collect"><img src="./images/icon/icon-collect-check.svg" class="-collect -hide">');
           html += '</span>';
           html += '</div>';
           html += '<div class="-col">' + item.number + '</div>';
           html += ((item.user_is_correct == 1)?'<div class="-col"><span class="img_span"><img src="./images/icon/icon-answer-o.svg" class="-answer"></span></div>':'<div class="-col"><span class="img_span"><img src="./images/icon/icon-answer-x.svg" class="-answer"></span></div>');
           html += '<div class="-col">' + item.all_correct + '%</div>';
           html += '<div class="-col">' + item.tqr_used_second + '秒</div>';
           html += '<div class="-col">' + item.all_used_second + '秒</div>';
           html += '<div class="-col">' + item.tqt_name + '</div>';
           html += '<div class="-col"><p class="para_concept">' + item.tq_navi + '</p></div>';
           html += '<div class="-col"><button type="button" class="btn_open"><span class="-text-on">ON</span><span class="-text-off">OFF</span></button></div></div>';
           //html += '<div class="drawer_block"><div class="q_and_a_block"><div id="question' + item.tq_id + '" data-tq-html="' + item.tq_html_link + '" class="left_block"><a href="javascript:getQuestion(' + item.tq_id + ')">題目</a></div>';
           html += '<div class="drawer_block"><div class="q_and_a_block"><div id="question' + item.tq_id + '" class="left_block">' + item.tq_content_static + '</div>';

           var correctAnswer = '';
           var myAnswer = '';

           $.each(item.answers, function( index2, item2 ) {
             if (item2.ta_correct == 1) {
                correctAnswer += (item2.option).toString();
             }

             if (item2.ta_choose == 1) {
                myAnswer += (item2.option).toString();
             }
           });

           html += '<div class="right_block"><div class="answer_block">';

           html += '<div class="answer_container">';

           html += '<div class="answer_left_block"><p class="para_correct_answer">正確答案：' + correctAnswer + '</p></div>';
           html += '<div class="answer_right_block"><p class="para_my_answer">我的答案：' + myAnswer + '</p></div>';

           html += '<div class="video_container no_display"><video class="answer_video" width="410" controls controlsList="nodownload" preload="auto" disablePictureInPicture="true"><source src="' + item.tq_video_path + '" type="video/mp4"></video></div>';

           html += '</div>';

           //html += '</div><div id="explain' + item.tq_id + '" data-tq-explain="' + item.tq_explain + '"><a href="javascript:getExplain(' + item.tq_id + ')">詳解</a></div></div></div>';
           html += '</div><div id="explain' + item.tq_id + '">' + item.tq_explain_static + '</div></div></div>';
           html += '<div class="middle_block"><p class="para">各選項回答比率：</p><ul class="answer_ratio_list">';
           $.each(item.answers, function( index3, item3 ) {
             html += '<li><div class="ratio_item"><span class="-a"><span class="-a-text">' + item3.option + '</span></span><span class="-ratio">' + item3.ta_choose_percent + '%</span></div></li>';
           });
           html += '</ul><button type="button" class="btn_img_answer ' + (item.tq_video_path == "" ?"-off":"") + '">影像詳解</button></div>';
           html += '<div class="bottom_block"><p class="para">' + item.ts_number + '</p><ul class="action_block">';
           html += '<li><button type="button" onclick="setQuestionNo(' + index + ');" class="btn_action btn_wrong" data-target-modal="wrong_modal">送出勘誤</button></li>';
           html += '<li><button type="button" onclick="setQuestionNo(' + index + ');" class="btn_action btn_confuse" data-target-modal="confuse_modal">送出解惑</button></li>';
           html += '<li><button type="button" onclick="setQuestionNo(' + index + ');" class="btn_action btn_note">儲存筆記</button></li>';
           html += '</ul></div></div></li>';

           count++;
        }
      });
  }

  setPager(count);

  return html;
}

/**
 * 取得題目
 */
/*
function getQuestion(tqId) {
  $.ajax({
    url: $("#question" + tqId).data("tq-html"),
    type: 'get',
    // data: data,
    // dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $("#question" + tqId).html(response);
      questionArr.push({
            tqId: tqId,
            html: response
      });
    }
  });
}
*/

/**
 * 取得詳解
 */
/*
function getExplain(tqId) {
  $.ajax({
    url: $("#explain" + tqId).data("tq-explain"),
    type: 'get',
    // data: data,
    // dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $("#explain" + tqId).html(response);
      explainArr.push({
            tqId: tqId,
            html: response
      });
    }
  });
}
*/

/**
 * 預載題目和詳解
 */
function preloadQuestionAndExplain(listData) {
  /*
  if (questionArr.length > 0) {

    $.each(questionArr, function( index, item ) {
         $("#question" + item.tqId).html(item.html);
    });

    $.each(explainArr, function( index, item ) {
         $("#explain" + item.tqId).html(item.html);
    });
  } else {
    $.each(listData, function( index, item ) {
         getQuestion(item.tq_id);
         getExplain(item.tq_id);
    });
  }
  */
}

/**
 * 設定分頁
 */
function setPager(count) {
  $("#current_page").html(str_pad(1, 2, 0));
  $("#pager_left_button").addClass("-disabled");

  var totalPage = Math.ceil(count / countPerPage);

  if (totalPage > 1) {
    $("#total_page").html(str_pad(totalPage, 2, 0));
    $("#pager_right_button").removeClass("-disabled");
  } else {
    $("#total_page").html(str_pad(1, 2, 0));
    $("#pager_right_button").addClass("-disabled");
  }
}

/**
 * 時間格式轉換
 */
function secondsTimeSpanToMS(s) {
    var m = Math.floor(s/60); //Get remaining minutes
    s -= m*60;
    return (m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding
}

// 學習適性診斷
function learn_detect(outline_details){
  if(outline_details.length > 0){
    $("button.btn_check").removeClass("-disabled");
    var two_layer = false; // 推播建議及補救建議是否都要出現
    $.each(outline_details, function( index, item ) {
      if(!isEmpty(item.od_id_remedy)){
        two_layer = true;
      }
    });

    if(!two_layer){ // 只會出現推播建議
      $("div.broadcast_help_recommend_block").addClass("-only1");
      $("div.broadcast_help_recommend_block span.-horizontal").hide();
    }

    var html = '';
    $.each(outline_details, function( index, item ) {
      html += '<li class="' + (!isEmpty(item.od_id_remedy) ? "-on" : "") + ' ' + (index > 3 ? "no_display":"") + '">'; // -on 表示產生透明背景
      html +=   '<a href="./f_teacher_video.html?ol_id=' + item.ol_id + '&unit_ol_id=' + item.previous_nodes[2].ol_id + '&field_name=' + item.previous_nodes[5].ol_display_name + '&subject_name=' + item.previous_nodes[4].ol_display_name + '&version_name=' + ((item.previous_nodes[3].ol_display_name == "不分版"?"":item.previous_nodes[3].ol_display_name)) + '&grade_group_name=' + item.previous_nodes[2].ol_display_name + '&chapter_name=' + item.previous_nodes[1].ol_display_name + '&part_name=' + item.previous_nodes[0].ol_display_name + '&od_id=' + item.od_id + '" class="item_link">';
      html +=     '<img src="' + item.ol_img + '" class="link_img">';
      html +=     '<div class="desc_block">';
      html +=       '<p class="para1">' + item.ol_display_name + '</p>';
      html +=       '<p class="para2">' + item.od_name + '</p>';
      html +=     '</div>';
      html +=   '</a>';
      if(!isEmpty(item.od_id_remedy)){
        html +=   '<a href="./f_teacher_video.html?ol_id=' + item.od_id_remedy.ol_id + '&unit_ol_id=' + item.od_id_remedy.previous_nodes[2].ol_id + '&field_name=' + item.od_id_remedy.previous_nodes[5].ol_display_name + '&subject_name=' + item.od_id_remedy.previous_nodes[4].ol_display_name + '&version_name=' + ((item.od_id_remedy.previous_nodes[3].ol_display_name == "不分版"?"":item.od_id_remedy.previous_nodes[3].ol_display_name)) + '&grade_group_name=' + item.od_id_remedy.previous_nodes[2].ol_display_name + '&chapter_name=' + item.od_id_remedy.previous_nodes[1].ol_display_name + '&part_name=' + item.od_id_remedy.previous_nodes[0].ol_display_name + '&od_id=' + item.od_id_remedy.od_id + '" class="item_link">';
        html +=     '<img src="' + item.od_id_remedy.ol_img + '" class="link_img">';
        html +=     '<div class="desc_block">';
        html +=       '<p class="para1">' + item.od_id_remedy.ol_display_name + '</p>';
        html +=       '<p class="para2">' + item.od_id_remedy.od_name + '</p>';
        html +=     '</div>';
        html +=   '</a>';
      }
      html += '</li>';
    });
    $("#broadcast_data").html(html);
    setPagerForRecommend(outline_details.length);
  }
}

/**
 * 設定分頁
 */
function setPagerForRecommend(count) {
  $("#current_page2").html(str_pad(1, 2, 0));
  $("#pager_left_button2").addClass("-disabled");

  var totalPage = Math.ceil(count / 4);

  if (totalPage > 1) {
    $("#total_page2").html(str_pad(totalPage, 2, 0));
    $("#pager_right_button2").removeClass("-disabled");
  } else {
    $("#total_page2").html(str_pad(1, 2, 0));
    $("#pager_right_button2").addClass("-disabled");
  }
}

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // If it isn't an object at this point
    // it is empty, but it can't be anything *but* empty
    // Is it empty?  Depends on your application.
    if (typeof obj !== "object") return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}
