var select_1 = ''; // 第一個下拉選單：領域
var select_2 = ''; // 第二個下拉選單：科目
var select_3 = ''; // 第三個下拉選單：年級群組
//var select_2_ol_ids = []; // 第二個下拉選單：科目

var version_data = [];
var select3_data = []; // 第三個下拉選單準備放進去的資料

var callForSelect2 = 0;
var callForSelect3 = 0;

var small_bottle_arr = []; // 小瓶子相關資料

var subject_text = ''; // 科目

$(function(){
  // 取得 url 參數
  if($.url('?select1') != undefined){
    select_1 = $.url('?select1');
  }
  if($.url('?select2') != undefined){
    select_2 = $.url('?select2');
  }
  if($.url('?select3') != undefined){
    select_3 = $.url('?select3');
  }

  getOutlineRoot(g_obj.ec_id, g_obj.db_practice_ot_id); // 學習記錄的 ot_id 與名師教學相同
});

/**
 * 取得大綱根節點資料
 */
function getOutlineRoot(ecId, otId) {
  $.get( g_obj.domain + '/api/outline/root?ec_id=' + ecId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    $.each(response.data, function( index, item ) {
      if(index == 0){
        getOutlineNextNodeForSelect1(item.ol_id, otId);
      }
       //return false;
    });
  });
}

/**
 * 取得領域資料
 */
function getOutlineNextNodeForSelect1(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "";
      $.each(response.data, function( index, item ) {
         if(index == 0){ // 33 為「最新版　全科」
           html += '<option value="' + item.ol_id + '" selected>' + item.ol_display_name + '</option>';
           if(select_1 == ""){
             select_1 = item.ol_id;
           }
         }else{
           html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
         }
      });
      $("#select1").append(html);
      if(select_1 != ""){
        $("#select1").val(select_1);
      }

      if(select_1 != "" && callForSelect2 == 0){
        callForSelect2++;
        getOutlineNextNodeForSelect2(select_1, otId);


      }

    }
  });
}

/**
 * 取得科目資料
 */
function getOutlineNextNodeForSelect2(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      var html = "<option value='all' selected>全部</option>";
      if(select_2 == "" || select_2 == "all"){ // 若網址無參數，或有 select_2為"all"時，將所有的 ol_ids 存至 select_2_ol_ids 陣列
        select_2 = "all";
        //$.each(response.data, function( index, item ) {
          //select_2_ol_ids.push(item.ol_id)
        //});
      }else{
        //select_2_ol_ids.push(select_2)
      }
      $.each(response.data, function( index, item ) {
         html += '<option value="' + item.ol_id + '">' + item.ol_display_name + '</option>';
      });
      $("#select2").append(html);
      $("#select2").val(select_2);

      getOutlineNextNodeForSelect3(select_2, otId);
      //$("#select2_text").html($("#select2 option:selected").text());
      //getVideoRiver();
    }
    //else{
      //$("#page_container").addClass("no_display");
    //}
  });
}

/**
 * 取得年級群組資料：第三個下拉選單
 */
function getOutlineNextNodeForSelect3(olId, otId) {
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( response ) {
    if(response.data.length > 0){
      //var version_data = [];
      $.each(response.data, function( index, item ) {
        version_data.push({
          ol_id: item.ol_id,
          ol_display_name: item.ol_display_name
        });
      });
      getOutlineNextNodeForSelect3Again(otId);
    }
  });
}

function getOutlineNextNodeForSelect3Again(otId) {

  //console.log(version_data);
  $.get( g_obj.domain + '/api/outline/nextNode?ol_id=' + version_data[callForSelect3].ol_id + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'), function( res ) {
    //console.log(res);
    if(res.data.length > 0){
      $.each(res.data, function( i, item ) {
        //console.log(item);
        //alert(callForSelect3);
        select3_data.push({
          ol_id: item.ol_id,
          ol_display_name: item.ol_display_name,
          version_name: (version_data[callForSelect3].ol_display_name == "不分版" ? "" : version_data[callForSelect3].ol_display_name )
        });
      });
      callForSelect3++;
      if(callForSelect3 < version_data.length){
        getOutlineNextNodeForSelect3Again(otId);
      }
      if(callForSelect3 == version_data.length){
        //console.log(select3_data);
        // 將 select3_data 裡的資料，放入第三個下拉選單
        var select3_html = "";
        $.each(select3_data, function( i, item ) {
          if( i == 0 ){
            if(select_3 == ""){
              select_3 = item.ol_id;
            }
          }
          select3_html += '<option value="' + item.ol_id + '">' + item.version_name + item.ol_display_name + '</option>';
        });
        $("#select3").html(select3_html);
        $("#select3").val(select_3);

        // 取得答題河流資料
        getTestRecordTotalAndRightCountBig();
        //getVideoRiverSmallStep1(otId);
        //getTestRecordTotalAndRightCountStep1(otId);
      }
    }
  });

}


// 取得答題河流資料
function getTestRecordTotalAndRightCountBig(){
  //var call_times = 0;
  //alert(select_3);
  $.ajax({
    url: g_obj.domain + '/api/test/record/totalAndRightCount?token=' + Cookies.get('u_t') + '&ol_id=' + select_3,
    type: 'get',
    // data: data,
    dataType: 'json',
    //headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      //console.log(res);

      $("#select3_text").text($("#select3 option:selected").text());
      $("#count_test_correct").text(res.data.test_correct_count);
      $("#count_test_already").text(res.data.test_already_count);
      $("#count_test_total").text(res.data.test_total + '題');

      subject_text = $("#select2 option:selected").text();
      // 大的瓶子
      var big_bottle = '';
      big_bottle += '<div class="subject_block">';
      big_bottle +=   '<div class="subject -empty" style="height: ' + (((res.data.test_total - res.data.test_already_count) / res.data.test_total) * 100) + '%;"></div>'; // 空的佔位
      big_bottle +=   '<div class="subject -water" style="height: ' + (((res.data.test_already_count - res.data.test_correct_count) / res.data.test_total) * 100) + '%;"></div>'; // 水
      if(subject_text == "國文"){
        big_bottle +=   '<div class="subject -purple" style="height: ' + ( (res.data.test_correct_count / res.data.test_total) * 100 ) + '%;"></div>'; // 國文
      }
      if(subject_text == "英文"){
        big_bottle +=   '<div class="subject -red1" style="height: ' + ( (res.data.test_correct_count / res.data.test_total) * 100 ) + '%;"></div>'; // 英文
      }
      if(subject_text == "數學"){
        big_bottle +=   '<div class="subject -blue" style="height: ' + ( (res.data.test_correct_count / res.data.test_total) * 100 ) + '%;"></div>'; // 數學
      }
      if(subject_text == "生物"){
        big_bottle +=   '<div class="subject -red2" style="height: ' + ( (res.data.test_correct_count / res.data.test_total) * 100 ) + '%;"></div>'; // 生物
      }
      if(subject_text == "地球科學"){
        big_bottle +=   '<div class="subject -yellow" style="height: ' + ( (res.data.test_correct_count / res.data.test_total) * 100 ) + '%;"></div>'; // 地球科學
      }
      if(subject_text == "物理"){
        big_bottle +=   '<div class="subject -green2" style="height: ' + ( (res.data.test_correct_count / res.data.test_total) * 100 ) + '%;"></div>'; // 物理
      }
      if(subject_text == "化學"){
        big_bottle +=   '<div class="subject -green1" style="height: ' + ( (res.data.test_correct_count / res.data.test_total) * 100 ) + '%;"></div>'; // 化學
      }
      if(subject_text == "歷史"){
        big_bottle +=   '<div class="subject -brown" style="height: ' + ( (res.data.test_correct_count / res.data.test_total) * 100 ) + '%;"></div>'; // 歷史
      }
      if(subject_text == "地理"){
        big_bottle +=   '<div class="subject -pink" style="height: ' + ( (res.data.test_correct_count / res.data.test_total) * 100 ) + '%;"></div>'; // 地理
      }
      if(subject_text == "公民"){
        big_bottle +=   '<div class="subject -green3" style="height: ' + ( (res.data.test_correct_count / res.data.test_total) * 100 ) + '%;"></div>'; // 公民
      }
      big_bottle += '</div>';
      $("#main_bottle").html(big_bottle);

      // 小瓶子
      if(res.data.nodes.length > 0){
        $.each(res.data.nodes, function( index, item_obj ) {
          small_bottle_arr.push({
            ol_id: item_obj.ol_id,
            ol_display_name: item_obj.ol_display_name,
            river: []
          });
        });
      }

      getTestRecordTotalAndRightCountSmall();

    }
  });

}

// 取得答題河流資料
var callGetTestRecordTotalAndRightCountSmallTimes = 0;
function getTestRecordTotalAndRightCountSmall(){
  //console.log(small_bottle_arr);
  $.ajax({
    url: g_obj.domain + '/api/test/record/totalAndRightCount?token=' + Cookies.get('u_t') + '&ol_id=' + small_bottle_arr[callGetTestRecordTotalAndRightCountSmallTimes].ol_id,
    type: 'get',
    // data: data,
    dataType: 'json',
    //headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      small_bottle_arr[callGetTestRecordTotalAndRightCountSmallTimes].river = res.data.nodes
      callGetTestRecordTotalAndRightCountSmallTimes++;
      //console.log(small_bottle_arr);
      if(callGetTestRecordTotalAndRightCountSmallTimes < small_bottle_arr.length){
        getTestRecordTotalAndRightCountSmall();
      }
      if(callGetTestRecordTotalAndRightCountSmallTimes == small_bottle_arr.length){
        renderSmallBottleData();
      }
    }
  });

}

function renderSmallBottleData(){
  //console.log(small_bottle_arr);
  var bottle_list_html = '';
  $.each(small_bottle_arr, function(i, item){
    $.each(item.river, function(j, river){
      bottle_list_html += '<li>';
      bottle_list_html +=   '<div class="bottle_parent">';
      bottle_list_html +=     '<p class="para1 -small">' + river.test_correct_count + ' 題</p>';
      bottle_list_html +=     '<p class="para2 -small">' + river.test_already_count + ' 題</p>';

      bottle_list_html +=     '<div class="bottle -small">';
      bottle_list_html +=       '<div class="subject_block">';
      bottle_list_html +=         '<div class="subject -empty" style="height: ' + ( ((river.test_total - river.test_already_count) / river.test_total) * 100 ) + '%;"></div>'; // 空的佔位
      bottle_list_html +=         '<div class="subject -water" style="height: ' + ( ((river.test_already_count - river.test_correct_count) / river.test_total) * 100 ) + '%;"></div>'; // 水
      if(subject_text == '國文'){
        bottle_list_html +=       '<div class="subject -purple" style="height: ' + ( ( river.test_correct_count / river.test_total) * 100 ) + '%;"></div>'; // 國文
        bottle_list_html +=     '</div>';
        if( (river.test_already_count / river.test_total) >= 0.5 ){ // 做答一半以上，不會出現鎖頭
          bottle_list_html +=   '<span class="pipe_line -water -purple"></span>';
        }else{
          bottle_list_html +=   '<span class="pipe_line -lock"><img src="./images/icon/icon-lock.svg" class="icon_lock"></span>';
        }
      }
      if(subject_text == '英文'){
        bottle_list_html +=       '<div class="subject -red1" style="height: ' + ( ( river.test_correct_count / river.test_total) * 100 ) + '%;"></div>'; // 英文
        bottle_list_html +=     '</div>';
        if( (river.test_already_count / river.test_total) >= 0.5 ){ // 做答一半以上，不會出現鎖頭
          bottle_list_html +=   '<span class="pipe_line -water -red1"></span>';
        }else{
          bottle_list_html +=   '<span class="pipe_line -lock"><img src="./images/icon/icon-lock.svg" class="icon_lock"></span>';
        }
      }
      if(subject_text == '數學'){
        bottle_list_html +=       '<div class="subject -blue" style="height: ' + ( ( river.test_correct_count / river.test_total) * 100 ) + '%;"></div>'; // 數學
        bottle_list_html +=     '</div>';
        if( (river.test_already_count / river.test_total) >= 0.5 ){ // 做答一半以上，不會出現鎖頭
          bottle_list_html +=   '<span class="pipe_line -water -blue"></span>';
        }else{
          bottle_list_html +=   '<span class="pipe_line -lock"><img src="./images/icon/icon-lock.svg" class="icon_lock"></span>';
        }
      }
      if(subject_text == "生物"){
        bottle_list_html +=       '<div class="subject -red2" style="height: ' + ( ( river.test_correct_count / river.test_total) * 100 ) + '%;"></div>'; // 生物
        bottle_list_html +=     '</div>';
        if( (river.test_already_count / river.test_total) >= 0.5 ){ // 做答一半以上，不會出現鎖頭
          bottle_list_html +=   '<span class="pipe_line -water -red2"></span>';
        }else{
          bottle_list_html +=   '<span class="pipe_line -lock"><img src="./images/icon/icon-lock.svg" class="icon_lock"></span>';
        }
      }
      if(subject_text == "地球科學"){
        bottle_list_html +=       '<div class="subject -yellow" style="height: ' + ( ( river.test_correct_count / river.test_total) * 100 ) + '%;"></div>'; // 地球科學
        bottle_list_html +=     '</div>';
        if( (river.test_already_count / river.test_total) >= 0.5 ){ // 做答一半以上，不會出現鎖頭
          bottle_list_html +=   '<span class="pipe_line -water -yellow"></span>';
        }else{
          bottle_list_html +=   '<span class="pipe_line -lock"><img src="./images/icon/icon-lock.svg" class="icon_lock"></span>';
        }
      }
      if(subject_text == "物理"){
        bottle_list_html +=       '<div class="subject -green2" style="height: ' + ( ( river.test_correct_count / river.test_total) * 100 ) + '%;"></div>'; // 物理
        bottle_list_html +=     '</div>';
        if( (river.test_already_count / river.test_total) >= 0.5 ){ // 做答一半以上，不會出現鎖頭
          bottle_list_html +=   '<span class="pipe_line -water -green2"></span>';
        }else{
          bottle_list_html +=   '<span class="pipe_line -lock"><img src="./images/icon/icon-lock.svg" class="icon_lock"></span>';
        }
      }
      if(subject_text == "化學"){
        bottle_list_html +=       '<div class="subject -green1" style="height: ' + ( ( river.test_correct_count / river.test_total) * 100 ) + '%;"></div>'; // 化學
        bottle_list_html +=     '</div>';
        if( (river.test_already_count / river.test_total) >= 0.5 ){ // 做答一半以上，不會出現鎖頭
          bottle_list_html +=   '<span class="pipe_line -water -green1"></span>';
        }else{
          bottle_list_html +=   '<span class="pipe_line -lock"><img src="./images/icon/icon-lock.svg" class="icon_lock"></span>';
        }
      }
      if(subject_text == "歷史"){
        bottle_list_html +=       '<div class="subject -brown" style="height: ' + ( ( river.test_correct_count / river.test_total) * 100 ) + '%;"></div>'; // 歷史
        bottle_list_html +=     '</div>';
        if( (river.test_already_count / river.test_total) >= 0.5 ){ // 做答一半以上，不會出現鎖頭
          bottle_list_html +=   '<span class="pipe_line -water -brown"></span>';
        }else{
          bottle_list_html +=   '<span class="pipe_line -lock"><img src="./images/icon/icon-lock.svg" class="icon_lock"></span>';
        }
      }
      if(subject_text == "地理"){
        bottle_list_html +=       '<div class="subject -pink" style="height: ' + ( ( river.test_correct_count / river.test_total) * 100 ) + '%;"></div>'; // 地理
        bottle_list_html +=     '</div>';
        if( (river.test_already_count / river.test_total) >= 0.5 ){ // 做答一半以上，不會出現鎖頭
          bottle_list_html +=   '<span class="pipe_line -water -pink"></span>';
        }else{
          bottle_list_html +=   '<span class="pipe_line -lock"><img src="./images/icon/icon-lock.svg" class="icon_lock"></span>';
        }
      }
      if(subject_text == "公民"){
        bottle_list_html +=       '<div class="subject -green3" style="height: ' + ( ( river.test_correct_count / river.test_total) * 100 ) + '%;"></div>'; // 公民
        bottle_list_html +=     '</div>';
        if( (river.test_already_count / river.test_total) >= 0.5 ){ // 做答一半以上，不會出現鎖頭
          bottle_list_html +=   '<span class="pipe_line -water -green3"></span>';
        }else{
          bottle_list_html +=   '<span class="pipe_line -lock"><img src="./images/icon/icon-lock.svg" class="icon_lock"></span>';
        }
      }

      if(river.test_correct_count >= 10){ // 做對10題以上，不出現紅線
        bottle_list_html +=     '<span class="red_line no_display"></span>';
      }else{
        bottle_list_html +=     '<span class="red_line"></span>';
      }
      bottle_list_html +=     '</div>';

      bottle_list_html +=     '<p class="para6">' + river.test_total + '題</p>';
      bottle_list_html +=     '<p class="para7">' + item.ol_display_name + '</p>';
      bottle_list_html +=     '<p class="para8">' + river.ol_display_name + '</p>';
      bottle_list_html +=   '</div>';
      bottle_list_html += '</li>';
    });
  });
  $("#bottle_list").html(bottle_list_html);
  $("#bottle_list").children("li").last().find("span.pipe_line").addClass("no_display");

  setPager();
}

/**
 * 設定分頁
 */
function setPager() {
  //$("#bottle_list").html(totalContent);
  $("#bottle_list").children("li").each(function(index){
    if(index >= 7){
      $(this).addClass("no_display");
    }
  });
  var totalPage = Math.ceil($("#bottle_list").children().length / 7);
  $("#total_page").html(str_pad(totalPage, 2, 0));
  if (totalPage == 1) {
    $("#pager_right_button").addClass("-disabled");
  }
  if (totalPage > 1) {
    $("#pager_right_button").removeClass("-disabled");
  }
}
