var ol_id;
var olId;
var unitOlId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var ho_id; // 講義 id
var ho_is_ltr; // 講義方向
var paragraphData = []; // 段落資料
var file_path_data = []; // 講義圖片資料
var next_link = '';

$(function(){
	// 取得頁面間傳遞參數
	ol_id = $.url('?ol_id');
	if (typeof ol_id == "undefined") ol_id = "";
});

// post 回傳的資料存於此，假設資料如下：
/*
var post_res_data = [
 {
   "ho_id": 1,
   "ho_is_ltr": 1
 }
];
*/
var post_res_data = [];
var ol_ids = [];


// 取得大綱節點下講義
function newHandoutGet() {
	var olIDArray = { ol_ids };
	var myJSON = JSON.stringify(olIDArray);
	/* 參考 JSON: {"ol_ids":[15,16]} */
	$.ajax({
		url: g_obj.domain + '/api/handout/get?token=' + Cookies.get('u_t'),
		type: 'post',
		data: myJSON,
		dataType: 'json',
		contentType: "application/json; charset=utf-8",
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					post_res_data = response.data;
					console.log(response.data);

					// 取得右側展開之資料，並會接續取得講義資料
					getHandoutPara();
					break;
				default:
					alert(response.msg);
					break;
			}
		}
	});
}

// 取得大綱節點下講義
function newHandoutRecord() {
	$.ajax({
		url: g_obj.domain + '/api/handout/record',
		type: 'POST',
		data: "ho_id=" + post_res_data[callGetHandoutFileTimes].ho_id + "&ol_id=" + ol_id,
		headers: {
            "token": Cookies.get('u_t'),
            "Content-Type": "application/x-www-form-urlencoded"
        },
		dataType: 'json',
		contentType: "application/json; charset=utf-8",
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					console.log(response.data);
					// 取得右側展開之資料，並會接續取得講義資料
					break;
				default:
					alert(response.msg);
					break;
			}
		}
	});
}

$(function(){
  // 取得頁面間傳遞參數
  olId = $.url('?ol_id');
  unitOlId = $.url('?unit_ol_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');
  versionName = $.url('?version_name');
  gradeGroupName = $.url('?grade_group_name');
  chapterName = $.url('?chapter_name');
  partName = $.url('?part_name');

	checkHeaderTextAfterUnit(unitOlId);
  //$("#header_field").text(fieldName);
  //$("#header_subject").text(subjectName);
  //$("#header_grade_group").text(versionName + gradeGroupName);

  $("#header_unit").attr("href", "#");

  $("#header_unit").attr("href", "./m_e_unit.html?ol_id=" + unitOlId + "&field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + versionName + "&grade_group_name=" + gradeGroupName);

  $("#main_title").text(subjectName + " " + ((versionName)?versionName + " " : "") + gradeGroupName + " " + chapterName + " " + partName);
  // $("#course_name").text(subjectName + versionName + gradeGroupName);

  setHeaderGradeGroupHref(unitOlId);

  // 取得大綱節點下講義
  ol_ids[0] = olId;
  newHandoutGet();

	// Next 按鈕連結
	next_link_func();
});

// 取得段落資料
callGetHandoutParaTimes = 0;
function getHandoutPara(){
  // paragraphData
  if(post_res_data.length > 0){
    ho_id = post_res_data[0].ho_id;
    ho_is_ltr = post_res_data[0].ho_is_ltr;
  }

  $.get( g_obj.domain + "/api/handout/paragraph?ho_id=" + post_res_data[callGetHandoutParaTimes].ho_id + '&token=' + Cookies.get('u_t'), function( res ) {
    if( res.data.length > 0 ){
      $.each(res.data, function(i, item){
        paragraphData.push(item);
      });
    }
    callGetHandoutParaTimes++;
    if(callGetHandoutParaTimes < post_res_data.length){
      getHandoutPara();
    }else{
      renderHandoutPara();

      // 再去取得各講義圖片
      getHandoutFile()
    }
  });
}

// 右側段落 render
function renderHandoutPara(){
  //console.log(paragraphData);
  var para_html = '';
  $.each(paragraphData, function(i, item){
    //console.log(item.hp_id);
    if(item.hp_id != undefined){ // 段落
      para_html += '<li>';
      para_html +=   '<span class="item_block">';
      para_html +=     '<span class="str_text -title"><span class="circle"><span class="circle_inner"></span></span>' + item.hp_name + '</span>';
      para_html +=   '</span>';
      para_html += '</li>';
      if(item.nodes.length > 0){
        $.each(item.nodes, function(j, sub_para){
          para_html += '<li>';
          para_html +=   '<a href="#" class="item_block -go_page" data-hp-page-index="' + sub_para.hp_page_index + '">'; // -on
          para_html +=     '<span class="str_text">' + sub_para.hp_name + '</span>';
          para_html +=   '</a>';
          para_html += '</li>';
        });
      }
    }else{ // 子段落
      if(item.nodes.length > 0){
        $.each(item.nodes, function(j, sub_para){
          para_html += '<li>';
          para_html +=   '<a href="#" class="item_block -go_page" data-hp-page-index="' + sub_para.hp_page_index + '">'; // -on
          para_html +=     '<span class="str_text">' + sub_para.hp_name + '</span>';
          para_html +=   '</a>';
          para_html += '</li>';
        });
      }
    }
  });
  $("#para_desc_list").html(para_html);
  $("#para_desc_list").find("a.-go_page").first().addClass("-on");
}

// 取得電子講義
callGetHandoutFileTimes = 0;
function getHandoutFile(){
	newHandoutRecord();
  $.get( g_obj.domain + "/api/handout/file?ho_id=" + post_res_data[callGetHandoutFileTimes].ho_id + '&token=' + Cookies.get('u_t'), function( res ) {
    if(res.data.length > 0){
      $.each(res.data, function(i, item){
        file_path_data.push(item);
      });
    }
    callGetHandoutFileTimes++;
    if(callGetHandoutFileTimes < post_res_data.length){
      getHandoutFile();
    }else{
      // 顯示講義圖片
      renderFilePathData();
    }
  });

}

// 顯示講義圖片
function renderFilePathData(){
  //console.log(file_path_data);
  var file_html = '';
  $.each(file_path_data, function(i, item){
    file_html += '<div style="background-image:url(' + item.hd_path + ')" class="each_page">';
	file_html +=   '<img id="page_' + i + '" src="' + item.hd_path + '" style="display:none;" crossOrigin="Anonymous" />';
    file_html +=   '<div class="custom_checkbox ' + ( (i%2 == 0 ? "-right-bottom": "-left-bottom") )  + '">';
    file_html +=     '<input type="checkbox" id="custom_checkbox_left_bottom_' + i + '" class="custom_checkbox" value="page_' + i + '" />';
    file_html +=     '<label for="custom_checkbox_left_bottom_' + i + '" class="-checkitem"></label>';
    file_html +=   '</div>';
    file_html += '</div>';
  })
  $("#flipbook").html(file_html);
  loadApp( (ho_is_ltr == 1 ? "rtl" : "ltr") );

  // 判斷收藏：電子講義
  getUsercollection(g_obj.m_e_ot_id, olId, "", "", ho_id, "");
}

function next_link_func(){
	$.ajax({
		url: g_obj.domain + '/api/outline/nextTwoNode?token=' + Cookies.get('u_t') + '&ol_id=' + unitOlId + '&ot_id=' + g_obj.m_e_ot_id,
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        //console.log(response);
      }
    },
    success: function(response){
			console.log(response);
			if(response.data.length > 0){
				//unitOlId
				//alert(olId);
				var next_two_nodes_data = response.data;
				$.each(response.data, function( index, item ) {
					if(item.ol_id == olId){
						if(next_two_nodes_data.length == index + 1){
							$("a.btn_action.-link").addClass("-disabled");
						}else{
							$("a.btn_action.-link").attr("href", "./m_e_paper.html?ol_id=" + next_two_nodes_data[index+1].ol_id + "&unit_ol_id=" + unitOlId + "&field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + versionName + "&grade_group_name=" + gradeGroupName + "&chapter_name=" + next_two_nodes_data[index+1].ol_display_name_1 + "&part_name=" + next_two_nodes_data[index+1].ol_display_name_2);
							$("a.btn_action.-link").removeClass("-disabled");
						}
					}
	      });
			}
    }
  });
}
