var olId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterCount;
var totalCount = 0;
var totalContent = "";
//var chapterData;

$(function(){
  // 取得頁面間傳遞參數
  olId = $.url('?ol_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');
  versionName = $.url('?version_name');
  gradeGroupName = $.url('?grade_group_name');

  // 設定導覽列
  checkHeaderTextUnit(olId);
  //$("#header_field").text(fieldName);
  //$("#header_subject").text(subjectName);
  //$("#header_grade_group").text(versionName + gradeGroupName);
  $("#header_unit").attr("href", "#");

  getChapterAndPart(olId, g_obj.m_e_ot_id);


  // 設定導覽列連結
  setHeaderGradeGroupHref(olId);

  left_aside_link(3); // 更換大類別連結
});

// 取得章和節資料
function getChapterAndPart(olId, otId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/nextTwoNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        $.each(response.data, function( index, item ) {
           totalContent += '<li class="' + (totalCount <= 14 ? "" : "no_display") + '"><a href="./m_e_paper.html?ol_id=' + item.ol_id + '&unit_ol_id=' + olId + '&field_name=' + fieldName + '&subject_name=' + subjectName + '&version_name=' + versionName + '&grade_group_name=' + gradeGroupName + '&chapter_name=' + item.ol_display_name_1 + '&part_name=' + item.ol_display_name_2 + '" class="item_link"><img src="' + item.ol_img + '" class="link_img"><div class="desc_block"><p class="para1">' + item.ol_display_name_1 + '</p><p class="para2">' + item.ol_display_name_2 + '</p></div><div class="custom_checkbox"><input type="checkbox" id="custom_checkbox_'+ item.ol_id +'" value="'+ item.ol_id +'" class="custom_checkbox"><label for="custom_checkbox_'+ item.ol_id +'" class="-checkitem">Custom Checkbox!</label></div></a></li>';
           totalCount++;
        });
        setPager();
      } else {
        $("#item_list").find("span.-loading").text("此課程無電子講義");
        $("#total_page").html(str_pad(1, 2, 0));
        $("#pager_right_button").addClass("-disabled");
      }
    }
  });
}

/**
 * 取得節資料
 */
/*
function getOutlineNextNode(olId, otId, chapterName, unitOlId) {
  $.ajax({
    url: g_obj.domain + '/api/outline/nextNode?ol_id=' + olId + '&ot_id=' + otId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      var count = response.data.length;
      if(count > 0){

        $.each(response.data, function( index, item ) {
           // console.log(item.ol_id);
           if (totalCount <= 14) {
              totalContent += '<li><a href="./m_e_paper.html?ol_id=' + item.ol_id + '&unit_ol_id=' + unitOlId + '&field_name=' + fieldName + '&subject_name=' + subjectName + '&version_name=' + versionName + '&grade_group_name=' + gradeGroupName + '&chapter_name=' + chapterName + '&part_name=' + item.ol_display_name + '" class="item_link"><img src="' + item.ol_img + '" class="link_img"><div class="desc_block"><p class="para1">' + chapterName + '</p><p class="para2">' + item.ol_display_name + '</p></div><div class="custom_checkbox"><input type="checkbox" id="custom_checkbox_'+ item.ol_id +'" value="'+ item.ol_id +'" class="custom_checkbox"><label for="custom_checkbox_'+ item.ol_id +'" class="-checkitem">Custom Checkbox!</label></div></a></li>';
           } else {
              totalContent += '<li class="no_display"><a href="./m_e_paper.html?ol_id=' + item.ol_id + '&unit_ol_id=' + unitOlId + '&field_name=' + fieldName + '&subject_name=' + subjectName + '&version_name=' + versionName + '&grade_group_name=' + gradeGroupName + '&chapter_name=' + chapterName + '&part_name=' + item.ol_display_name + '" class="item_link"><img src="' + item.ol_img + '" class="link_img"><div class="desc_block"><p class="para1">' + chapterName + '</p><p class="para2">' + item.ol_display_name + '</p></div><div class="custom_checkbox"><input type="checkbox" id="custom_checkbox_'+ item.ol_id +'" value="'+ item.ol_id +'" class="custom_checkbox"><label for="custom_checkbox_'+ item.ol_id +'" class="-checkitem">Custom Checkbox!</label></div></a></li>';
           }

           totalCount++;
        });

      }

      chapterCount--;
      if (chapterCount == 0) { // 所有資料組合完後再設定分頁
        setPager();
      }else{
        preparePartData();
      }
    }
  });
}
*/

// 取得大綱節點下講義Pdf
function handoutGetPdf() {
	var tmp_chapter_name = '';
	$.each($(".custom_checkbox:checked"), function (i, v) {
		if (tmp_chapter_name == '') tmp_chapter_name = $(this).closest("a").find(".para1").html();
	})
	var ol_ids = ($(".custom_checkbox:checked").map(function () {return this.value;}).get().join(","));
	if (ol_ids == '') {
		alert('請先選擇單元');
		return;
	}
	/* 參考 JSON: {"token": "testToken", "ol_ids": [16, 17]} */
	$.ajax({
		// url: "https://test.azul.com.tw:15200/handout_ol_id_58.pdf",
		url: g_obj.domain + '/api/handout/getPdf?token=' + Cookies.get('u_t') + '',
		type: "POST",
		data: '{"token": "' + Cookies.get('u_t') + '", "ol_ids": [' + ol_ids + ']}',
		dataType: "binary",
		contentType: "application/json; charset=utf-8",
		processData: false,
		success: function(response){
			// console.log(response);
			var binaryData = [];
			binaryData.push(response);
			var downloadUrl = window.URL.createObjectURL(new Blob(binaryData, {type: "application/pdf"}))
			// window.open(downloadUrl);

			var a = document.createElement("a");
			a.href = downloadUrl;
			a.download = "電子講義_" + (($.url('?field_name') != undefined) ? $.url('?field_name') + "_" : "") + (($.url('?subject_name') != undefined) ? $.url('?subject_name') + "_" : "") + (($.url('?version_name') != undefined) ? $.url('?version_name') + "_" : "") + (($.url('?grade_group_name') != undefined) ? $.url('?grade_group_name') + "_" : "") + tmp_chapter_name + "_" + new Date().getFullYear().toString().substr(-2) + ("0" + (new Date().getMonth() + 1).toString()).substr(-2) + ("0" + new Date().getDate().toString()).substr(-2) + ("0" + new Date().getHours().toString()).substr(-2) + ("0" + new Date().getMinutes().toString()).substr(-2) + ("0" + new Date().getSeconds().toString()).substr(-2) + ".pdf";
			document.body.appendChild(a);
			a.click();
		}
	});
}

// 設定分頁
function setPager() {
	$("#item_list").html(totalContent);
	var totalPage = Math.ceil(totalCount / 15);
	$("#total_page").html(str_pad(totalPage, 2, 0));
	if (totalPage == 1) {
		$("#pager_right_button").addClass("-disabled");
	}
  if (totalPage > 1) {
		$("#pager_right_button").removeClass("-disabled");
	}
}
