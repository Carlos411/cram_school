var scopes = {
    'teacherVue': null,
    'praticeVue': null
};

for (var scopeName in scopes) {
    scopes[scopeName] = new Vue({
        el:'#' + scopeName ,
        data: {
            current_page: 1,
            totalPage:1,
            displayPageNumber:12,
            ot_id: g_obj.teacher_ot_id,
            dbFields:[],
            dbSubjects:[],
            dbVersions:[],
            dbGradeGroups:[],
            dbUnits:[],
            dbParts:[],
            version_ol_id:0,
            abbName: {
                field: '領域',
                subject:'科目',
                gradeGroup:'年級/群組',
                unit:'單元'
            },
            path_ol_ids: []
        },
        methods: {
            titleControl:function() {
                var self = this;
                self.empty_data();
                return {
                    field:function() {
                        self.abbName.field = '領域';
                        self.abbName.subject = '科目';
                        self.abbName.gradeGroup = '年級/群組';
                        // scopes.teacherVue.abbName = self.abbName;
                        // scopes.praticeVue.abbName = self.abbName;
                    },
                    subject:function() {
                        self.abbName.subject = '科目';
                        self.abbName.gradeGroup = '年級/群組';
                        // scopes.teacherVue.abbName = self.abbName;
                        // scopes.praticeVue.abbName = self.abbName;
                        self.nextData(self.path_ol_ids[0],function(data) {
                            console.log(data);
                            self.dbSubjects = data;
                        });
                    },
                    group:function() {
                        self.abbName.gradeGroup = '年級/群組';
                        // scopes.teacherVue.abbName = self.abbName;
                        // scopes.praticeVue.abbName = self.abbName;
                        self.nextData(self.path_ol_ids[1],function(data) {
                            if (data.length > 0) {
                                self.version_ol_id = data[0].ol_id;
                                self.dataControl().gradeGroup(data[0]);
                            }
                            self.dbVersions = data;
                        });
                    },
                    unit:function() {
                        self.dbUnits = [];
                        // self.abbName.gradeGroup = item.ol_abbreviation;
                        var parameters = [
                            'token=' + Cookies.get('u_t'),
                            'ol_id=' + self.path_ol_ids[2],
                            'ot_id=' + self.ot_id
                        ].join('&');
                        console.log("parameter:",parameters);
                        var url = g_obj.domain + '/api/outline/nextTwoNode?' + parameters;
                        $.get(url, function(response){
                            console.log("TwoNode:",response.data);
                            self.dbParts = response.data;
                        });
                    }
                }
            },
            pageControl: function() {
                var self = this;
                return {
                    lastPage: function () {
                        if (self.current_page > 1) {
                            self.current_page -= 1;
                        }

                    },
                    nextPage: function () {
                        if (self.current_page < self.totalPage) {
                            self.current_page += 1;
                        }

                    },
                    displayRange: function (index) {

                        if (index >= self.current_page * self.displayPageNumber) {
                            return true;
                        }
                        if (index < (self.current_page - 1) * self.displayPageNumber) {
                            return true;
                        }
                        console.log(index);
                        return false;
                    }
                }
            },
            dataControl: function() {
                var self = this;
                self.empty_data();
                return {
                    subject:function(item) {
                        self.dbSubjects = [];
                        self.abbName.field = item.ol_abbreviation;

                        self.syncData().abbName();
                        self.syncData().path_ol_ids(0, item.ol_id);

                        self.nextData(item.ol_id,function(data) {
                            console.log(data);
                            self.dbSubjects = data;
                        });
                    },
                    version:function(item) {
                        self.dbVersions = [];
                        self.abbName.subject = item.ol_abbreviation;

                        self.syncData().abbName();
                        self.syncData().path_ol_ids(1, item.ol_id);

                        self.nextData(item.ol_id,function(data) {
                            if (data.length > 0) {
                                self.version_ol_id = data[0].ol_id;
                                self.dataControl().gradeGroup(data[0]);
                            }
                            self.dbVersions = data;
                        });
                    },
                    gradeGroup:function(item) {
                        self.dbGradeGroups = [];
                        self.version_ol_id = item.ol_id;

                        self.nextData(item.ol_id,function(data) {
                            console.log(data);
                            self.dbGradeGroups = data;
                        });
                    },
                    unit:function(item) {
                        self.dbUnits = [];
                        self.abbName.gradeGroup = item.ol_abbreviation;

                        self.syncData().abbName();
                        self.syncData().path_ol_ids(2, item.ol_id);

                        var parameters = [
                            'token=' + Cookies.get('u_t'),
                            'ol_id=' + item.ol_id,
                            'ot_id=' + self.ot_id
                        ].join('&');
                        console.log("parameter:",parameters);
                        var url = g_obj.domain + '/api/outline/nextTwoNode?' + parameters;
                        $.get(url, function(response){
                            console.log("TwoNode:",response.data);
                            self.dbParts = response.data;
                        });


                        // self.dbUnits = [];
                        // self.abbName.gradeGroup = item.ol_abbreviation;
                        // // self.version_ol_id = item.ol_id;
                        // self.nextData(item.ol_id,function(data) {
                        //     console.log(data);
                        //     self.dbUnits = data;
                        //     // self.totalPage = Math.ceil(self.dbUnits.length / self.displayPageNumber);
                        //     self.dataControl().partPromise(data);
                        // });
                    },
                    // part:function(items) {
                    //
                    //     console.log("Items:",items);
                    //     self.dbParts = [];
                    //
                    //     for(var i = 0; i < items.length; i += 1) {
                    //
                    //         var item = items[i];
                    //         unit_name = item.ol_display_name;
                    //         self.partData(item.ol_id,unit_name,function(response,up_name){
                    //
                    //             for(key in response) {
                    //                 var data = response[key];
                    //                 data['unit_name'] = up_name;
                    //
                    //                 self.dbParts.push(data);
                    //
                    //             }
                    //             self.totalPage = Math.ceil(self.dbParts.length / self.displayPageNumber);
                    //
                    //         });
                    //
                    //     }
                    //
                    //
                    //
                    // },
                    partPromise:function(items) {
                        console.log("ITEMS:",items);
                        let myPromises = [];

                        self.dbParts = [];
                        var unitPartsCount = 0;
                        var unitParts = [];
                        for(i = 0; i < items.length; i += 1) {
                            unitParts[i] = [];
                            let item = items[i];
                            unit_name = item.ol_display_name;
                            self.partData(item.ol_id,unit_name,i,function(response,up_name,index){

                                unitParts[index].response = response;
                                unitParts[index].unit_name = up_name;
                                unitPartsCount += 1;
                                if (unitPartsCount == items.length) {
                                    self.dePartData(unitParts);
                                }


                            });


                        }

                    }

                }
            },

            rootData:function(completion) {
                var parameters = [
                    'token=' + Cookies.get('u_t'),
                    'ec_id=' + g_obj.ec_id,
                    'ot_id=' + this.ot_id
                ].join('&');
                var url = g_obj.domain + '/api/outline/root?' + parameters;
                $.get(url, function(response){
                    console.log("Response:",response);
                    completion(response.data);
                });
            },
            nextData:function(ol_id, completion) {
                var parameters = [
                    'token=' + Cookies.get('u_t'),
                    'ol_id=' + ol_id,
                    'ot_id=' + this.ot_id
                ].join('&');
                var url = g_obj.domain + '/api/outline/nextNode?' + parameters;
                $.get(url, function(response){
                    console.log(response);
                    completion(response.data);
                });

            },
            partData:function(ol_id, unit_name,index,completion) {
                var parameters = [
                    'token=' + Cookies.get('u_t'),
                    'ol_id=' + ol_id,
                    'ot_id=' + this.ot_id
                ].join('&');
                var url = g_obj.domain + '/api/outline/nextNode?' + parameters;
                $.get(url, function(response){
                    console.log(response);
                    console.log("Part UpName:",unit_name)
                    completion(response.data,unit_name,index);
                });

            },
            dePartData:function(dePartDatas) {
                console.log("DePartDatas:",dePartDatas);
                // console.log("UP_NAME:",up_name);
                var self = this;
                for (key in dePartDatas) {
                    var dePartData = dePartDatas[key].response;
                    var unit_name = dePartDatas[key].unit_name;
                    console.log("unit_name",unit_name);
                    for (index in dePartData) {
                        var data = dePartData[index];
                        data["unit_name"] = unit_name;
                        self.dbParts.push(data);
                    }

                }
                self.totalPage = Math.ceil(self.dbParts.length / self.displayPageNumber);
            },
            init: function(ot_id) {
                var self = this;
                self.ot_id = ot_id;
                self.rootData(function(mainData) {
                    console.log("main:",mainData);
                    self.nextData(mainData[0].ol_id,function(data) {
                        console.log(data);
                        self.dbFields = data;
                    });

                });

            },
            syncData: function(){
              var self = this;
              return {
                  abbName:function() {
                      scopes.teacherVue.abbName = self.abbName;
                      scopes.praticeVue.abbName = self.abbName;
                  },
                  path_ol_ids:function(path_index, path_ol_id) {
                      self.path_ol_ids[path_index] = path_ol_id
                      scopes.teacherVue.path_ol_ids = self.path_ol_ids;
                      scopes.praticeVue.path_ol_ids = self.path_ol_ids;
                  }
              }
            },
            empty_data: function(){
              scopes.teacherVue.dbSubjects = [];
              scopes.praticeVue.dbSubjects = [];
              scopes.teacherVue.dbVersions = [];
              scopes.praticeVue.dbVersions = [];
              scopes.teacherVue.dbGradeGroups = [];
              scopes.praticeVue.dbGradeGroups = [];
              scopes.teacherVue.dbUnits = [];
              scopes.praticeVue.dbUnits = [];
              scopes.teacherVue.dbParts = [];
              scopes.praticeVue.dbParts = [];
            }
        }
    });
}




scopes.teacherVue.init(g_obj.teacher_ot_id);
scopes.praticeVue.init(g_obj.db_practice_ot_id);
