$(function(){

  getApiUserStudent();
});

function getApiUserStudent(){
  $.ajax({
    url: g_obj.domain + '/api/user/student?token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    //headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      if(res.data.length > 0){
        $("#user_edit_link").attr("href", "./user_edit.html?us_id=" + res.data[0].us_id); // 修改資料按鈕，us_id 為 學生 id

        $("#us_name").html(res.data[0].us_name); // 學生姓名
        if(res.data[0].us_img != ""){
          //$("#us_img").attr("src", res.data[0].us_img); // 使用者圖片路徑
          $("#us_img").addClass("no_display");
          $("#us_img").closest("span.avatar_img").addClass("-add-border");
          $("#us_img").closest("span.avatar_img").attr("style", "background-image: url('" + res.data[0].us_img + "');");
        }
        $("#us_is_male").html((res.data[0].us_is_male == "1") ? "男" : "女" ); // 性別
        if(res.data[0].us_birthday != null){
          $("#us_birthday").html( ((res.data[0].us_birthday).split(" "))[0] ); // 使用者生日
        }
        $("#us_cell_phone").html(res.data[0].us_cell_phone); // 使用者行動電話
        $("#us_phone").html(res.data[0].us_phone); // 使用者電話
        $("#us_mail").html(res.data[0].us_mail); // 使用者電子郵件
        $("#us_school").html(res.data[0].us_school); // 使用者學校
        $("#us_grade").html(res.data[0].us_grade); // 使用者年級
        $("#us_class").html(res.data[0].us_class); // 使用者班級

        $("#up_name").html(res.data[0].up_name); // 使用者家長姓名
        $("#up_cell_phone").html(res.data[0].up_cell_phone); // 使用者家長行動電話
      }else{
        alert("無使用者資料");
      }
    }
  });
}
