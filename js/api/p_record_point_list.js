var token;
var countPerPage = 10;

$(function(){
  token = Cookies.get('u_t');

  getUserPoint(token);
  getUserParentStudent(token);
  getUserPointType();
});

$("button.btn_submit").on("click", function(e){
      e.preventDefault();

      var usId = $("#account_select").val();
      var upiId = $("#item_select").val();
      var dateStart = $('#date_start').val();
      var dateEnd = $('#date_end').val();

      // 判斷終止日要大於等於起始日才行
      var dateStartTimeStamp = new Date(dateStart).getTime();
      var dateEndTimeStamp = new Date(dateEnd).getTime();
      //console.log("開始：" + dateStartTimeStamp);
      //console.log("結束：" + dateEndTimeStamp);
      if(dateStartTimeStamp > dateEndTimeStamp){
        alert("日期區間有誤：起始日需早於或等於終止日。");
      }else{
        // 抓取資料：

        // 將 dateEnd 加1天
        if(dateEnd != ""){
          var tomorrow_date = new Date(dateEnd);
          tomorrow_date.setDate(tomorrow_date.getDate() + 1);
          dateEnd = tomorrow_date.getFullYear() + '/' + (tomorrow_date.getMonth()+1) + '/' + tomorrow_date.getDate();
        }

        getUserPointRecord(token, usId, upiId, dateStart, dateEnd);
      }
});

/**
 * 取得使用者點數
 */
function getUserPoint(token) {
  $.ajax({
    url: g_obj.domain + '/api/userpoint?token=' + token,
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data != null){
        var item = response.data;
        $("#current_point").html('目前剩餘點數：' + item.ups_point);
      }
    }
  });
}

/**
 * 取得使用者帳號
 */
function getUserParentStudent(token) {
  $.ajax({
    url: g_obj.domain + '/api/user/parent?token=' + token,
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      var up_id = response.data.devices[0].up_id;
      $.ajax({
        url: g_obj.domain + '/api/user/parent/student?up_id=' + up_id + '&token=' + token,
        type: 'get',
        // data: data,
        dataType: 'json',
        statusCode: {
          200: function (response) {
            // console.log(response);
          }
        },
        success: function(response){
          if(response.data.users.length > 0){
            var html = "";

            $.each(response.data.users, function( index, item ) {
               html += '<option value="' + item.us_id + '">' + item.us_name + '</option>';
            });

            $("#account_select").append(html);
          }
        }
      });
    }
  });
}

/**
 * 取得使用者點數紀錄項目
 */
function getUserPointType() {
  $.ajax({
    url: g_obj.domain + '/api/userpoint/type?token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        var html = "";

        $.each(response.data, function( index, item ) {
           html += '<option value="' + item.upi_id + '">' + item.upi_item + '</option>';
        });

        $("#item_select").append(html);
      }
    }
  });
}

/**
 * 取得使用者點數紀錄
 */
function getUserPointRecord(token, usId, upiId, dateStart, dateEnd) {
  var url = g_obj.domain + '/api/userpoint/record?token=' + token;

  if (usId != -1) {
      url += '&us_id=' + usId;
  }

  if (upiId != -1) {
      url += '&upi_id=' + upiId;
  }

  if (dateStart != '' && dateEnd != '') {
      url += '&upr_datetime_start=' + dateStart + '&upr_datetime_stop=' + dateEnd;
  }

  // console.log(url);

  $.ajax({
    url: url,
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      $("#search_result").html("");
      var count = response.data.length; // 共取得幾筆資料
      var html = "";

      html += '<table class="custom_table">';
      html +=   '<thead>';
      html +=     '<tr>';
      html +=       '<th>日期</th>';
      html +=       '<th>帳號</th>';
      html +=       '<th>項目</th>';
      html +=       '<th>增扣點數</th>';
      html +=       '<th>剩餘點數</th>';
      html +=     '</tr>';
      html +=   '</thead>';
      html +=   '<tbody>';

      $.each(response.data, function( index, item ) {

              if(index < countPerPage){
                html += '<tr>';
              }else{
                html += '<tr class="no_display">';
              }
              html += '<td>' + item.upr_datetime + '</td>';
              html += '<td>' + item.us_name + '</td>';
              html += '<td>' + item.upi_item + '</td>';
              html += '<td>' + ((item.upr_point_increase != 0)?'+'+item.upr_point_increase:'-'+item.upr_point_decrease) + '</td>';
              html += '<td>' + item.upr_point_left + '</td>';
              html += '</tr>';

      });

      html +=   '</tbody>';
      html += '</table>';

      html += '<div class="page_container">';
      html +=   '<div class="page_number_block">';

      html +=     '<button type="button" id="pager_tri_left_button" class="btn_pager_left -disabled"><span class="tri_left"></span><span class="tri_left"></span></button>';

      html +=     '<button type="button" id="pager_left_button" class="btn_item btn_left -disabled"></button>';
      html +=     '<span class="page_number"><span id="current_page" class="current_page">01</span><span class="span_slash">/</span><span id="total_page" class="total_page"></span></span>';
      html +=     '<button type="button" id="pager_right_button" class="btn_item btn_right"></button>';

      html +=     '<button type="button" id="pager_tri_right_button" class="btn_pager_right"><span class="tri_right"></span><span class="tri_right"></span></button>';

      html +=   '</div>';
      html += '</div>';

      $("#search_result").html(html);
      setPager(count);
    }
  });
}

/**
 * 設定分頁
 */
function setPager(count) {
  $("#current_page").html(str_pad(1, 2, 0));
  $("#pager_left_button").addClass("-disabled");
  $("#pager_tri_left_button").addClass("-disabled");

  var totalPage = Math.ceil(count / countPerPage);

  if (totalPage > 1) {
    $("#total_page").html(str_pad(totalPage, 2, 0));
    $("#pager_right_button").removeClass("-disabled");
    $("#pager_tri_right_button").removeClass("-disabled");
  } else {
    $("#total_page").html(str_pad(1, 2, 0));
    $("#pager_right_button").addClass("-disabled");
    $("#pager_tri_right_button").addClass("-disabled");
  }
}
