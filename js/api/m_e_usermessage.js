$(function(){
	// 取得頁面間傳遞參數
});

// 取得學生留言群組公告
function getUserMessageannounce() {
	$.ajax({
		url: g_obj.domain + '/api/userMessage/announce',
		type: 'GET',
		data: "token=" + Cookies.get('u_t'),
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					$(".serviceTime").html(response.data.user_message_announce);
					break;
				default:
					alert(response.msg);
					break;
			}
		}
	});
}

// 取得學生留言服務時間
function getUserMessageserviceTime() {
	$.ajax({
		url: g_obj.domain + '/api/userMessage/serviceTime',
		type: 'GET',
		data: "token=" + Cookies.get('u_t'),
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					$(".serviceTime").html(response.data.user_message_service_time);
					break;
				default:
					alert(response.msg);
					break;
			}
		}
	});
}

// 取得學生留言
function getUserMessage() {
	var fData = new FormData($("#submit_usermessage")[0]);	// 表單名稱請依實際修改
	fData.append("token", Cookies.get('u_t'));
	$.ajax({
		url: g_obj.domain + '/api/userMessage/student?token=' + Cookies.get('u_t'),
		type: 'GET',
		data: fData,
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					$("#user_msg_list").html("");
					var userMessageHtml = '';
					var userMessageData = response.data;
					for(var i = 0; i < userMessageData.length; i++) {
						var msgDate = new Date(userMessageData[i]["um_datetime"]);
						var oneMessageHtml = '';
						if (userMessageData[i]["um_content"] != "") {
							oneMessageHtml += userMessageData[i]["um_content"];
						}
						if (userMessageData[i]["um_img"] != "") {
							oneMessageHtml += '<span style="width: 120px; height: 120px; text-align: center; display:flex; align-items:center; justify-content:center;">';
							oneMessageHtml += '<a href="javascript:;" onclick="showLargeImage(\'' + userMessageData[i]["um_img"] + '\');">';
							oneMessageHtml += '<img src="' + userMessageData[i]["um_img"] + '" class="msgBlockImage" style="max-width: 120px; max-height: 120px;" />';
							oneMessageHtml += '</a>';
							oneMessageHtml += '</span>';
						}
						if (userMessageData[i]["um_link"] != "") {
							oneMessageHtml += '<a href="' + userMessageData[i]["um_link"] + '" target="_blank">' + userMessageData[i]["um_link"] + '</a>';
						}
						if (userMessageData[i]["is_admin_response"] == true) {
							userMessageHtml += '<li class="-others">';
							userMessageHtml += '<div class="msg_block">';
							userMessageHtml += '<div class="msg_bubble">';
							userMessageHtml += oneMessageHtml;
							userMessageHtml += '</div>';
							userMessageHtml += '<div class="time_block">';
							userMessageHtml += '<span class="date">' + msgDate.getFullYear() + "/" + (msgDate.getMonth() + 1) + "/" + msgDate.getDate()  + '</span>';
							userMessageHtml += '<span class="time">' + ((msgDate.getHours().toString().length == 1) ? "0" : "") + msgDate.getHours() + ":" + ((msgDate.getMinutes().toString().length == 1) ? "0" : "") + msgDate.getMinutes()  + '</span>';
							userMessageHtml += '</div>';
							userMessageHtml += '</div>';
							userMessageHtml += '</li>';
						}
						else {
							userMessageHtml += '<li class="-own">';
							userMessageHtml += '<div class="msg_block">';
							userMessageHtml += '<div class="time_block">';
							userMessageHtml += '<span class="date">' + msgDate.getFullYear() + "/" + (msgDate.getMonth() + 1) + "/" + msgDate.getDate()  + '</span>';
							userMessageHtml += '<span class="time">' + ((msgDate.getHours().toString().length == 1) ? "0" : "") + msgDate.getHours() + ":" + ((msgDate.getMinutes().toString().length == 1) ? "0" : "") + msgDate.getMinutes()  + '</span>';
							userMessageHtml += '</div>';
							userMessageHtml += '<div class="msg_bubble">';
							userMessageHtml += oneMessageHtml;
							userMessageHtml += '</div>';
							userMessageHtml += '</div>';
							userMessageHtml += '</li>';
						}
					}
					$(".user_msg_list").html(userMessageHtml);
					calc_msg_list_block_height();
					$(".txtNewMessage").hide();
					setTimeout(function () {$('.user_msg_list').scrollTop($('.user_msg_list').prop("scrollHeight"));}, 1000);
					break;
				default:
					alert(response.msg);
					break;
			}
		}
	});
}
// 留言點擊顯示大圖
function showLargeImage(u) {
	$('#user_message_large img').attr("src", u);
	$('#user_message_large').modal();
}

// 新增學生留言
function newUserMessage(t) {
	var fData = new FormData($("#submit_usermessage")[0]);	// 表單名稱請依實際修改
	if (t == 0) fData = new FormData($("#submit_usermessage2")[0]);	// 表單名稱請依實際修改
	fData.append("dv_id", "4");
	fData.append("token", Cookies.get('u_t'));
	$.ajax({
		url: g_obj.domain + '/api/userMessage/student',
		type: 'post',
		data: fData,
		dataType: 'json',
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					$("input.file_input").val("");
					if (t != 0) $("textarea#um_content").val("");
					getUserMessage();
					break;
				default:
					alert(response.msg);
					/*
						3601: // 學生留言內容及圖片未填
						3602: // 學生留言內容長度過長
						3603: // 學生留言圖片不為圖片檔
						3800: // 裝置id無效
					*/
					break;
			}
		}
	});
}
