var token;
var countPerPage = 10;
var userErrataListData = null;

$(function(){
  token = Cookies.get('u_t');

  getUserErrataList(token);
});

$(".option_radio").change(function() {
    var value = $('input[name=option_check]:checked').val();

    if (userErrataListData != null) {
      $("#item_list").html(setListView(userErrataListData, value));
    }
});

/**
 * 取得學生勘誤列表
 */
function getUserErrataList(token) {
  $.ajax({
    url: g_obj.domain + '/api/userErrata/list?token=' + token,
    type: 'get',
    // data: data,
    dataType: 'json',
    statusCode: {
      200: function (response) {
        // console.log(response);
      }
    },
    success: function(response){
      if(response.data.length > 0){
        userErrataListData = response.data;
        var value = $('input[name=option_check]:checked').val();
        $("#item_list").html(setListView(userErrataListData, value));
      } else {
        $("#item_list").html("");
        setPager(0);
      }
    }
  });
}

/**
 * 根據模式顯示列表資料
 */
function setListView(data, mode) {
  var html = "";
  var count = 0;

  if (mode == 'all') {
      $.each(data, function( index, item ) {
         if (count < countPerPage) {
            html += '<li><div class="item_block"><div class="item_right_block"><ul class="span_list">' + ((item.is_solved)?'<li><span class="hint_text"><img src="./images/icon/icon_handled.svg"></span></li>':'<li><span class="hint_text"><img src="./images/icon/icon_not_handled.svg"></span></li>') + '</ul><a href="./p_record_wrong_single.html?ue_id=' + item.ue_id + '" class="para">' + item.ue_content + '</a></div></div></li>';
         } else {
            html += '<li class="no_display"><div class="item_block"><div class="item_right_block"><ul class="span_list">' + ((item.is_solved)?'<li><span class="hint_text"><img src="./images/icon/icon_handled.svg"></span></li>':'<li><span class="hint_text"><img src="./images/icon/icon_not_handled.svg"></span></li>') + '</ul><a href="./p_record_wrong_single.html?ue_id=' + item.ue_id + '" class="para">' + item.ue_content + '</a></div></div></li>';
         }
         count++;
      });
  } else if (mode == 'handled') {
      $.each(data, function( index, item ) {
        if (item.is_solved) {
           if (count < countPerPage) {
              html += '<li><div class="item_block"><div class="item_right_block"><ul class="span_list">' + ((item.is_solved)?'<li><span class="hint_text"><img src="./images/icon/icon_handled.svg"></span></li>':'<li><span class="hint_text"><img src="./images/icon/icon_not_handled.svg"></span></li>') + '</ul><a href="./p_record_wrong_single.html?ue_id=' + item.ue_id + '" class="para">' + item.ue_content + '</a></div></div></li>';
           } else {
              html += '<li class="no_display"><div class="item_block"><div class="item_right_block"><ul class="span_list">' + ((item.is_solved)?'<li><span class="hint_text"><img src="./images/icon/icon_handled.svg"></span></li>':'<li><span class="hint_text"><img src="./images/icon/icon_not_handled.svg"></span></li>') + '</ul><a href="./p_record_wrong_single.html?ue_id=' + item.ue_id + '" class="para">' + item.ue_content + '</a></div></div></li>';
           }
           count++;
        }
      });
  } else if (mode == 'not_handled') {
      $.each(data, function( index, item ) {
        if (!item.is_solved) {
           if (count < countPerPage) {
              html += '<li><div class="item_block"><div class="item_right_block"><ul class="span_list">' + ((item.is_solved)?'<li><span class="hint_text"><img src="./images/icon/icon_handled.svg"></span></li>':'<li><span class="hint_text"><img src="./images/icon/icon_not_handled.svg"></span></li>') + '</ul><a href="./p_record_wrong_single.html?ue_id=' + item.ue_id + '" class="para">' + item.ue_content + '</a></div></div></li>';
           } else {
              html += '<li class="no_display"><div class="item_block"><div class="item_right_block"><ul class="span_list">' + ((item.is_solved)?'<li><span class="hint_text"><img src="./images/icon/icon_handled.svg"></span></li>':'<li><span class="hint_text"><img src="./images/icon/icon_not_handled.svg"></span></li>') + '</ul><a href="./p_record_wrong_single.html?ue_id=' + item.ue_id + '" class="para">' + item.ue_content + '</a></div></div></li>';
           }
           count++;
        }
      });
  }

  setPager(count);

  return html;
}

/**
 * 設定分頁
 */
function setPager(count) {
  $("#current_page").html(str_pad(1, 2, 0));
  $("#pager_left_button").addClass("-disabled");
  $("#pager_tri_left_button").addClass("-disabled");

  var totalPage = Math.ceil(count / countPerPage);

  if (totalPage > 1) {
    $("#total_page").html(str_pad(totalPage, 2, 0));
    $("#pager_right_button").removeClass("-disabled");
    $("#pager_tri_right_button").removeClass("-disabled");
  } else {
    $("#total_page").html(str_pad(1, 2, 0));
    $("#pager_right_button").addClass("-disabled");
    $("#pager_tri_right_button").addClass("-disabled");
  }
}
