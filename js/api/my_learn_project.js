
var app = new Vue({
    el: "#vueApp",
    data: {
        dayString:['一','二','三','四','五','六','日'],
        current_page: 1,
        total_page: 1,
        ol_list: [],
        weekDays:[],
        clickDate: new Date(),
        weekNumber:1,
        planList:[],
        upcs_content:'',
		upcs_id:''
    },
    methods: {
        lastPage: function() {

            if (app.current_page > 1) {
                app.current_page -= 1;
            }

        },
        nextPage: function() {

            if (app.current_page < app.total_page) {
                app.current_page += 1;
            }

        },
        displayRange: function(index) {
            if (index >= app.current_page * 15) {
                return true;
            }
            if (index < (app.current_page - 1) * 15) {
                return true;
            }
            return false;
        },
        getDateString:function(date) {
            return (date.getMonth() + 1) + '/' + date.getDate()
        },
        getDateParam:function(date) {
            return addZero(date.getFullYear()) + '-' + addZero((date.getMonth() + 1)) + '-' + addZero(date.getDate());
        },
        lastWeek: function() {
            this._doChangeWeek(-(86400000 * 7));
        },
        nextWeek: function() {
            this._doChangeWeek(86400000 * 7);
        },
        getWeekNumber: function (date) {
            var firstDay = new Date(date.getFullYear(),0, 1);
            var dayOfWeek = firstDay.getDay();
            var spendDay= 1;
            if (dayOfWeek !=0) {
                spendDay=7-dayOfWeek+1;
            }
            firstDay = new Date(date.getFullYear(),0, 1+spendDay);
            var d =Math.ceil((date.valueOf()- firstDay.valueOf())/ 86400000);
            var result =Math.ceil(d/7);
            return result + 1;
        },
        getWeekDays: function(date) {
            var weekDay = this._getDayOfWeek(date);
            this.clickDate = date;
            var weekDates = [];
            var firstUnixTime = date.getTime() - (weekDay - 1) * 86400000;
            for (i = 0; i <= 6; i += 1) {
                var tempDate = new Date(firstUnixTime + (i * 86400000));
                weekDates.push( tempDate );
                if (i == 6) {
                    this.getWeekNumber( tempDate );
                }
            }
            this.weekDays = weekDates;
        },
        planClass: function(date) {

            return {
                '-on' : this.getDateString(date) == this.getDateString(this.clickDate),
                '-lavel1' : this.classLevel(date) == 1 ,
                '-lavel2' : this.classLevel(date) == 2 ,
                '-lavel3' : this.classLevel(date) >= 3
            }
        },
        classLevel: function(date) {
            for (var key in this.planList) {
                var item = this.planList[key];
                if (item.datetime == this.getDateParam(date) + ' 00:00:00') {
                    return item.user_count;
                }
            }
            return 0;

        },
        commentDisplay: function(date) {
            for (var key in this.planList) {
                var item = this.planList[key];
                if (item.datetime == this.getDateParam(date) + ' 00:00:00') {
                    return item.is_comment == 1;
                }
            }
            return false;
        },
        redirectUrl: function(item) {
            var self = this;
            if (item.ot_id == g_obj.teacher_ot_id) {
                self.redirectTeaching(item);
            }else{
                self.redirectPratice(item);
            }
        },
        redirectTeaching:function(item) {
            console.log("ITEM:",item);
            var nodes = item.previous_nodes;

            var parameters = [
                'ol_id=' + item.ol_id,
                'unit_ol_id=' + nodes[2].ol_id,
                'field_name=' + nodes[5].ol_display_name,
                'subject_name=' + nodes[4].ol_display_name,
                'version_name=' + (nodes[3].ol_display_name != '不分版' ? nodes[3].ol_display_name : ''),
                'grade_group_name=' + nodes[2].ol_display_name,
                'chapter_name=' + nodes[1].ol_display_name,
                'part_name=' + item.ol_display_name
            ].join('&');

            location.href = '/f_teacher_video.html?' + parameters;

        },
        redirectPratice:function(item) {
            console.log("item pratice:",item);
            var nodes = item.previous_nodes;
            var parameters = [
                'ol_id=' + nodes[2].ol_id,
                'field_name=' + nodes[5].ol_display_name,
                'subject_name=' + nodes[4].ol_display_name,
                'version_name=' + (nodes[3].ol_display_name != '不分版' ? nodes[3].ol_display_name : ''),
                'grade_group_name=' + nodes[2].ol_display_name,
                // 'chapter_name=' + nodes[1].ol_display_name,
                // 'part_name=' + item.ol_display_name
            ].join('&');
            window.localStorage["String_Check_Value"] = item.ol_id + ',' ;
            location.href = '/q_db_auto_and_manual_question.html?' + parameters;
        },
        //請求課程內容資料
        requestData: function(date) {
            console.log(date);
            var self = this;
            this.clickDate = date;
            $.get(g_obj.domain + '/api/userplan?token='+ Cookies.get('u_t') +'&upls_datetime='+this.getDateParam(date),function(response) {
                var ol_list = response.data.ol_list;
                self.ol_list = ol_list;
                console.log("OL_LIST:",app.ol_list);
                self.total_page = Math.ceil((ol_list.length + 1) / 15);
                if (typeof response.data.comment[0] != "undefined") {
					if (typeof response.data.comment[0].upcs_content != "undefined") self.upcs_content = response.data.comment[0].upcs_content;
					if (typeof response.data.comment[0].upcs_id != "undefined") self.upcs_id = response.data.comment[0].upcs_id;
				}
				else {
					self.upcs_content = "";
					self.upcs_id = "";
				}
            });
        },
        /** 使用者計劃 */
        getPlanList: function() {
            var beginDate = this.weekDays[0];
            var endDate = this.weekDays[6];
            var url = g_obj.domain + '/api/userplan/list?';
                url += 'token='+ Cookies.get('u_t');
                url += '&upls_datetime_start=' + this.getDateParam(beginDate);
                url += '&upls_datetime_stop=' + this.getDateParam(endDate);
            $.get(url,function(response) {
                console.log("data:",response);
                app.planList = response.data;
            });

        },
        _doChangeWeek:function(timeInterval) {
			
		console.log(timeInterval);
			
            var weekDay = this._getDayOfWeek(this.clickDate);
            var date = new Date(this.clickDate.getTime() + timeInterval);
            this.getWeekDays(date);
            this.clickDate = this.weekDays[weekDay - 1];
            this.requestData(this.clickDate);
        },
        /** 將錯誤的星期日索引修正 */
        _getDayOfWeek: function(date) {
            return date.getDay() != 0 ? date.getDay() : 7;
        },
        /** 科目，版本，年級群組，章的文字*/
        itemFirstTitle:function(item) {
            var subjectTitle = item.previous_nodes[4].ol_display_name + ' ';
            var versionTitle = item.previous_nodes[3].ol_display_name + ' ';
            var groupTitle = item.previous_nodes[2].ol_display_name + ' ';
            var unitTitle = item.previous_nodes[1].ol_display_name;
            if (versionTitle == '不分版 ' || versionTitle == '不分版本 ') {
                versionTitle = '';
            }
            return subjectTitle + versionTitle + groupTitle + unitTitle;
        }


    },
    beforeMount: function() {
        if($.url('?c_date') != undefined){
          var today = new Date($.url('?c_date'));
        }else{
          var today = new Date();
        }
        this.getWeekDays(today);
        this.requestData(today);

    },
    watch: {
        weekDays : function() {
            this.getPlanList();
        }
    }
});

/** 日期小於10的補0值 */
function addZero(number) {
    return number < 10 ? '0'+ number : number ;
}



//http://localhost:24681/f_teacher_video.html?ol_id=58&unit_ol_id=45&field_name=%E5%85%A8%E7%A7%91&subject_name=%E5%9C%8B%E6%96%87&version_name=&grade_group_name=%E6%A0%B8%E5%BF%83%E6%96%87%E8%A8%80%E6%96%87%E9%81%B8&chapter_name=%E7%AC%AC%E4%B8%80%E5%96%AE%E5%85%83%20%E8%99%AF%E9%AB%AF%E5%AE%A2%E5%82%B3&part_name=%E7%87%AD%E4%B9%8B%E6%AD%A6%E9%80%80%E7%A7%A6%E5%B8%AB(%E4%B8%8A)
