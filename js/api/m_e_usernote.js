var ol_id, un_id;
var boolDelete = [false, false];
var uploadBase64 = ["", ""];

$(function(){
	// 取得頁面間傳遞參數
	ol_id = $.url('?ol_id');
	if (typeof ol_id == "undefined") ol_id = "";
	un_id = $.url('?un_id');
	if (typeof un_id == "undefined") un_id = "";
});

/**
 * 新增/修改筆記
 */
function saveUserNote(imgBase64_1, imgBase64_2, imgBase64_3, content) {
	var rtnStatus = false;
	var fData = new FormData();
	fData.append("token", Cookies.get('u_t'));
	fData.append("un_id", un_id);
	// fData.append("ol_id", ((ol_id == "" && $('#tmp_ol_id').length > 0) ? $('#tmp_ol_id').val() : ol_id));
	fData.append("ol_id", (($('#tmp_ol_id').length > 0 && $('#tmp_ol_id').val() != "") ? $('#tmp_ol_id').val() : ol_id));
	fData.append("un_content", content);
	fData.append("note_img_base64", imgBase64_1);
	fData.append("note_img2_base64", imgBase64_2);
	fData.append("note_img3_base64", imgBase64_3);
	if (un_id != "") {	// 修改筆記
		if (boolDelete[0] && imgBase64_2 == "") delUserNoteImg(2);
		if (boolDelete[1] && imgBase64_3 == "") delUserNoteImg(3);
		$.ajax({
			url: g_obj.domain + '/api/usernote/update',
			type: 'post',
			data: fData,
			dataType: 'json',
			async: false,
			processData: false,
			contentType: false,
			statusCode: {
				200: function (response) {
					// console.log(response);
					// 成功
				}
			},
			success: function(response){
				console.log(response);
				switch (response.status) {
					case 1000:
						// alert("更新完成");
						if ($(".para_note_new_ok").length > 0) $(".para_note_new_ok").hide();
						rtnStatus = true;
						break;
					default:
						// alert(response.msg);
						/*
							2009: // 大綱id無效
							6000: // 使用者筆記id無效
							6001: // 使用者筆記內容過長
							6002: // 使用者筆記圖片 1, 2, 3 未上傳
							6003: // 使用者筆記圖片 1, 2, 3 類型錯誤
						*/
						break;
				}
			}
		});
	}
	else {	// 新增筆記
		$.ajax({
			url: g_obj.domain + '/api/usernote',
			type: 'post',
			data: fData,
			dataType: 'json',
			async: false,
			processData: false,
			contentType: false,
			statusCode: {
				200: function (response) {
					// console.log(response);
					// 成功
				}
			},
			success: function(response){
				console.log(response);
				switch (response.status) {
					case 1000:
						// 執行成功要做的事
						// alert("新文章編號: " + response.data.un_id);
						if ($(".para_note_update_ok").length > 0) $(".para_note_update_ok").hide();
						rtnStatus = true;
						break;
					default:
						// alert(response.msg);
						/*
							2009: // 大綱id無效
							6001: // 使用者筆記內容過長
							6002: // 使用者筆記圖片 1, 2, 3 未上傳
							6003: // 使用者筆記圖片 1, 2, 3 類型錯誤
						*/
						break;
				}
			}
		});		
	}
	return rtnStatus;
}

/**
 * 刪除一個使用者筆記圖片
 */
function removeNotePic(i, j) {
	if (confirm("確定要移除圖片嗎?")) {
		$(".note_item_pic_" + i).remove();
		$("#add_pic_li").removeClass("no_display");
		if (j == 0) boolDelete[i] = true;
		else {
			uploadBase64[j - 1] = "";
		}
	}
}
function delUserNoteImg(n) {
	$.ajax({
		url: g_obj.domain + '/api/usernote/img',
		type: 'DELETE',
		data: "un_id=" + un_id + "&un_img=" + n,
		headers: {
            "token": Cookies.get('u_t'),
            "Content-Type": "application/x-www-form-urlencoded"
        },
		dataType: 'json',
		async: false,
		processData: false,
		contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					break;
				default:
					// alert(response.msg);
					/*
						6000: // 使用者筆記id無效
						6004: // 使用者筆記圖片編號無效
					*/
					break;
			}
		}
	});
}

/**
 * 刪除使用者筆記
 */
function delUserNote(del_id) {
	var rtnStatus = false;
	$.ajax({
		url: g_obj.domain + '/api/usernote',
		type: 'DELETE',
		data: "un_id=" + del_id,
		headers: {
            "token": Cookies.get('u_t'),
            "Content-Type": "application/x-www-form-urlencoded"
        },
		dataType: 'json',
		async: false,
		// processData: false,
		// contentType: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
				// 成功
			}
		},
		success: function(response){
			console.log(response);
			switch (response.status) {
				case 1000:
					// 執行成功要做的事
					// alert(response.msg);
					window.location.reload();
					rtnStatus = true;
					break;
				default:
					alert(response.msg);
					// 6000: // 使用者筆記id無效
					break;
			}
		}
	});
	return rtnStatus;
}