var q;
$(function(){
	// 取得頁面間傳遞參數
	q = $.url('?q');
	if (typeof q == "undefined") q = "";
});

// 取得查詢條件
var jsonSearchArray = [];
function doSearch() {
	for(var i = 1; i <= 11; i++) {
		if (i != 5 && i != 7 && i != 9 && i != 10) {
			$.ajax({
				url: g_obj.domain + '/api/outline/search',
				type: 'GET',
				data: "ot_id=" + i + "&ol_display_name=" + encodeURIComponent(q) + "&token=" + Cookies.get('u_t'),
				dataType: 'json',
				async: false,
				processData: false,
				contentType: false,
				statusCode: {
					200: function (response) {
						// console.log(response);
						// 成功
					}
				},
				success: function(response){
					console.log(response);
					switch (response.status) {
						case 1000:
							var queryResult = response.data;
							jsonSearchArray[i] = queryResult;
							$(".tab_" + i + " .total_count").html(queryResult.length + "筆");
							showSearchResult(i);
							break;
						default:
							// alert(response.msg);
							break;
					}
				}
			});
		}
	}
}

var extClickEvent = "";
function showSearchResult(t) {
	var rtnHtml = '';
	if (jsonSearchArray[t].length == 0) {
		rtnHtml += '<span class="-loading">搜尋不到相關內容</span>';
	}
	else {
		for(var j = 0; j < jsonSearchArray[t].length; j++) {
			rtnHtml += '<li class="' + ((j > 14) ? 'no_display' : '') + '">';
			rtnHtml += '<a href="' + getSearchOutline(jsonSearchArray[t][j].ol_id, t, jsonSearchArray[t][j].ol_display_name) + '" onclick="' + extClickEvent + '" class="item_link">';
			rtnHtml += '<img src="' + jsonSearchArray[t][j].ol_img + '" class="link_img">';
			rtnHtml += '<div class="desc_block">';
			if (t == 2) rtnHtml += '<p class="para1">' + jsonSearchArray[t][j].parents[2].ol_display_name + ' ' + jsonSearchArray[t][j].parents[1].ol_display_name + '</p>';
			else rtnHtml += '<p class="para1">' + jsonSearchArray[t][j].parents[3].ol_display_name + ' ' + jsonSearchArray[t][j].parents[2].ol_display_name + ' ' + jsonSearchArray[t][j].parents[1].ol_display_name + ' ' + jsonSearchArray[t][j].parents[0].ol_display_name + '</p>';
			rtnHtml += '<p class="para2">' + jsonSearchArray[t][j].ol_display_name + '</p>';
			rtnHtml += '</div>';
			rtnHtml += '</a>';
			rtnHtml += '</li>';
		}
	}
	$(".tab_content_" + t + " ul.item_list").html(rtnHtml);
	
	var tPager = '';
	// if (Math.ceil(jsonSearchArray[t].length / 15) > 1) {
		tPager += '<button type="button" class="btn_item btn_left -disabled"></button>';
		tPager += '<span class="page_number"><span class="current_page">01</span><span class="span_slash">/</span><span class="total_page">' + str_pad(Math.ceil(jsonSearchArray[t].length / 15), 2, 0) + '</span></span>';
		tPager += '<button type="button" class="btn_item btn_right ' + ((Math.ceil(jsonSearchArray[t].length / 15) > 1) ? '' : '-disabled') + '"></button>';
	// }
	$(".tab_content_" + t + " .page_number_block").html(tPager);
}

function getSearchOutline(olId, t, p) {
	var rtnUrl = "javascript:;";
	extClickEvent = "";
	$.ajax({
		url: g_obj.domain + '/api/outline?ol_id=' + olId + '&token=' + Cookies.get('u_t'),
		type: 'get',
		dataType: 'json',
		async: false,
		statusCode: {
			200: function (response) {
				// console.log(response);
			}
		},
		success: function(response) {
			if (typeof response.data != "undefined") {
				var rtnValue = response.data[0];
				switch(t) {
					case 1:
						rtnUrl = "./f_teacher_video.html?ol_id=" + olId + "&unit_ol_id=" + rtnValue.previous_nodes[0].ol_id_parent
							+ "&field_name=" + rtnValue.previous_nodes[4].ol_display_name + "&subject_name=" + rtnValue.previous_nodes[3].ol_display_name
							+ "&version_name=&grade_group_name=" + rtnValue.previous_nodes[2].ol_display_name + "&chapter_name=" + rtnValue.previous_nodes[1].ol_display_name
							+ "&part_name=" + rtnValue.previous_nodes[0].ol_display_name;
						break;
					case 2:
						rtnUrl = "./exam_live_stream_video.html?ol_id=" + olId + "&unit_ol_id=" + rtnValue.previous_nodes[0].ol_id
							+ "&field_name=" + rtnValue.previous_nodes[1].ol_display_name + "&chapter_name=" + rtnValue.previous_nodes[0].ol_display_name
							+ "&part_name=" + p + "&select_1=" + rtnValue.previous_nodes[1].ol_id + "&select_2=" + rtnValue.previous_nodes[0].ol_id;
						break;
					case 3:
						extClickEvent = "localStorage.setItem('String_Check_Value', '" + rtnValue.ol_id + ",');";
						rtnUrl = "./q_db_auto_and_manual_question.html?ol_id=" + rtnValue.previous_nodes[1].ol_id
							+ "&field_name=" + rtnValue.previous_nodes[4].ol_display_name + "&subject_name=" + rtnValue.previous_nodes[3].ol_display_name
							+ "&version_name=" + ((rtnValue.previous_nodes[2].ol_display_name == "不分版") ? "" : rtnValue.previous_nodes[2].ol_display_name) + "&grade_group_name=" + rtnValue.previous_nodes[1].ol_display_name;
						break;
					case 4:
						rtnUrl = "./m_e_paper.html?ol_id=" + olId + "&unit_ol_id=" + rtnValue.previous_nodes[0].ol_id_parent
							+ "&field_name=" + rtnValue.previous_nodes[4].ol_display_name + "&subject_name=" + rtnValue.previous_nodes[3].ol_display_name
							+ "&version_name=" + ((rtnValue.previous_nodes[2].ol_display_name == "不分版") ? "" : rtnValue.previous_nodes[2].ol_display_name) + "&grade_group_name=" + rtnValue.previous_nodes[1].ol_display_name
							+ "&chapter_name=" + rtnValue.previous_nodes[0].ol_display_name + "&part_name=" + rtnValue.ol_display_name;
						break;
					case 6:	
						rtnUrl = "./p_question_list.html?ol_id=" + olId + "&unit_ol_id=" + rtnValue.previous_nodes[0].ol_id_parent
							+ "&field_name=" + rtnValue.previous_nodes[4].ol_display_name + "&subject_name=" + rtnValue.previous_nodes[3].ol_display_name
							+ "&version_name=&grade_group_name=" + rtnValue.previous_nodes[2].ol_display_name + "&chapter_name=" + rtnValue.previous_nodes[1].ol_display_name
							+ "&part_name=" + rtnValue.previous_nodes[0].ol_display_name;
						break;
					case 8:
						rtnUrl = "./my_learn_notes_list.html?ol_id=" + olId + "&unit_ol_id=" + rtnValue.previous_nodes[0].ol_id_parent
							+ "&field_name=" + rtnValue.previous_nodes[4].ol_display_name + "&subject_name=" + rtnValue.previous_nodes[3].ol_display_name
							+ "&version_name=" + rtnValue.previous_nodes[2].ol_display_name + "&grade_group_name=" + rtnValue.previous_nodes[1].ol_display_name
							+ "&chapter_name=" + rtnValue.previous_nodes[0].ol_display_name
							+ "&part_name=" + rtnValue.ol_display_name;
						break;
					case 9:
						rtnUrl = "./m_i_list.html?ol_id=" + olId + "&unit_ol_id=" + rtnValue.previous_nodes[0].ol_id_parent
							+ "&field_name=" + rtnValue.previous_nodes[4].ol_display_name + "&subject_name=" + rtnValue.previous_nodes[3].ol_display_name
							+ "&version_name=&grade_group_name=" + rtnValue.previous_nodes[2].ol_display_name + "&chapter_name=" + rtnValue.previous_nodes[1].ol_display_name
							+ "&part_name=" + rtnValue.previous_nodes[0].ol_display_name;
						break;
					case 11:
						rtnUrl = "./q_db_exam.html?ol_id=" + olId + "&unit_ol_id=" + rtnValue.previous_nodes[0].ol_id_parent
							+ "&field_name=" + rtnValue.previous_nodes[4].ol_display_name + "&subject_name=" + rtnValue.previous_nodes[3].ol_display_name
							+ "&version_name=&grade_group_name=" + rtnValue.previous_nodes[2].ol_display_name + "&chapter_name=" + rtnValue.previous_nodes[1].ol_display_name
							+ "&part_name=" + rtnValue.previous_nodes[0].ol_display_name;
						break;
					default:
						
						break;
				}
			}
		}
	});
	return rtnUrl;
}