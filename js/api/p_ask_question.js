var olId;
var unitOlId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterName;
var partName;

$(function(){
  // 取得頁面間傳遞參數
  olId = $.url('?ol_id');
  unitOlId = $.url('?unit_ol_id');
  fieldName = $.url('?field_name');
  subjectName = $.url('?subject_name');
  versionName = $.url('?version_name');
  gradeGroupName = $.url('?grade_group_name');
  chapterName = $.url('?chapter_name');
  partName = $.url('?part_name');

  // 設定導覽列
  checkHeaderTextAfterUnit(unitOlId);
  //$("#header_field").text(fieldName);
  //$("#header_subject").text(subjectName);
  //$("#header_grade_group").text(versionName + gradeGroupName);
  $("#header_unit").attr("href", "./p_ask_unit.html?ol_id=" + unitOlId + "&field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + versionName + "&grade_group_name=" + gradeGroupName);

  $("#title1").text(subjectName + " " + ((versionName)?versionName + " " : "") + gradeGroupName + " " + chapterName + " " + partName);

  // 設定導覽列連結
  setHeaderGradeGroupHref(unitOlId);

  // 筆記截圖
  getUsernoteListData();

  left_aside_link(4); // 更換大類別連結
});

function getUsernoteListData(){
  $.ajax({
    url: g_obj.domain + '/api/usernote/list?ol_id=' + olId + '&token=' + Cookies.get('u_t'),
    type: 'get',
    // data: data,
    dataType: 'json',
    //headers: { 'token': Cookies.get('u_t') },
    success: function(res){
      //alert(res.data.length);
      if(res.data.length > 0){
        var html = '';
        $.each(res.data, function(index, item){
          html += '<li><button type="button" class="btn_note_choose"><img src="' + item.un_path + '" data-un-id="' + item.un_id + '"></button></li>'
        });
        $("ul.note_list").html(html);
      }else{
        $("button.btn_upload_confirm").addClass("-disabled");
      }
    }
  });
}
