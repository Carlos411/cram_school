﻿var olId;
var fieldName;
var subjectName;
var versionName;
var gradeGroupName;
var chapterCount;
var totalCount = 0;
var totalContent = "";
var custom_title_for_filename = '';

var ol_id_data = [];

$(function(){
	// 取得頁面間傳遞參數
	olId = $.url('?ol_id');
	fieldName = $.url('?field_name');
	subjectName = $.url('?subject_name');
	versionName = $.url('?version_name');
	gradeGroupName = $.url('?grade_group_name');

	// 設定導覽列
	checkHeaderTextAfterUnit(olId);
	//$("#header_field").text(fieldName);
	//$("#header_subject").text(subjectName);
	//$("#header_grade_group").text(versionName + gradeGroupName);
	//$("#header_unit").attr("href", "#");
	$("#header_unit").attr("href", "./q_db_exam_unit.html?ol_id=" + olId + "&field_name=" + fieldName + "&subject_name=" + subjectName + "&version_name=" + versionName + "&grade_group_name=" + gradeGroupName);

	// 設定導覽列連結
	setHeaderGradeGroupHref(olId);


	// 最近觀看
	//get_video_recent("video_recent_item_list");

	setHeaderText();

});

var arr_Check_Value_count = 0;
function setHeaderText(){
	var arr_Check_Value = window.localStorage["String_Check_Value"].split(",");
	arr_Check_Value.pop(); // 移除陣列中的最後一項
	if(arr_Check_Value.length > 0){
		//alert("XX String_Check_Value=["+ index +"]["+ item +"]");
		$.get( g_obj.domain + "/api/outline?ol_id=" + arr_Check_Value[arr_Check_Value_count] + '&token=' + Cookies.get('u_t'), function( res ) {
			ol_id_data.push(res.data);
			if(arr_Check_Value_count == arr_Check_Value.length - 1){
				getHeaderTextUnit();
			}else{
				arr_Check_Value_count++;
				setHeaderText()
			}
		});
	}
}

function getHeaderTextUnit(){
	var parse_data = []; // 將相同章的歸類
	$.each(ol_id_data, function( index, item ) {
		if(index == 0){
			parse_data.push({previous_nodes_zero_ol_id: item[0].previous_nodes[0].ol_id, same_chapter: [item]});
		}else{
			var have_same = false;
			$.each(parse_data, function( index_inner, item_inner ) {
				if(item_inner.previous_nodes_zero_ol_id == item[0].previous_nodes[0].ol_id){
					have_same = true;
					parse_data[index_inner].same_chapter.push(item);
				}
			});
			if(!have_same){
				parse_data.push({previous_nodes_zero_ol_id: item[0].previous_nodes[0].ol_id, same_chapter: [item]});
			}
		}
	});
	var custom_title = subjectName + " " + (versionName == "不分版" ? "" : versionName) + gradeGroupName;

	custom_title_for_filename = fieldName + "_" + subjectName + (versionName == "不分版" ? "" : "_" + versionName) + "_" + gradeGroupName + "_";

	$.each(parse_data, function( index, item ) {
		$.each(item.same_chapter, function( index_inner, item_inner ) {
			if(index_inner == 0){
				custom_title += " " + item_inner[0].previous_nodes[0].ol_display_name + " " + item_inner[0].ol_display_name;
				if(index == 0){ // 只取第一個的章
					custom_title_for_filename += item_inner[0].previous_nodes[0].ol_display_name;
				}
			}else{
				custom_title += " " + item_inner[0].ol_display_name;
			}
		});
	});
	// alert(custom_title_for_filename);
	$("h1.title1").html(custom_title);
}
