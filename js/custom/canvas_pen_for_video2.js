var original_data_url;
var canvas_el;
function InitThis() {
  original_data_url = document.getElementById('draw_canvas').toDataURL("image/png");
  canvas_el = new fabric.Canvas('draw_canvas', {
    isDrawingMode: true,
    //backgroundColor : "#eee",
    selection: false
  });
  canvas_el.setBackgroundImage(original_data_url,canvas_el.renderAll.bind(canvas_el));
  canvas_el.renderAll();
  canvas_el.on('object:selected', function(){
    if(!canvas_el.isDrawingMode){
      var activeObject = canvas_el.getActiveObject();
      canvas_el.remove(activeObject);
    }
  });
  $("button.btn_pencil_yellow").click();
}

$(function(){

  // 清除
  $("button.btn_delete").on("click", function(){
    var arrObj = canvas_el.getObjects();
    while(arrObj.length != 0){
      canvas_el.remove(arrObj[0]);
    }
  });

  // 畫筆
  $("button.btn_pen").on("click", function(){
    canvas_el.isDrawingMode = true;
    canvas_el.freeDrawingBrush.color = $(this).attr("data-value");
    canvas_el.freeDrawingBrush.width = 5;
    $(this).closest("ul.draw_list").find("button.btn_draw").removeClass("-on");
    $(this).addClass("-on");
  });

  // 橡皮擦
  $("button.btn_eraser").on("click", function(){
    canvas_el.isDrawingMode = false;
    $(this).closest("ul.draw_list").find("button.btn_draw").removeClass("-on");
    $(this).addClass("-on");
  });

});
