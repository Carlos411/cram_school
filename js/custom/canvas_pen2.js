var ctx;
var img_src = '';
var already_set_canvas_height = false;
var original_data_url;
var canvas_el;
//var img_src = 'http://via.placeholder.com/1120x488';

function InitThis() {
  ctx = document.getElementById('draw_canvas').getContext("2d");
  append_img_on_canvas();
}

function fabric_canvas(){
  original_data_url = document.getElementById('draw_canvas').toDataURL();
  canvas_el = new fabric.Canvas('draw_canvas', {
    isDrawingMode: true,
    //backgroundColor : "#eee",
    selection: false
  });
  canvas_el.setBackgroundImage(original_data_url,canvas_el.renderAll.bind(canvas_el));
  canvas_el.renderAll();
  canvas_el.on('object:selected', function(){
    if(!canvas_el.isDrawingMode){
      var activeObject = canvas_el.getActiveObject();
      canvas_el.remove(activeObject);
    }
  });
  $("button.btn_pencil_yellow").click();
}


// canvan 放上圖片
function append_img_on_canvas(){
  base_image = new Image();
  base_image.crossOrigin = "Anonymous";
  base_image.src = img_src;
  base_image.onload = function(){
    //console.log(base_image.naturalWidth + ' ' + base_image.naturalHeight);
    var ratio = 1020 / base_image.naturalWidth;
    if(!already_set_canvas_height){
      already_set_canvas_height = true;
      $("#draw_canvas").attr("height", base_image.naturalHeight * ratio);
    }
    ctx.drawImage(base_image, 0, 0, 1020, base_image.naturalHeight * ratio);
    fabric_canvas();
  }
}

$(function(){
  // 清除
  $("button.btn_delete").on("click", function(){
    var arrObj = canvas_el.getObjects();
    while(arrObj.length != 0){
      canvas_el.remove(arrObj[0]);
    }
  });

  // 畫筆
  $("button.btn_pen").on("click", function(){
    canvas_el.isDrawingMode = true;
    canvas_el.freeDrawingBrush.color = $(this).attr("data-value");
    canvas_el.freeDrawingBrush.width = 5;
    $(this).closest("ul.draw_list").find("button.btn_draw").removeClass("-on");
    $(this).addClass("-on");
  });

  // 橡皮擦
  $("button.btn_eraser").on("click", function(){
    canvas_el.isDrawingMode = false;
    $(this).closest("ul.draw_list").find("button.btn_draw").removeClass("-on");
    $(this).addClass("-on");
  });
});
